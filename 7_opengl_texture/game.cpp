#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <istream>
#include <memory>
#include <string_view>

#include "engine.h"

int main(int /*argc*/, char * /*argv*/[]) {
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error =
        engine->initialize("title=TEXTURE;window_width=640;window_height=480;");
    if (!error.empty()) {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    om::texture *tank = engine->create_texture("../tank.png");
    if (tank == nullptr) {
        std::cerr << "Failed load texture." << std::endl;
        return EXIT_FAILURE;
    }
    om::texture *cat = engine->create_texture("../cat.png");
    if (cat == nullptr) {
        std::cerr << "Failed load texture." << std::endl;
        return EXIT_FAILURE;
    }

    bool continue_loop = true;
    while (continue_loop) {
        om::event event;
        while (engine->read_input(event)) {
            if (event == om::event::turn_off) {
                continue_loop = false;
            }
        }

        om::triangle_buffer tb0;
        std::ifstream buffer_0("../buffer_0.txt");
        assert(!!buffer_0);
        buffer_0 >> tb0;

        om::triangle_buffer tb1;
        std::ifstream buffer_1("../buffer_1.txt");
        assert(!!buffer_1);
        buffer_1 >> tb1;

        engine->render(tb0, *tank);
        engine->render(tb1, *cat);

        engine->swap_buffers();
    }

    delete tank;
    delete cat;
    engine->uninitialize();

    return EXIT_SUCCESS;
}