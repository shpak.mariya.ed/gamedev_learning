#version 330 core
layout (location = 0) in vec2 a_pos;
layout (location = 1) in vec3 a_color;
layout (location = 2) in vec2 a_text_pos;

out vec3 out_color;
out vec2 out_text_pos;

void main() {
    out_color = a_color;
    out_text_pos = a_text_pos;
    gl_Position = vec4(a_pos, 0.0, 1.0);
}