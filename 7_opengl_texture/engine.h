#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

namespace om {
enum class event { turn_off };

class engine;

engine *create_engine();
void destroy_engine(engine *e);

struct vertex {
    float x = 0.f;
    float y = 0.f;
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    float u = 0.f;
    float v = 0.f;
};

struct triangle {
    triangle() {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

using triangle_buffer = std::vector<triangle>;

std::istream &operator>>(std::istream &is, vertex &);
std::istream &operator>>(std::istream &is, triangle &);
std::istream &operator>>(std::istream &is, triangle_buffer &);

class texture {
public:
    virtual ~texture();
    virtual void bind() const = 0;
    virtual uint8_t get_id() const = 0;
};

class engine {
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view config) = 0;
    virtual float get_time() = 0;
    virtual void set_uniform(std::string_view, const texture &) = 0;
    virtual bool read_input(event &e) = 0;
    virtual texture *create_texture(std::string_view) = 0;
    virtual void render(const triangle &) = 0;
    virtual void render(const triangle_buffer &, const texture &) = 0;
    virtual void swap_buffers() = 0;
    virtual void uninitialize() = 0;
};

} // end namespace om