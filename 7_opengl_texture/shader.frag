#version 330 core
in vec3 out_color;
in vec2 out_text_pos;

uniform sampler2D texture_;

void main() {
    gl_FragColor = texture(texture_, out_text_pos);
}