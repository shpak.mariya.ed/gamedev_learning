#include "engine.h"
#include "picopng.hxx"

#include <algorithm>
#include <array>
#include <cassert>
#include <charconv>
#include <cmath>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;
PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
PFNGLBUFFERDATAPROC glBufferData = nullptr;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = nullptr;
PFNGLUNIFORM1IPROC glUniform1i = nullptr;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;

template <typename T>
static void load_gl_func(const char *func_name, T &result) {
    void *gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("Cannot load GL function: ") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

namespace om {

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream &operator>>(std::istream &is, vertex &v) {
    is >> v.x >> v.y >> v.r >> v.g >> v.b >> v.u >> v.v;
    return is;
}

std::istream &operator>>(std::istream &is, triangle &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream &operator>>(std::istream &is, triangle_buffer &tb) {
    while (!is.eof()) {
        triangle tr;
        is >> tr;
        tb.push_back(tr);
    }
    return is;
}

// TEXTURE_2D
class texture_2d final : public texture {
public:
    explicit texture_2d(std::string_view);
    ~texture_2d() override;
    uint8_t get_id() const final;
    void bind() const final;

private:
    GLuint texture_id = 0;
};

texture_2d::texture_2d(std::string_view file_name) {
    std::vector<unsigned char> png_file_in_memory;
    std::ifstream ifs(file_name.data(), std::ios_base::binary);
    if (!ifs) {
        throw std::runtime_error("Cannot load texture.");
    }
    ifs.seekg(0, std::ios_base::end);
    std::streamoff pos_in_file = ifs.tellg();
    png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
    ifs.seekg(0, std::ios_base::beg);
    if (!ifs) {
        throw std::runtime_error("Cannot load texture.");
    }

    ifs.read(reinterpret_cast<char *>(png_file_in_memory.data()), pos_in_file);
    if (!ifs.good()) {
        throw std::runtime_error("Cannot load texture.");
    }

    std::vector<unsigned char> image;
    unsigned long w = 0;
    unsigned long h = 0;
    int error = decodePNG(image, w, h, &png_file_in_memory[0],
                          png_file_in_memory.size(), false);

    if (error != 0) {
        std::cerr << "Error: " << error << std::endl;
        throw std::runtime_error("Cannot load texture.");
    }

    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GLint mipmap_level = 0;
    GLint border = 0;
    auto width = static_cast<GLsizei>(w);
    auto height = static_cast<GLsizei>(h);
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width, height, border,
                 GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);
}

texture::~texture() = default;
texture_2d::~texture_2d() { glDeleteTextures(1, &texture_id); }

uint8_t texture_2d::get_id() const { return texture_id; }

void texture_2d::bind() const { glBindTexture(GL_TEXTURE_2D, texture_id); }

// ENGINE_IMPL
class engine_impl final : public engine {
public:
    ~engine_impl() override;
    std::string initialize(std::string_view) final;
    float get_time() final;
    void set_uniform(std::string_view, const texture &) final;
    bool read_input(event &) final;
    texture *create_texture(std::string_view) final;
    void render(const triangle &) final;
    void render(const triangle_buffer &, const texture &) final;
    void swap_buffers() final;
    void uninitialize() final;

private:
    SDL_Window *window = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint program_id_ = 0;
};

static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr.");
    }
    delete e;
}

engine::~engine() = default;
engine_impl::~engine_impl() = default;

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }

    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        res = d;
    }

    return res;
}

static GLuint load_shader(const std::string &file_name, GLenum shader_type) {
    std::string line;
    std::ifstream file(file_name, std::ios::in);
    assert(!!file);
    unsigned long pos = file.tellg();
    file.seekg(0, std::ios::end);
    unsigned long len = file.tellg();
    file.seekg(std::ios::beg);
    if (len == 0) throw std::runtime_error("Loaded shader is empty!");

    auto *shader_src = (GLchar *)calloc(len + 1, 1);

    file.read(shader_src, len);
    file.close();

    GLuint shader = glCreateShader(shader_type);
    glShaderSource(shader, 1, &shader_src, nullptr);

    glCompileShader(shader);

    GLint compiled_status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled_status);

    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader, info_len, nullptr, info_chars.data());
        glDeleteShader(shader);

        std::string_view shader_type_str = "vertex";
        if (shader_type == GL_FRAGMENT_SHADER) shader_type_str = "fragment";

        std::cerr << "Error compiling " << shader_type_str
                  << " shader:" << std::endl
                  << shader << std::endl
                  << info_chars.data();
        throw std::runtime_error("Error compiling shader!");
    }

    return shader;
}

std::string engine_impl::initialize(std::string_view config) {
    config_map_t config_map = parse_config(config);

    using namespace std;
    stringstream serr;

    SDL_version compiled = {0, 0, 0};
    SDL_version linked = {0, 0, 0};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << err_message << endl;
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    int window_width = str_to_int(config_map["window_width"], 640);
    int window_height = str_to_int(config_map["window_height"], 480);

    window =
        SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, window_width, window_height,
                         ::SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("Cannot create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    const int gl_min_major_ver = 2;
    const int gl_min_minor_ver = 1;

    if (gl_major_ver <= gl_min_major_ver && gl_minor_ver < gl_min_minor_ver) {
        serr << "Current context opengl version: " << gl_major_ver << "."
             << gl_minor_ver << std::endl
             << "Need opengl version at least: " << gl_min_major_ver << "."
             << gl_min_minor_ver << std::endl
             << std::flush;
        return serr.str();
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
        load_gl_func("glBindBuffer", glBindBuffer);
        load_gl_func("glGenBuffers", glGenBuffers);
        load_gl_func("glGenVertexArrays", glGenVertexArrays);
        load_gl_func("glBindVertexArray", glBindVertexArray);
        load_gl_func("glBufferData", glBufferData);
        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniform1i", glUniform1i);
        load_gl_func("glDeleteBuffers", glDeleteBuffers);
        load_gl_func("glDisableVertexAttribArray", glDisableVertexAttribArray);
    } catch (std::exception &ex) {
        return ex.what();
    }

    GLuint vbo = 0;
    GLuint vao = 0;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);

    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLuint vertex_shader = load_shader("../shader.vert", GL_VERTEX_SHADER);
    GLuint fragment_shader = load_shader("../shader.frag", GL_FRAGMENT_SHADER);

    program_id_ = glCreateProgram();
    if (0 == program_id_) {
        serr << "Failed to create gl program.";
        return serr.str();
    }

    glAttachShader(program_id_, vertex_shader);
    glAttachShader(program_id_, fragment_shader);

    glLinkProgram(program_id_);
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    if (linked_status == 0) {
        GLint info_len = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> infoLog(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id_, info_len, nullptr, infoLog.data());
        serr << "Error linking program:" << std::endl << infoLog.data();
        glDeleteProgram(program_id_);
        return serr.str();
    }

    glUseProgram(program_id_);

    return "";
}

float engine_impl::get_time() { return SDL_GetTicks(); }

void engine_impl::set_uniform(std::string_view name, const texture &t) {
    GLint uniform_location = glGetUniformLocation(program_id_, name.data());
    glActiveTexture(GL_TEXTURE0);
    t.bind();
    glUniform1i(uniform_location, 0);
}

bool engine_impl::read_input(event &e) {
    SDL_Event sdl_event;

    if (SDL_PollEvent(&sdl_event)) {
        if (sdl_event.type == SDL_QUIT) {
            e = event::turn_off;
            return true;
        }
    }

    return false;
}

texture *engine_impl::create_texture(std::string_view file_name) {
    texture *t = new texture_2d(file_name);
    return t;
}

void engine_impl::render(const triangle &tr) {
    glBufferData(GL_ARRAY_BUFFER, sizeof(tr), &tr, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex);
    GLintptr position = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position));
    // r, g, b
    glEnableVertexAttribArray(1);
    position += sizeof(float) * 2;
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position));
    // u, v - texture
    glEnableVertexAttribArray(2);
    position += sizeof(float) * 3;
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void engine_impl::render(const triangle_buffer &tb, const texture &t) {
    set_uniform("texture_", t);

    glValidateProgram(program_id_);
    GLint validate_status = 0;
    glGetProgramiv(program_id_, GL_VALIDATE_STATUS, &validate_status);
    if (validate_status == GL_FALSE) {
        GLint info_len = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_log(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id_, info_len, nullptr, info_log.data());
        std::cerr << "Error linking program:" << std::endl << info_log.data();
        throw std::runtime_error("Error!");
    }

    for (auto triangle : tb) {
        render(triangle);
    }
}

void engine_impl::swap_buffers() {
    SDL_GL_SwapWindow(window);
    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void engine_impl::uninitialize() {
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace om