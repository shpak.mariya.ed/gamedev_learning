#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

namespace om {
enum class event { mouse_move, mouse_scroll, turn_off };

class engine;

OM_DECLSPEC engine *create_engine();
OM_DECLSPEC void destroy_engine(engine *e);

struct OM_DECLSPEC vertex {
    float x = 0.f;
    float y = 0.f;
    float z = 0.f;
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
};

struct OM_DECLSPEC triangle {
    triangle() {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

struct OM_DECLSPEC user_state {
    float x = 0.0; // mouse x
    float y = 0.0; // mouse y
    float z = 0.0; // mouse scroll
};

class OM_DECLSPEC engine {
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view config) = 0;
    virtual void update_user_state(const user_state &) = 0;
    virtual void set_uniform() = 0;
    virtual bool read_input(event &e) = 0;
    virtual void render_grid(float) = 0;
    virtual void render_triangle(const triangle &) = 0;
    virtual void swap_buffers() = 0;
    virtual void uninitialize() = 0;
};

} // end namespace om