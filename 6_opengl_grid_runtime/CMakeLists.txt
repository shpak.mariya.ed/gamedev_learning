cmake_minimum_required(VERSION 3.17)
project(opengl_grid_runtime)

set(CMAKE_CXX_STANDARD 17)

add_executable(opengl_grid_runtime
        engine.h
        engine.cpp
        game.cpp)

find_package(SDL2 REQUIRED)
set(SDL2_DIR /usr/local/lib/cmake/SDL2)

target_link_libraries(opengl_grid_runtime
        PRIVATE
        -lGL
        SDL2::SDL2
        SDL2::SDL2main)