#include "engine.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <charconv>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

#define OM_GL_CHECK()                                                          \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR) {                                              \
            switch (err) {                                                     \
            case GL_INVALID_ENUM:                                              \
                std::cerr << "GL_INVALID_ENUM" << std::endl;                   \
                break;                                                         \
            case GL_INVALID_VALUE:                                             \
                std::cerr << "GL_INVALID_VALUE" << std::endl;                  \
                break;                                                         \
            case GL_INVALID_OPERATION:                                         \
                std::cerr << "GL_INVALID_OPERATION" << std::endl;              \
                break;                                                         \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                             \
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;  \
                break;                                                         \
            case GL_OUT_OF_MEMORY:                                             \
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                  \
                break;                                                         \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;

template <typename T>
static void load_gl_func(const char *func_name, T &result) {
    void *gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("Cannot load GL function") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

namespace om {
static std::array<std::string_view, 17> event_names = {
    {"left_pressed", "left_released", "right_pressed", "right_released",
     "up_pressed", "up_released", "down_pressed", "down_released",
     "select_pressed", "select_released", "start_pressed", "start_released",
     "button1_pressed", "button1_released", "button2_pressed",
     "button2_released", "turn_off"}};

std::ostream &operator<<(std::ostream &stream, const event &e) {
    auto value = static_cast<std::uint32_t>(e);
    auto minimal = static_cast<std::uint32_t>(event::left_pressed);
    auto maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_names[value];
        return stream;
    } else {
        throw std::runtime_error("Too big event value!");
    }
}
static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream &operator>>(std::istream &is, vertex &v) {
    is >> v.x;
    is >> v.y;
    is >> v.z;
    return is;
}

std::istream &operator>>(std::istream &is, triangle &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

struct bind {
    bind(SDL_Keycode k, std::string_view s, event pressed, event released)
        : key(k), name(s), event_pressed(pressed), event_released(released) {}

    SDL_Keycode key;
    std::string_view name;
    event event_pressed;
    event event_released;
};

const std::array<bind, 8> keys{
    {{SDLK_w, "up", event::up_pressed, event::up_released},
     {SDLK_a, "left", event::left_pressed, event::left_released},
     {SDLK_s, "down", event::down_pressed, event::down_released},
     {SDLK_d, "right", event::right_pressed, event::right_released},
     {SDLK_LCTRL, "button1", event::button1_pressed, event::button1_released},
     {SDLK_SPACE, "button2", event::button2_pressed, event::button2_released},
     {SDLK_ESCAPE, "select", event::select_pressed, event::select_released},
     {SDLK_RETURN, "start", event::start_pressed, event::start_released}}};

static bool check_input(const SDL_Event &e, const bind *&result) {
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind &b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

class engine_impl final : public engine {
public:
    std::string initialize(std::string_view) final;
    bool read_input(event &) final;
    void render_triangle(const triangle &) final;
    void swap_buffers() final;
    void uninitialize() final;

private:
    SDL_Window *window = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint program_id_ = 0;
};
static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr.");
    }
    delete e;
}

engine::~engine() = default;

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }

    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        res = d;
    }

    return res;
}

static GLuint load_shader(const std::string &file_name, GLenum shader_type) {
    std::string line;
    std::ifstream file(file_name, std::ios::in);
    if (!file) throw std::runtime_error("Cannot load shader from file!");

    unsigned long pos = file.tellg();
    file.seekg(0, std::ios::end);
    unsigned long len = file.tellg();
    file.seekg(std::ios::beg);
    if (len == 0) throw std::runtime_error("Loaded shader is empty!");

    auto *shader_src = (GLchar *)calloc(len + 1, 1);

    file.read(shader_src, len);
    file.close();

    GLuint shader = glCreateShader(shader_type);
    OM_GL_CHECK()
    glShaderSource(shader, 1, &shader_src, nullptr);
    OM_GL_CHECK()

    glCompileShader(shader);
    OM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK()

    if (compiled_status == 0) {

        GLint info_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        OM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader, info_len, nullptr, info_chars.data());
        OM_GL_CHECK()
        glDeleteShader(shader);
        OM_GL_CHECK()

        std::string_view shader_type_str = "vertex";
        if (shader_type == GL_FRAGMENT_SHADER) shader_type_str = "fragment";

        std::cerr << "Error compiling " << shader_type_str
                  << " shader:" << std::endl
                  << shader << std::endl
                  << info_chars.data();
        throw std::runtime_error("Error compiling shader!");
    }

    return shader;
}

std::string engine_impl::initialize(std::string_view config) {
    config_map_t config_map = parse_config(config);

    using namespace std;
    stringstream serr;

    SDL_version compiled = {0, 0, 0};
    SDL_version linked = {0, 0, 0};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << err_message << endl;
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    int window_width = str_to_int(config_map["window_width"], 640);
    int window_height = str_to_int(config_map["window_height"], 480);

    window =
        SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, window_width, window_height,
                         ::SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        return serr.str();
    }

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("Cannot create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    const int gl_min_major_ver = 2;
    const int gl_min_minor_ver = 1;

    if (gl_major_ver <= gl_min_major_ver && gl_minor_ver < gl_min_minor_ver) {
        serr << "Current context opengl version: " << gl_major_ver << "."
             << gl_minor_ver << std::endl
             << "Need opengl version at least: " << gl_min_major_ver << "."
             << gl_min_minor_ver << std::endl
             << std::flush;
        return serr.str();
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
    } catch (std::exception &ex) {
        return ex.what();
    }

    GLuint vertex_shader = load_shader("../shader.vert", GL_VERTEX_SHADER);
    GLuint fragment_shader = load_shader("../shader.frag", GL_FRAGMENT_SHADER);

    program_id_ = glCreateProgram();
    OM_GL_CHECK()
    if (0 == program_id_) {
        serr << "Failed to create gl program.";
        return serr.str();
    }

    glAttachShader(program_id_, vertex_shader);
    OM_GL_CHECK()
    glAttachShader(program_id_, fragment_shader);
    OM_GL_CHECK()

    glBindAttribLocation(program_id_, 0, "a_position");
    OM_GL_CHECK()
    glLinkProgram(program_id_);
    OM_GL_CHECK()
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    OM_GL_CHECK()
    if (linked_status == 0) {
        GLint infoLen = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
        OM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
        OM_GL_CHECK()
        serr << "Error linking program:" << std::endl << infoLog.data();
        glDeleteProgram(program_id_);
        OM_GL_CHECK()
        return serr.str();
    }

    glUseProgram(program_id_);
    OM_GL_CHECK()

    glEnable(GL_DEPTH_TEST);
    // glDisable(GL_DEPTH_TEST);

    return "";
}

bool engine_impl::read_input(event &e) {
    SDL_Event sdl_event;

    if (SDL_PollEvent(&sdl_event)) {
        const bind *binding = nullptr;

        if (sdl_event.type == SDL_QUIT) {
            e = event::turn_off;
            return true;
        } else if (sdl_event.type == SDL_KEYDOWN) {
            if (check_input(sdl_event, binding)) {
                e = binding->event_pressed;
                return true;
            }
        } else if (sdl_event.type == SDL_KEYUP) {
            if (check_input(sdl_event, binding)) {
                e = binding->event_released;
                return true;
            }
        }
    }

    return false;
}

void engine_impl::render_triangle(const triangle &t) {
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex), &t.v[0]);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);
    OM_GL_CHECK()
    glValidateProgram(program_id_);
    OM_GL_CHECK()
    GLint validate_status = 0;
    glGetProgramiv(program_id_, GL_VALIDATE_STATUS, &validate_status);
    OM_GL_CHECK()
    if (validate_status == GL_FALSE) {
        GLint infoLen = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
        OM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
        OM_GL_CHECK()
        std::cerr << "Error linking program:" << std::endl << infoLog.data();
        throw std::runtime_error("error");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()
}

void engine_impl::swap_buffers() {
    SDL_GL_SwapWindow(window);

    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    OM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    OM_GL_CHECK()
}

void engine_impl::uninitialize() {
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace om