varying vec4 v_position;

void main() {
    float z = v_position.z;
    float def_green = 0.8f;
    if (z == 0.0) {
        gl_FragColor = vec4(0.0, def_green, 0.0, 1.0);
    } else {
        float green = def_green - (z * 0.5);
        gl_FragColor = vec4(0.0, green, 0.0, 1.0);
    }
}