#include <charconv>
#include <engine.h>
#include <iostream>

engine::engine() {
    window = nullptr;
    renderer = nullptr;
    gamepad = nullptr;
    running = false;
}

engine::~engine() = default;

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }
    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        // using default value unless convert string to int
        res = d;
    }
    return res;
}

bool engine::init(std::string_view conf) {
    // creating map from config string
    config_map_t config_map = parse_config(conf);

    int sdl_init = SDL_Init(SDL_INIT_EVERYTHING);
    if (sdl_init != 0) {
        std::cerr << "Unable to initialize SDL: " << SDL_GetError()
                  << std::endl;
        return false;
    }

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    int window_width = str_to_int(config_map["window_width"], 640);
    int window_height = str_to_int(config_map["window_height"], 480);

    window = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              window_width, window_height, 0);
    if (window == nullptr) {
        std::cerr << "Could not create window: " << SDL_GetError() << std::endl;
        return false;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
        std::cerr << "Could not create renderer: " << SDL_GetError()
                  << std::endl;
        return false;
    }

    running = true;

    return true;
}

void engine::destroy() {
    SDL_GameControllerClose(gamepad);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

bool engine::is_running() { return running; }

static void call_action(devices dev, Uint16 key, bool stat) {
    auto map = get_actions_map();
    auto it = map.find(std::pair(dev, key));
    if (it != map.end()) {
        it->second(stat);
    }
}

void engine::handle_event() {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
        // KEYBOARD
        case SDL_KEYDOWN:
            call_action(devices::keyboard, event.key.keysym.scancode, true);
            break;
        case SDL_KEYUP:
            call_action(devices::keyboard, event.key.keysym.scancode, false);
            break;
        // MOUSE
        case SDL_MOUSEBUTTONDOWN:
            call_action(devices::mouse, event.button.button, true);
            break;
        case SDL_MOUSEBUTTONUP:
            call_action(devices::mouse, event.button.button, false);
            break;
        // GAMEPAD
        case SDL_CONTROLLERDEVICEADDED:
            std::cout << "Gamepad added." << std::endl;
            gamepad = SDL_GameControllerOpen(event.cdevice.which);
            break;
        case SDL_CONTROLLERDEVICEREMOVED:
            std::cout << "Gamepad removed." << std::endl;
            SDL_GameControllerClose(gamepad);
            break;
        case SDL_CONTROLLERBUTTONDOWN:
            call_action(devices::gamepad, event.cbutton.button, true);
            break;
        case SDL_CONTROLLERBUTTONUP:
            call_action(devices::gamepad, event.cbutton.button, false);
            break;
        case SDL_QUIT:
            running = false;
            break;
        default:
            break;
        }
    }
}