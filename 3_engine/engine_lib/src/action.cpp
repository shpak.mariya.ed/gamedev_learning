#include <action.h>
#include <iostream>

void actions::attack(bool stat) { std::cout << "ATTACK: " << stat << std::endl; }
void actions::up(bool stat) { std::cout << "UP: " << stat << std::endl; }
void actions::down(bool stat) { std::cout << "DOWN: " << stat << std::endl; }
void actions::left(bool stat) { std::cout << "LEFT: " << stat << std::endl; }
void actions::right(bool stat) { std::cout << "RIGHT: " << stat << std::endl; }
void actions::pause(bool stat) { std::cout << "PAUSE: " << stat << std::endl; }

const actions_map_t &get_actions_map() {
    static actions_map_t map = {
        // KEYBOARD
        {{keyboard, SDL_SCANCODE_SPACE}, &actions::attack},
        {{keyboard, SDL_SCANCODE_W}, &actions::up},
        {{keyboard, SDL_SCANCODE_S}, &actions::down},
        {{keyboard, SDL_SCANCODE_A}, &actions::left},
        {{keyboard, SDL_SCANCODE_D}, &actions::right},
        {{keyboard, SDL_SCANCODE_ESCAPE}, &actions::pause},

        // MOUSE
        {{mouse, 1}, &actions::attack}, // LEFT BUTTON
        {{mouse, 2}, &actions::pause},  // MIDDLE BUTTON

        // GAMEPAD
        {{gamepad, SDL_CONTROLLER_BUTTON_A}, &actions::attack},
        {{gamepad, SDL_CONTROLLER_BUTTON_DPAD_UP}, &actions::up},
        {{gamepad, SDL_CONTROLLER_BUTTON_DPAD_DOWN}, &actions::down},
        {{gamepad, SDL_CONTROLLER_BUTTON_DPAD_LEFT}, &actions::left},
        {{gamepad, SDL_CONTROLLER_BUTTON_DPAD_RIGHT}, &actions::right},
        {{gamepad, SDL_CONTROLLER_BUTTON_BACK}, &actions::pause},
    };
    return map;
}