#pragma once
#include <action.h>
#include <string_view>

class engine {
    SDL_Window *window;
    SDL_Renderer *renderer;
    // TODO remove gamepad
    SDL_GameController *gamepad;
    // checking for loop's continue
    bool running;

public:
    engine();
    ~engine();

    bool init(std::string_view);
    void destroy();

    bool is_running();

    void handle_event();
};