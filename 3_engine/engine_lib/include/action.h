#pragma once
#include <SDL2/SDL.h>
#include <engine.h>
#include <map>

enum devices { keyboard, mouse, gamepad };

struct actions {
    static void attack(bool);
    static void up(bool);
    static void down(bool);
    static void left(bool);
    static void right(bool);
    static void pause(bool);
};
using action_method = void (*)(bool);

using actions_map_t = std::map<std::pair<devices, Uint16>, action_method>;
const actions_map_t &get_actions_map();