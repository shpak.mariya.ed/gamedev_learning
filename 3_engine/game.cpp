#include <cstdlib>
#include <engine.h>
#include <memory>

int main() {
    std::unique_ptr<engine> e = std::make_unique<engine>();

    // TODO add reading from config file
    bool is_init = e->init("title=GAME;window_width=640;window_height=480;");
    if (!is_init) return EXIT_FAILURE;

    while (e->is_running()) {
        e->handle_event();
    }

    e->destroy();
    return EXIT_SUCCESS;
}