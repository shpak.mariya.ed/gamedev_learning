role: main_hero
position: 0.0 0.0
direction: down
default_width: 0.23
default_texture: main_hero_move_down
textures_config_path: res/config/textures_main_hero.txt
health_textures_config_path: res/textures/lights.txt
health_position: 0.425 0.65
health: 5
damage: 25
speed: 0.5