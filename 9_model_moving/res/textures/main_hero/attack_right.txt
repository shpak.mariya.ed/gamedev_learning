id: attack_right_0
texture: main_hero_attack_right
top_left: 0 1
bottom_right: 0.2 0.8
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_1
texture: main_hero_attack_right
top_left: 0.2 1
bottom_right: 0.4 0.8
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_2
texture: main_hero_attack_right
top_left: 0.4 1
bottom_right: 0.6 0.8
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_3
texture: main_hero_attack_right
top_left: 0.6 1
bottom_right: 0.8 0.8
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_4
texture: main_hero_attack_right
top_left: 0.8 1
bottom_right: 1 0.8
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_5
texture: main_hero_attack_right
top_left: 0 0.8
bottom_right: 0.2 0.6
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_6
texture: main_hero_attack_right
top_left: 0.2 0.8
bottom_right: 0.4 0.6
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_7
texture: main_hero_attack_right
top_left: 0.4 0.8
bottom_right: 0.6 0.6
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_8
texture: main_hero_attack_right
top_left: 0.6 0.8
bottom_right: 0.8 0.6
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_9
texture: main_hero_attack_right
top_left: 0.8 0.8
bottom_right: 1 0.6
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_10
texture: main_hero_attack_right
top_left: 0 0.6
bottom_right: 0.2 0.4
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_11
texture: main_hero_attack_right
top_left: 0.2 0.6
bottom_right: 0.4 0.4
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_12
texture: main_hero_attack_right
top_left: 0.4 0.6
bottom_right: 0.6 0.4
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_13
texture: main_hero_attack_right
top_left: 0.6 0.6
bottom_right: 0.8 0.4
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_14
texture: main_hero_attack_right
top_left: 0.8 0.6
bottom_right: 1 0.4
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_15
texture: main_hero_attack_right
top_left: 0 0.4
bottom_right: 0.2 0.2
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_16
texture: main_hero_attack_right
top_left: 0.2 0.4
bottom_right: 0.4 0.2
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_17
texture: main_hero_attack_right
top_left: 0.4 0.4
bottom_right: 0.6 0.2
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_18
texture: main_hero_attack_right
top_left: 0.6 0.4
bottom_right: 0.8 0.2
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_19
texture: main_hero_attack_right
top_left: 0.8 0.4
bottom_right: 1 0.2
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13

id: attack_right_20
texture: main_hero_attack_right
top_left: 0 0.2
bottom_right: 0.2 0
scale: 1.15
move_by_y: -0.075
move_by_x: 0.13