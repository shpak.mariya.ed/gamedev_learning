health: 5
texture: lights
width: 0.5
top_left: 0 1
bottom_right: 1 0.8

health: 4
texture: lights
width: 0.5
top_left: 0 0.8
bottom_right: 1 0.6

health: 3
texture: lights
width: 0.5
top_left: 0 0.6
bottom_right: 1 0.4

health: 2
texture: lights
width: 0.5
top_left: 0 0.4
bottom_right: 1 0.2

health: 1
texture: lights
width: 0.5
top_left: 0 0.2
bottom_right: 1 0