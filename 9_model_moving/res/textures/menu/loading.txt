id: loading_0
texture: loading
top_left: 0 1
bottom_right: 0.166667 0.8

id: loading_1
texture: loading
top_left: 0.166667 1
bottom_right: 0.333333 0.8

id: loading_2
texture: loading
top_left: 0.333333 1
bottom_right: 0.5 0.8

id: loading_3
texture: loading
top_left: 0.5 1
bottom_right: 0.666667 0.8

id: loading_4
texture: loading
top_left: 0.666667 1
bottom_right: 0.833333 0.8

id: loading_5
texture: loading
top_left: 0.833333 1
bottom_right: 1 0.8

id: loading_6
texture: loading
top_left: 0 0.8
bottom_right: 0.166667 0.6

id: loading_7
texture: loading
top_left: 0.166667 0.8
bottom_right: 0.333333 0.6

id: loading_8
texture: loading
top_left: 0.333333 0.8
bottom_right: 0.5 0.6

id: loading_9
texture: loading
top_left: 0.5 0.8
bottom_right: 0.666667 0.6

id: loading_10
texture: loading
top_left: 0.666667 0.8
bottom_right: 0.833333 0.6

id: loading_11
texture: loading
top_left: 0.833333 0.8
bottom_right: 1 0.6

id: loading_12
texture: loading
top_left: 0 0.6
bottom_right: 0.166667 0.4

id: loading_13
texture: loading
top_left: 0.166667 0.6
bottom_right: 0.333333 0.4

id: loading_14
texture: loading
top_left: 0.333333 0.6
bottom_right: 0.5 0.4

id: loading_15
texture: loading
top_left: 0.5 0.6
bottom_right: 0.666667 0.4

id: loading_16
texture: loading
top_left: 0.666667 0.6
bottom_right: 0.833333 0.4

id: loading_17
texture: loading
top_left: 0.833333 0.6
bottom_right: 1 0.4

id: loading_18
texture: loading
top_left: 0 0.4
bottom_right: 0.166667 0.2

id: loading_19
texture: loading
top_left: 0.166667 0.4
bottom_right: 0.333333 0.2

id: loading_20
texture: loading
top_left: 0.333333 0.4
bottom_right: 0.5 0.2

id: loading_21
texture: loading
top_left: 0.5 0.4
bottom_right: 0.666667 0.2

id: loading_22
texture: loading
top_left: 0.666667 0.4
bottom_right: 0.833333 0.2

id: loading_23
texture: loading
top_left: 0.833333 0.4
bottom_right: 1 0.2

id: loading_24
texture: loading
top_left: 0 0.2
bottom_right: 0.166667 0

id: loading_25
texture: loading
top_left: 0.166667 0.2
bottom_right: 0.333333 0

id: loading_26
texture: loading
top_left: 0.333333 0.2
bottom_right: 0.5 0