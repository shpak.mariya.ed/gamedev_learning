id: attack_right_0
texture: skeleton_attack_right
top_left: 0 1
bottom_right: 0.25 0.666667

id: attack_right_1
texture: skeleton_attack_right
top_left: 0.25 1
bottom_right: 0.5 0.666667

id: attack_right_2
texture: skeleton_attack_right
top_left: 0.5 1
bottom_right: 0.75 0.666667

id: attack_right_3
texture: skeleton_attack_right
top_left: 0.75 1
bottom_right: 1 0.666667

id: attack_right_4
texture: skeleton_attack_right
top_left: 0 0.666667
bottom_right: 0.25 0.333333

id: attack_right_5
texture: skeleton_attack_right
top_left: 0.25 0.666667
bottom_right: 0.5 0.333333

id: attack_right_6
texture: skeleton_attack_right
top_left: 0.5 0.666667
bottom_right: 0.75 0.333333

id: attack_right_7
texture: skeleton_attack_right
top_left: 0.75 0.666667
bottom_right: 1 0.333333

id: attack_right_8
texture: skeleton_attack_right
top_left: 0 0.333333
bottom_right: 0.25 0

id: attack_right_9
texture: skeleton_attack_right
top_left: 0.25 0.333333
bottom_right: 0.5 0

id: attack_right_10
texture: skeleton_attack_right
top_left: 0.5 0.333333
bottom_right: 0.75 0

id: attack_right_11
texture: skeleton_attack_right
top_left: 0.75 0.333333
bottom_right: 1 0