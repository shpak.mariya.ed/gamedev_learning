#include <algorithm>
#include <iostream>

#include "animation_2d.h"
#include "counter.h"
#include "parser.h"

namespace game {
// DIGITS
void digits::load(std::string_view path, texture_ptr t) {
    texture = t;
    if (texture == nullptr) {
        std::cerr << "Cannot set digits texture - texture is nullptr!"
                  << std::endl;
        return;
    }
    // generation vaos by texture
    std::vector<om::config_map_t> config_maps =
        om::get_list_of_config_map_from_file(path);
    for (auto config_map : config_maps) {
        // value
        auto value_str = config_map["value"];
        if (!value_str.empty()) {
            int value = om::str_to_int(value_str);
            if (value < 0 || value > 10) {
                std::cerr << "Digit value is over than 10!" << std::endl;
                continue;
            }
            // top_left
            auto top_left_str = config_map["top_left"];
            std::vector<std::string> top_left_splitted =
                om::get_split_string(top_left_str, ' ');
            om::texture_position top_left = {
                om::str_to_float(top_left_splitted.at(0)),
                om::str_to_float(top_left_splitted.at(1))};
            // top_left
            auto bottom_right_str = config_map["bottom_right"];
            std::vector<std::string> bottom_right_splitted =
                om::get_split_string(bottom_right_str, ' ');
            om::texture_position bottom_right = {
                om::str_to_float(bottom_right_splitted.at(0)),
                om::str_to_float(bottom_right_splitted.at(1))};
            float ratio =
                (top_left.v - bottom_right.v) / (bottom_right.u - top_left.u);
            // vao generation
            vao_buffer vao =
                generate_vao_buffer(0.07f, generation_type::centered, ratio,
                                    {0.f, 0.f}, top_left, bottom_right);
            vaos.at(value) = vao;
        }
    }
}
std::vector<vao_buffer> digits::get_vaos(int v) const {
    if (texture == nullptr) {
        std::cerr << "Cannot get digits vao - texture hasn't been set up!"
                  << std::endl;
    }
    std::vector<int> digs;
    if (v == -1) digs.push_back(10);
    else if (v == 0) digs.push_back(v);
    else {
        while (v > 0) {
            digs.push_back(v % 10);
            v /= 10;
        }
    }
    std::vector<vao_buffer> out;
    auto find_vao = [this](int i) { return this->vaos.at(i); };
    std::transform(digs.begin(), digs.end(), std::back_inserter(out), find_vao);
    return out;
}
texture_ptr digits::get_texture() const {
    if (texture == nullptr) {
        std::cerr << "Cannot get digits texture - texture hasn't been set up!"
                  << std::endl;
    }
    return texture;
}

// COUNTER
void counter::load_icon(texture_ptr t) {
    texture = t;
    if (texture == nullptr) {
        std::cerr << "Cannot set enemy_icon texture - texture is nullptr!"
                  << std::endl;
        return;
    }
    // generation vao by texture
    vao = generate_vao_buffer(0.1f);
}
void counter::load_digits(std::string_view path, texture_ptr t) {
    dig.load(path, t);
}
texture_ptr counter::get_texture() const { return texture; }
vao_buffer counter::get_vao() const { return vao; }
texture_ptr counter::get_digit_texture() const { return dig.get_texture(); }
std::vector<vao_buffer> counter::get_digit_vaos(int v) const {
    return dig.get_vaos(v);
}
} // namespace game