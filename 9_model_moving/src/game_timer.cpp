#include <stdexcept>
#include <vector>

#include "animation_2d.h"
#include "engine.h"
#include "game_timer.h"

namespace game {
game_timer::game_timer() : timer(om::create_timer()) {
    if (timer.get() == nullptr)
        throw std::runtime_error("Error timer::timer initialization!");
}
game_timer::~game_timer() = default;
const timer_model &game_timer::get_model() const { return model; }
void game_timer::set_texture_size(float s) { model.texture_size = s; }
void game_timer::set_textures(texture_ptr day_texture,
                              texture_ptr night_texture) {
    model.day = day_texture;
    model.night = night_texture;
    // todo check day_ration == night_ration
    model.main_vao = generate_vao_buffer(model.texture_size);
}
void game_timer::set_time(const float sec) {
    timer->stop();
    timer->start(sec);
}
static void get_quarter_pos(uint8_t q, float hw, om::position &v_start,
                            om::position &v_top, om::position &v_end,
                            om::texture_position &t_start,
                            om::texture_position &t_top,
                            om::texture_position &t_end) {
    if (q == 0) {
        v_start = {0.f, hw};
        v_top = {hw, hw};
        v_end = {hw, 0.f};
        t_start = {0.5f, 1.f};
        t_end = {1.f, 0.5f};
        t_top = {1.f, 1.f};
    }
    if (q == 1) {
        v_start = {hw, 0.f};
        v_top = {hw, -hw};
        v_end = {0.f, -hw};
        t_start = {1.f, 0.5f};
        t_top = {1.f, 0.f};
        t_end = {0.5f, 0.f};
    }
    if (q == 2) {
        v_start = {0.f, -hw};
        v_top = {-hw, -hw};
        v_end = {-hw, 0.f};
        t_start = {0.5f, 0.f};
        t_top = {0.f, 0.f};
        t_end = {0.f, 0.5f};
    }
    if (q == 3) {
        v_start = {-hw, 0.f};
        v_top = {-hw, hw};
        v_end = {0.f, hw};
        t_start = {0.f, 0.5f};
        t_top = {0.f, 1.f};
        t_end = {0.5f, 1.f};
    }
}
static void push_quarter_triangles(std::vector<om::triangle_textured> *buffer,
                                   bool clockwise, uint8_t q, float hw,
                                   bool is_two_triangles,
                                   const om::position &vertex_point,
                                   const om::texture_position &texture_point) {
    om::position v_center = {0.f, 0.f};
    om::texture_position t_center = {0.5f, 0.5f};
    om::position v_start, v_top, v_end;
    om::texture_position t_start, t_top, t_end;
    get_quarter_pos(q, hw, v_start, v_top, v_end, t_start, t_top, t_end);

    om::triangle_textured tr0;
    tr0.v[0].pos = v_center;
    tr0.v[0].text_pos = t_center;
    if (clockwise) {
        tr0.v[1].pos = v_start;
        tr0.v[1].text_pos = t_start;
    } else {
        tr0.v[1].pos = v_end;
        tr0.v[1].text_pos = t_end;
    }
    tr0.v[2].pos = vertex_point;
    tr0.v[2].text_pos = texture_point;
    buffer->push_back(tr0);

    if (is_two_triangles) {
        om::triangle_textured tr1;
        if (clockwise) {
            tr1.v[0].pos = v_start;
            tr1.v[0].text_pos = t_start;
        } else {
            tr1.v[0].pos = v_end;
            tr1.v[0].text_pos = t_end;
        }
        tr1.v[1].pos = vertex_point;
        tr1.v[1].text_pos = texture_point;
        tr1.v[2].pos = v_top;
        tr1.v[2].text_pos = t_top;
        buffer->push_back(tr1);
    }
}
static void push_quarter_triangles(std::vector<om::triangle_textured> *buffer,
                                   uint8_t q, float hw) {
    om::position v_center = {0.f, 0.f};
    om::texture_position t_center = {0.5f, 0.5f};
    om::position v_start, v_top, v_end;
    om::texture_position t_start, t_top, t_end;
    get_quarter_pos(q, hw, v_start, v_top, v_end, t_start, t_top, t_end);

    om::triangle_textured tr0;
    tr0.v[0].pos = v_center;
    tr0.v[0].text_pos = t_center;
    tr0.v[1].pos = v_start;
    tr0.v[1].text_pos = t_start;
    tr0.v[2].pos = v_end;
    tr0.v[2].text_pos = t_end;
    buffer->push_back(tr0);

    om::triangle_textured tr1;
    tr1.v[0].pos = v_start;
    tr1.v[0].text_pos = t_start;
    tr1.v[1].pos = v_end;
    tr1.v[1].text_pos = t_end;
    tr1.v[2].pos = v_top;
    tr1.v[2].text_pos = t_top;
    buffer->push_back(tr1);
}
void game_timer::update() {
    float process = timer->get_percent();
    float half_width = model.texture_size / 2.f;
    bool is_two_triangles = false;
    bool after_top = false;
    om::position vertex_point;
    om::texture_position texture_point;
    // quarters
    uint8_t quarter = 0;
    if (process > 0.75f) quarter = 3;
    else if (process > 0.5f)
        quarter = 2;
    else if (process > 0.25f)
        quarter = 1;
    const float p = process - static_cast<float>(quarter) * 0.25f;
    if (p > 0.125f) after_top = true;

    if (quarter == 0 || quarter == 2) {
        vertex_point.x = half_width * p / 0.125f;
        vertex_point.y = half_width;
        texture_point.u = 0.5f + 0.5f * p / 0.125f;
        texture_point.v = 1.f;
        if (after_top) {
            is_two_triangles = true;
            vertex_point.y = half_width - (vertex_point.x - half_width);
            vertex_point.x = half_width;
            texture_point.v = 1.f - (texture_point.u - 1.f);
            texture_point.u = 1.f;
        }
        if (quarter == 2) {
            vertex_point.x *= -1.f;
            vertex_point.y *= -1.f;
            if (after_top) {
                texture_point.u = 0.f;
                texture_point.v = 1.f - texture_point.v;
            } else {
                texture_point.u = 1.f - texture_point.u;
                texture_point.v = 0.f;
            }
        }
    }

    if (quarter == 1 || quarter == 3) {
        vertex_point.x = half_width;
        vertex_point.y = (half_width * p / 0.125f) * -1.f;
        texture_point.u = 1.f;
        texture_point.v = 0.5f - 0.5f * p / 0.125f;
        if (after_top) {
            is_two_triangles = true;
            vertex_point.x = half_width + (vertex_point.y + half_width);
            vertex_point.y = -half_width;
            texture_point.u = texture_point.v + 1.f;
            texture_point.v = 0.f;
        }
        if (quarter == 3) {
            vertex_point.x *= -1.f;
            vertex_point.y *= -1.f;
            if (after_top) {
                texture_point.u = 1.f - texture_point.u;
                texture_point.v = 1.f;
            } else {
                texture_point.u = 0.f;
                texture_point.v = 1.f - texture_point.v;
            }
        }
    }

    std::vector<om::triangle_textured> process_buffer;
    std::vector<om::triangle_textured> main_buffer;
    push_quarter_triangles(&process_buffer, true, quarter, half_width,
                           is_two_triangles, vertex_point, texture_point);
    push_quarter_triangles(&main_buffer, false, quarter, half_width,
                           !is_two_triangles, vertex_point, texture_point);
    // pushing full quarters into the needed buffer
    for (int8_t i = 0; i <= 3; ++i) {
        if (i < quarter) {
            push_quarter_triangles(&process_buffer, i, half_width);
        } else if (i > quarter) {
            push_quarter_triangles(&main_buffer, i, half_width);
        }
    }

    model.process_vao = vao_buffer(process_buffer);
    model.main_vao = vao_buffer(main_buffer);
}
bool game_timer::is_working() {
    return (timer->get_state() != om::stop && !timer->is_expired());
}
bool game_timer::is_passed(float sec) const {
    return timer->get_duration_from_check_point() > sec;
}
void game_timer::save_check_point() { timer->set_check_point(); }
} // namespace game