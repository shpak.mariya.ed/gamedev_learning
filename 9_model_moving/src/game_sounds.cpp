#include <iostream>
#include <stdexcept>

#include "game_sounds.h"
#include "parser.h"

namespace game {
void game_sounds::load_from_file(om::engine *engine, std::string_view path) {
    om::config_map_t sounds_map = om::get_config_map_from_file(path);
    for (auto sound : sounds_map) {
        sounds.insert(
            std::pair(sound.first, engine->create_sound_buffer(sound.second)));
    }
}
sound_ptr game_sounds::get_sound(std::string name) const {
    auto it = sounds.find(name);
    if (it == sounds.end()) {
        std::cerr << "Cannot find sound with the name " << name << "!"
                  << std::endl
                  << "Try to add template into the sound paths file."
                  << std::endl;
        throw std::runtime_error("Error: Cannot set sound up!");
    }
    sound_ptr out = it->second;
    if (out == nullptr) {
        std::cerr << "Sound " << name << " is broken!" << std::endl;
        throw std::runtime_error("Error: Cannot set sound up!");
    }
    return out;
}
} // namespace game