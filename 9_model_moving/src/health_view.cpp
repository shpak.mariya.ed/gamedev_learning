#include <algorithm>
#include <iostream>
#include <stdexcept>

#include "health_view.h"

namespace game {
health_view::health_view() = default;
health_view::health_view(std::string_view path, const game_textures &textures) {
    std::vector<om::config_map_t> config_maps =
        om::get_list_of_config_map_from_file(path);
    for (auto config_map : config_maps) {
        health_view_model model;
        // health
        auto health = config_map["health"];
        if (!health.empty()) model.health = om::str_to_int(health);
        else {
            std::cerr << "Health_view: Cannot set up health!" << std::endl;
        }
        // texture
        auto texture = config_map["texture"];
        if (!texture.empty()) model.texture = textures.get_texture(texture);
        else {
            std::cerr << "Health_view: Cannot set up texture!" << std::endl;
        }
        // vao
        auto width = config_map["width"];
        float width_value;
        if (!width.empty()) width_value = om::str_to_float(width, model.width);
        else {
            width_value = model.width;
            std::cerr
                << "Health_view: Cannot set up width! Using default value: "
                << width_value << std::endl;
        }
        auto top_left = config_map["top_left"];
        auto bottom_right = config_map["bottom_right"];
        if (!top_left.empty() && !bottom_right.empty()) {
            // top_left
            std::vector<std::string> top_left_v =
                om::get_split_string(top_left, ' ');
            om::texture_position texture_top_left = {
                om::str_to_float(top_left_v.at(0)),
                om::str_to_float(top_left_v.at(1))};
            // bottom_right
            std::vector<std::string> bottom_right_v =
                om::get_split_string(bottom_right, ' ');
            om::texture_position texture_bottom_right = {
                om::str_to_float(bottom_right_v.at(0)),
                om::str_to_float(bottom_right_v.at(1))};
            // vao generation
            float dif_x = texture_bottom_right.u - texture_top_left.u;
            float dif_y = texture_top_left.v - texture_bottom_right.v;
            float ratio = dif_y / dif_x;
            model.vao = generate_vao_buffer(
                width_value, generation_type::centered, ratio, {0.f, 0.f},
                texture_top_left, texture_bottom_right);
        }
        views.push_back(std::make_unique<health_view_model>(model));
    }
}
health_view_model *health_view::get_model_by_health(int h) const {
    if (h <= 0) return nullptr;
    if (views.empty()) {
        std::cerr << "Health views hasn't been loaded! Use "
                     "health_textures_config_path to set it up!"
                  << std::endl;
        return nullptr;
    }
    for (auto &view : views) {
        if (view->health == h) return view.get();
    }
    std::cerr << "Model with " << h << " health hasn't been set up!"
              << std::endl;
    return nullptr;
}
const position &health_view::get_start_position() const { return pos; }
void health_view::set_start_position(const position &p) { pos = p; }
} // namespace game