#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <random>
#include <stdexcept>

#include "counter.h"
#include "game.h"
#include "game_objects.h"
#include "game_timer.h"
#include "gui.h"
#include "health_view.h"
#include "object_model.h"
#include "parser.h"

namespace game {
enum class game_state { menu, play, pause, game_over_win, game_over_lose };
enum class game_time { day, night };

struct background_model : object_model {
    texture_ptr day_texture = nullptr;
};

// GAME
class game_impl final : public game {
    bool is_still_playing = true;
    om::engine *engine = nullptr;
    game_state state = game_state::play;
    game_time times_of_day = game_time::night;
    game_menu menu;
    std::vector<std::unique_ptr<game_object>> game_objects;
    std::vector<enemy_prototype> enemy_prototypes;
    std::vector<game_object *> free_enemies;
    active_game_object *main_hero = nullptr;
    game_world_map world_map;
    game_textures textures;
    game_sounds sounds;
    background_model background;
    health_view health_views;
    counter free_enemies_counter;
    game_timer timer;
    float night_duration = 100.f;
    float day_duration = 100.f;
    // shaders id
    uint8_t background_shader_id;
    uint8_t progress_bar_shader_id;
    int lvl = 1;

    void start_night();
    void start_day();
    animation_map set_up_animation(std::string, float);
    bool generate_random_enemy();

public:
    ~game_impl() override;
    explicit game_impl(om::engine *);
    bool is_playing() const final;
    void set_up_menu(std::string_view) final;
    void set_up_game(std::string_view) final;
    void set_up_enemies(std::string_view) final;
    void set_up_background(std::string_view, std::string_view) final;
    void set_up_timer(std::string_view, std::string_view, float) final;
    void create_game_object(std::string_view) final;
    void finish_loading() final;
    // main loop methods
    void start() final;
    void read_event(const om::event &) final;
    void update() final;
    void render() final;
    // background
    void render_basic_layout();
    // game objects vector
    void render_game_objects();
    // health + timer + icons
    void render_final_layout();
};

static bool already_exist = false;
game *create_game(om::engine *e) {
    if (already_exist) {
        throw std::runtime_error("Game already exist.");
    }
    game *result = dynamic_cast<game *>(new game_impl(e));
    already_exist = true;
    return result;
}
void destroy_game(game *g) {
    if (!already_exist) {
        throw std::runtime_error("Game not created.");
    }
    if (g == nullptr) {
        throw std::runtime_error("Game is nullptr. Nothing to destroy!");
    }
    delete g;
}

game::~game() = default;
game_impl::~game_impl() = default;
game_impl::game_impl(om::engine *e) : engine(e) {
    if (engine == nullptr) throw std::runtime_error("Engine is nullptr!");
    // todo remote hardcode relative path
    background_shader_id =
        engine->create_shader("res/shaders/default/textured.vert",
                              "res/shaders/default/textured.frag");
    progress_bar_shader_id = engine->create_shader(
        "res/shaders/default/colored.vert", "res/shaders/default/colored.frag");
    menu = game_menu(engine, background_shader_id);
    background.vao = generate_vao_buffer(2.f);
}
bool game_impl::is_playing() const { return is_still_playing; }

// setting up
animation_map game_impl::set_up_animation(std::string path, float width) {
    animation_map out;
    om::config_map_t textures_config_map;
    textures_config_map = om::get_config_map_from_file(path);
    for (auto texture_pack : textures_config_map) {
        std::string animation_name = texture_pack.first;
        std::string animation_path = texture_pack.second;
        if (animation_path.empty()) {
            std::cerr << "Cannot find value of the " << animation_name
                      << " field!" << std::endl;
            throw std::runtime_error("Error: Cannot load textures pack!");
        }
        out.insert(std::make_pair(animation_name,
                                  animation_2d(animation_path, textures, width,
                                               engine->get_window_ratio())));
    }
    return out;
}
static int set_up_game_obj(om::config_map_t *config_map, std::string key,
                           int d) {
    auto value = (*config_map)[key];
    return om::str_to_int(value, d);
}
static float set_up_game_obj(om::config_map_t *config_map, std::string key,
                             float d) {
    auto value = (*config_map)[key];
    return om::str_to_float(value, d);
}
void game_impl::set_up_menu(std::string_view path) { menu.set_up(path); }
void game_impl::set_up_game(std::string_view path) {
    om::config_map_t config_map = om::get_config_map_from_file(path);
    // timer setting up
    night_duration =
        set_up_game_obj(&config_map, "night_duration", night_duration);
    day_duration = set_up_game_obj(&config_map, "day_duration", day_duration);
    if (night_duration < 0.f) {
        std::cerr << "The value " << night_duration
                  << " as the night_duration is not correct!" << std::endl;
        throw std::runtime_error("Error: Cannot set timer duration up!");
    }
    if (day_duration < 0.f) {
        std::cerr << "The value " << day_duration
                  << " as the day_duration is not correct!" << std::endl;
        throw std::runtime_error("Error: Cannot set timer duration up!");
    }
    // textures loading
    auto textures_path = config_map["textures_path"];
    if (textures_path.empty()) {
        std::cerr << "Cannot find \"textures_path\" field" << std::endl;
        throw std::runtime_error("Error: Cannot set texture paths up!");
    } else {
        textures.load_from_file(engine, textures_path);
    }
    // sounds loading
    auto sounds_path = config_map["sounds_path"];
    if (sounds_path.empty()) {
        std::cerr << "Cannot find \"sounds_path\" field" << std::endl;
        throw std::runtime_error("Error: Cannot set sounds paths up!");
    } else {
        sounds.load_from_file(engine, sounds_path);
    }
    // custom walls loading
    auto custom_wall_top_path = config_map["custom_wall_top_path"];
    if (!custom_wall_top_path.empty())
        world_map.set_custom_walls(custom_wall_top_path, direction::up);
    auto custom_wall_bottom_path = config_map["custom_wall_bottom_path"];
    if (!custom_wall_bottom_path.empty())
        world_map.set_custom_walls(custom_wall_bottom_path, direction::down);
    auto custom_wall_left_path = config_map["custom_wall_left_path"];
    if (!custom_wall_left_path.empty())
        world_map.set_custom_walls(custom_wall_left_path, direction::left);
    auto custom_wall_right_path = config_map["custom_wall_right_path"];
    if (!custom_wall_right_path.empty())
        world_map.set_custom_walls(custom_wall_right_path, direction::right);
    // digits set up
    auto digits_config_path = config_map["digits_config_path"];
    texture_ptr digits_texture = textures.get_texture("digits");
    if (!digits_config_path.empty()) {
        // todo remove hardcode
        free_enemies_counter.load_icon(textures.get_texture("enemy_icon"));
        free_enemies_counter.load_digits(digits_config_path, digits_texture);
    }
    // getting map exit
    auto world_map_exit = config_map["world_map_exit"];
    auto world_map_exit_pos = om::get_split_string(world_map_exit, ' ');
    if (world_map_exit_pos.size() == 2) {
        position pos;
        pos.x = om::str_to_float(world_map_exit_pos.at(0));
        pos.y = om::str_to_float(world_map_exit_pos.at(1));
        world_map.set_exit(pos);
    }
}
void game_impl::set_up_enemies(std::string_view path) {
    std::vector<om::config_map_t> config_maps =
        om::get_list_of_config_map_from_file(path);
    for (auto config_map : config_maps) {
        enemy_prototype p;
        // model: default_width
        auto default_width = config_map["default_width"];
        if (!default_width.empty()) {
            p.model.width = om::str_to_float(default_width);
        }
        // model: default_texture
        auto default_texture = config_map["default_texture"];
        if (default_texture.empty()) {
            std::cerr << "Cannot find \"default_texture\" field!" << std::endl;
        } else {
            p.model.texture = textures.get_texture(default_texture);
            float animation_texture_ratio =
                p.model.texture->get_scale_rate(engine->get_window_ratio());
            p.model.vao =
                generate_vao_buffer(p.model.width, generation_type::to_top,
                                    animation_texture_ratio);
        }
        // textures_config_path
        auto textures_config_path = config_map["textures_config_path"];
        if (!textures_config_path.empty()) {
            p.animations =
                set_up_animation(textures_config_path, p.model.width);
        }
        // characteristics: health, speed
        auto health = config_map["health"];
        if (!health.empty()) p.chars.initial_health = om::str_to_int(health);
        auto damage = config_map["damage"];
        if (!damage.empty()) p.chars.damage = om::str_to_int(damage);
        auto speed = config_map["speed"];
        if (!speed.empty()) p.chars.speed = om::str_to_float(speed);
        enemy_prototypes.push_back(p);
    }
}
void game_impl::set_up_background(std::string_view night,
                                  std::string_view day) {
    if (engine == nullptr) {
        throw std::runtime_error("Engine is null!");
    }
    background.texture = textures.get_texture(night.data());
    background.day_texture = textures.get_texture(day.data());
}
void game_impl::set_up_timer(std::string_view day_texture_name,
                             std::string_view night_texture_name, float size) {
    if (engine == nullptr) {
        throw std::runtime_error("Engine is null!");
    }
    timer.set_texture_size(size);
    texture_ptr day_texture_ptr = textures.get_texture(day_texture_name.data());
    texture_ptr night_texture_ptr =
        textures.get_texture(night_texture_name.data());
    timer.set_textures(day_texture_ptr, night_texture_ptr);
}
void game_impl::create_game_object(std::string_view path) {
    std::vector<om::config_map_t> config_maps =
        om::get_list_of_config_map_from_file(path);
    for (auto config_map : config_maps) {
        // type
        obj_type type = obj_type::passive;
        auto type_str = config_map["type"];
        auto role_str = config_map["role"];
        if (type_str == "active" || role_str == "main_hero" ||
            role_str == "enemy") {
            type = obj_type::active;
        }
        if (type == obj_type::active)
            game_objects.push_back(std::make_unique<active_game_object>());
        else
            game_objects.push_back(std::make_unique<game_object>());
        game_object *obj = game_objects.back().get();
        if (obj == nullptr) {
            throw std::runtime_error("Error: Cannot create game object.");
        }
        // setting roles
        if (role_str == "spawn") obj->set_role(obj_role::spawn);
        if (role_str == "main_hero") obj->set_role(obj_role::main_hero);
        if (role_str == "enemy") obj->set_role(obj_role::enemy);
        // position
        auto pos_str = config_map["position"];
        std::vector<std::string> pos_vector =
            om::get_split_string(pos_str, ' ');
        if (pos_vector.size() == 2) {
            position pos;
            position current_pos = obj->get_position();
            pos.x = om::str_to_float(pos_vector.at(0), current_pos.x);
            pos.y = om::str_to_float(pos_vector.at(1), current_pos.y);
            obj->set_position(pos);
        }
        // model: default_width
        auto width = obj->get_model_width();
        width = set_up_game_obj(&config_map, "default_width", width);
        obj->set_model_width(width);
        // model: default_texture
        auto default_texture = config_map["default_texture"];
        if (default_texture.empty()) {
            std::cerr << "Cannot find \"default_texture\" field!" << std::endl;
        } else {
            texture_ptr default_texture_ptr =
                textures.get_texture(default_texture);
            obj->set_default_texture(default_texture_ptr,
                                     engine->get_window_ratio());
        }
        // textures_config_path
        auto textures_config_path = config_map["textures_config_path"];
        if (!textures_config_path.empty()) {
            obj->set_up_animations(
                set_up_animation(textures_config_path, obj->get_model_width()));
        }
        // health_textures_config_path, health_position - for main_hero only
        if (obj->get_role() == obj_role::main_hero) {
            auto health_textures_config_path =
                config_map["health_textures_config_path"];
            if (!health_textures_config_path.empty()) {
                health_views =
                    health_view(health_textures_config_path, textures);
            }
            auto health_position = config_map["health_position"];
            std::vector<std::string> health_position_v =
                om::get_split_string(health_position, ' ');
            if (health_position_v.size() == 2) {
                position pos;
                pos.x = om::str_to_float(health_position_v.at(0));
                pos.y = om::str_to_float(health_position_v.at(1));
                health_views.set_start_position(pos);
            }
        }
        // only for active game objects
        if (type == obj_type::active) {
            active_game_object *act_obj =
                dynamic_cast<active_game_object *>(obj);
            bool is_main_hero = act_obj->get_role() == obj_role::main_hero;
            if (is_main_hero) main_hero = act_obj;
            // direction
            auto dir_str = config_map["direction"];
            if (!dir_str.empty()) {
                if (dir_str == "up") act_obj->set_direction(direction::up);
                else if (dir_str == "down")
                    act_obj->set_direction(direction::down);
                else if (dir_str == "left")
                    act_obj->set_direction(direction::left);
                else if (dir_str == "right")
                    act_obj->set_direction(direction::right);
                else
                    std::cerr << "direction: " << dir_str << " is unknown!"
                              << std::endl;
            }
            act_obj->update_animation();
            // characteristics: health, damage, speed
            auto current_chars = act_obj->get_characteristics();
            characteristics chars;
            chars.initial_health = set_up_game_obj(
                &config_map, "health", current_chars.initial_health);
            chars.damage =
                set_up_game_obj(&config_map, "damage", current_chars.damage);
            chars.speed =
                set_up_game_obj(&config_map, "speed", current_chars.speed);
            act_obj->set_characteristics(chars);
        }
    }
}
void game_impl::finish_loading() { menu.finish_loading(); }
bool game_impl::generate_random_enemy() {
    if (enemy_prototypes.size() == 0) {
        throw std::runtime_error("Error: Cannot find enemies prototype! Use "
                                 "game->set_up_enemy() before game starts.");
    }
    std::vector<game_object *> spawns;
    float x_offset = -0.05f;
    float y_offset = -0.05f;
    for (auto &obj : game_objects) {
        position possible_pos = obj->get_position();
        possible_pos.x += x_offset;
        possible_pos.y += y_offset;
        if (obj->get_role() == obj_role::spawn &&
            obj->get_state() != obj_state::free &&
            world_map.is_empty_place(possible_pos)) {
            spawns.push_back(obj.get());
        }
    }
    int spawns_count = static_cast<int>(spawns.size());
    if (spawns.empty()) return false;
    // getting random spawn
    std::random_device r;
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, spawns_count - 1);
    int random_index = uniform_dist(e1);
    game_object *current_spawn = spawns.at(random_index);
    // now is only one type of enemies
    game_objects.push_back(
        std::make_unique<active_game_object>(enemy_prototypes.at(0)));
    position pos = current_spawn->get_position();
    pos.x += x_offset;
    pos.y += y_offset;
    game_objects.back()->set_position(pos);
    // sound effect
    sounds.get_sound("rising")->play();
    return true;
}
// main loop methods
void game_impl::start() {
    state = game_state::menu;
    menu.set_state(menu_state::start);
}
void game_impl::start_night() {
    times_of_day = game_time::night;
    timer.set_time(night_duration);
    // sound effect
    sounds.get_sound("day")->stop();
    sounds.get_sound("night")->play(
        om::sound_buffer::sound_playing_type::looped);
    // new wave - new free enemies
    free_enemies.clear();
    // spawns blessing
    if (main_hero->is_alive() && lvl != 1) {
        std::vector<game_object *> not_blessed_spawns;
        for (auto &obj : game_objects) {
            if (obj->get_role() == obj_role::spawn &&
                obj->get_state() != obj_state::free) {
                not_blessed_spawns.push_back(obj.get());
            }
        }
        int spawns_count = static_cast<int>(not_blessed_spawns.size());
        if (not_blessed_spawns.empty()) {
            std::cout << "GAME OVER! WINNER!" << std::endl;
            state = game_state::menu;;
            sounds.get_sound("night")->stop();
            menu.set_state(menu_state::final_win);
        } else if (spawns_count == 1) {
            not_blessed_spawns.at(0)->act(obj_state::free, true);
            std::cout << "GAME OVER! WINNER!" << std::endl;
            state = game_state::menu;
            sounds.get_sound("night")->stop();
            menu.set_state(menu_state::final_win);
        }
        // getting random spawn
        std::random_device r;
        std::default_random_engine e1(r());
        std::uniform_int_distribution<int> uniform_dist(0, spawns_count - 1);
        int random_index = uniform_dist(e1);
        game_object *current_spawn = not_blessed_spawns.at(random_index);
        current_spawn->act(obj_state::free, true);
        // sound effect
        sounds.get_sound("blessing")->play();
    }
}
void game_impl::start_day() {
    ++lvl;
    times_of_day = game_time::day;
    timer.set_time(day_duration);
    // sound effect
    sounds.get_sound("night")->stop();
    sounds.get_sound("day")->play(om::sound_buffer::sound_playing_type::looped);
    // killing enemies
    for (auto &obj : game_objects) {
        obj_state obj_state = obj->get_state();
        if (obj->get_role() == obj_role::enemy &&
            obj_state != obj_state::free && obj_state != obj_state::dying) {
            obj->act(obj_state::dying, true);
        }
    }
    // main_hero -health
    int damage = static_cast<int>(free_enemies.size());
    std::cout << "Free enemies: " << damage << std::endl;
    main_hero->take_damage(damage);
    // final?
    if (!main_hero->is_alive()) {
        std::cout << "GAME OVER! LOSER!" << std::endl;
        state = game_state::menu;
        menu.set_state(menu_state::final_lose);
        sounds.get_sound("day")->stop();
    }
}
void game_impl::read_event(const om::event &e) {
    if (e.key == om::turn_off) {
        is_still_playing = false;
        return;
    }
    if (state == game_state::menu) is_still_playing = menu.read_event(e);
    else {
        if (main_hero == nullptr) {
            std::cerr << "Main hero hasn't been set up. Use "
                         "game_ptr->set_main_hero(active_game_object_ptr);"
                      << std::endl;
            throw std::runtime_error("Error: Cannot find main hero!");
        }
        switch (e.key) {
        case om::space:
            main_hero->act(obj_state::attack, e.is_pressed);
            break;
        case om::up:
            main_hero->act(obj_state::move, e.is_pressed, direction::up);
            break;
        case om::down:
            main_hero->act(obj_state::move, e.is_pressed, direction::down);
            break;
        case om::left:
            main_hero->act(obj_state::move, e.is_pressed, direction::left);
            break;
        case om::right:
            main_hero->act(obj_state::move, e.is_pressed, direction::right);
            break;
        default:
            break;
        }
    }
}
void game_impl::update() {
    if (state == game_state::menu) {
        menu.update();
        if (menu.get_state() == menu_state::off) {
            state = game_state::play;
            start_night();
        }
    }
    if (state == game_state::play) {
        // 1. updating game_objects
        for (auto &obj : game_objects) {
            obj->update(world_map);
        }
        // 2. clearing dead enemies from the list
        auto is_dead_enemy = [](const std::unique_ptr<game_object> &obj) {
            return (obj->get_role() == obj_role::enemy &&
                    obj->get_state() == obj_state::dead);
        };
        game_objects.erase(std::remove_if(game_objects.begin(),
                                          game_objects.end(), is_dead_enemy),
                           game_objects.end());
        // 3. updating map by the objects list
        world_map.update(&game_objects);
        // 4. looking for free enemies
        for (auto &obj : game_objects) {
            if (obj->get_role() == obj_role::enemy &&
                obj->get_state() == obj_state::free) {
                game_object *obj_ptr = obj.get();
                auto is_new_free_enemy = [obj_ptr](game_object *enemy) {
                    return enemy == obj_ptr;
                };
                // todo fix not correct free enemies calculaton
                if (!std::any_of(free_enemies.begin(), free_enemies.end(),
                                 is_new_free_enemy)) {
                    free_enemies.push_back(obj_ptr);
                    // sound effect
                    sounds.get_sound("scream")->play();
                }
            }
        }
        // 5. timer checking
        if (timer.is_working()) {
            // every 5 seconds an enemy appears
            // truly each update if it hasn't been possible to rise enemy last
            // update
            if (times_of_day == game_time::night && timer.is_passed(5.f) &&
                generate_random_enemy()) {
                timer.save_check_point();
            }
        } else {
            if (times_of_day == game_time::night) start_day();
            else
                start_night();
        }
        // 5. sorting for the rendering
        auto compare_by_y = [](const std::unique_ptr<game_object> &left,
                               const std::unique_ptr<game_object> &right) {
            return (left->get_position().y > right->get_position().y);
        };
        std::sort(game_objects.begin(), game_objects.end(), compare_by_y);
    }
}
void game_impl::render() {
    if (state == game_state::menu) menu.render();
    else {
        render_basic_layout();
        render_game_objects();
        render_final_layout();
    }
    engine->swap_buffers();
}
void game_impl::render_basic_layout() {
    engine->use_shader(background_shader_id);
    if (times_of_day == game_time::night) {
        engine->render(background.vao, background.texture);
    } else {
        engine->render(background.vao, background.day_texture);
    }
    // main_hero health
    int main_hero_health = main_hero->get_health();
    health_view_model *health_model =
        health_views.get_model_by_health(main_hero_health);
    if (health_model != nullptr) {
        texture_ptr health_texture = health_model->texture;
        position health_position = health_views.get_start_position();
        om::matrix_2x3 matrix =
            om::matrix_2x3::move({health_position.x, health_position.y});

        engine->use_default_shader();
        engine->render(health_model->vao, matrix, health_texture);
    }
}
void game_impl::render_game_objects() {
    for (auto &&obj : game_objects) {
        // object rendering
        auto vao = obj->get_vao_buffer();
        const obj_role obj_role = obj->get_role();
        const obj_state obj_state = obj->get_state();
        texture_ptr texture;
        if (obj_role == obj_role::spawn && obj_state == obj_state::free) {
            // todo remove hack
            texture = textures.get_texture("grave_blessed");
        } else {
            texture = obj->get_texture();
        }
        position pos = obj->get_position();
        om::matrix_2x3 matrix = om::matrix_2x3::move({pos.x, pos.y});
        if (obj_role == obj_role::enemy && obj_state == obj_state::free) {
            float freedom_duration = obj->get_freedom_duration();
            float trans_rate = 1.f - freedom_duration / 2.f;
            om::matrix_2x3 scale_matrix = om::matrix_2x3::scale(trans_rate);
            matrix =
                om::matrix_2x3::move({pos.x, pos.y + freedom_duration * 0.1f});
            matrix = scale_matrix * matrix;
        }
        engine->use_default_shader();
        engine->render(*vao, matrix, texture);
        // health bar rendering (enemies only)
        if (obj_role == obj_role::enemy && obj_state != obj_state::free) {
            active_game_object *active_obj =
                dynamic_cast<active_game_object *>(obj.get());
            float health_percent = active_obj->get_health_percent();
            float model_width = world_map.get_default_model_width_ndc();
            vao_buffer_colored progress_vao = generate_progress_bar(
                {pos.x, pos.y}, model_width, health_percent);
            engine->use_shader(progress_bar_shader_id);
            engine->render(progress_vao);
        }
    }
}
void game_impl::render_final_layout() {
    float window_ratio = engine->get_window_ratio();
    // timer rendering
    timer.update();
    texture_ptr timer_current_texture = timer.get_model().night;
    texture_ptr timer_process_texture = timer.get_model().day;
    if (times_of_day == game_time::day) {
        std::swap(timer_process_texture, timer_current_texture);
    }
    if (timer_current_texture != nullptr && timer_process_texture != nullptr) {
        // important day_texture.size === night_texture.size
        float scale_rate = timer_current_texture->get_scale_rate(window_ratio);
        om::matrix_2x3 scale = om::matrix_2x3::scale({1.f, scale_rate});
        om::matrix_2x3 moving = om::matrix_2x3::move({-0.9f, 0.83f});
        om::matrix_2x3 matrix = scale * moving;

        engine->use_default_shader();
        engine->render(timer.get_model().main_vao, matrix,
                       timer_current_texture);
        engine->render(timer.get_model().process_vao, matrix,
                       timer_process_texture);
    }
    // free_enemies_counter rendering
    int free_enemies_count = static_cast<int>(free_enemies.size());
    texture_ptr digits_texture = free_enemies_counter.get_digit_texture();
    float scale_rate = digits_texture->get_scale_rate(window_ratio);
    std::vector<vao_buffer> digits_vaos =
        free_enemies_counter.get_digit_vaos(free_enemies_count);
    om::matrix_2x3 scale = om::matrix_2x3::scale({1.f, scale_rate});
    om::matrix_2x3 moving = om::matrix_2x3::move({0.78f, 0.87f});
    om::matrix_2x3 matrix = scale * moving;
    // todo update counter view to 99+ possible free enemies
    if (digits_vaos.size() > 0){
        engine->use_default_shader();
        engine->render(digits_vaos.at(0), matrix, digits_texture);
    }
    if (digits_vaos.size() == 2) {
        moving = om::matrix_2x3::move({0.7f, 0.87f});
        matrix = scale * moving;
        engine->render(digits_vaos.at(1), matrix, digits_texture);
    }
    std::vector<vao_buffer> x_vao = free_enemies_counter.get_digit_vaos(-1);
    scale = om::matrix_2x3::scale({1.f, scale_rate});
    moving = om::matrix_2x3::move({0.85f, 0.87f});
    matrix = scale * moving;

    engine->use_default_shader();
    engine->render(x_vao.at(0), matrix, digits_texture);
    // counter icon
    texture_ptr enemy_icon = free_enemies_counter.get_texture();
    if (enemy_icon != nullptr) {
        vao_buffer enemy_icon_vao = free_enemies_counter.get_vao();
        scale_rate = enemy_icon->get_scale_rate(window_ratio);
        scale = om::matrix_2x3::scale({1.f, scale_rate});
        moving = om::matrix_2x3::move({0.93f, 0.87f});
        matrix = scale * moving;

        engine->use_default_shader();
        engine->render(enemy_icon_vao, matrix, enemy_icon);
    }
}
} // namespace game