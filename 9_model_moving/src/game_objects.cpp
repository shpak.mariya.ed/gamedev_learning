#include <algorithm>
#include <iostream>
#include <stdexcept>

#include "game_objects.h"
#include "parser.h"

namespace game {
static std::string get_action_name(const std::string &, const direction &);
// for enemy path finding
static bool check_path(const direction &, const position &, const position &);
static direction find_path(const direction &, const position &,
                           const game_world_map &);

// GAME_OBJECT
game_object::game_object() : timer(om::create_timer()) {
    if (model.vao.buffer().empty()) generate_vao_buffer(model.width);
}
game_object::~game_object() = default;
const position &game_object::get_position() const { return pos; }
const obj_role &game_object::get_role() const { return role; }
const obj_state &game_object::get_state() const { return state; }
texture_ptr game_object::get_texture() const {
    if (current_animation == nullptr) {
        if (model.texture != nullptr) return model.texture;
        else
            throw std::runtime_error("Cannot find texture or animation!");
    }
    return current_animation->get_current_sprite()->texture;
}
const vao_buffer *game_object::get_vao_buffer() const {
    if (current_animation == nullptr) return &(model.vao);
    return &(current_animation->get_current_sprite()->vao);
}
float game_object::get_model_width() const { return model.width; }
float game_object::get_freedom_duration() const {
    if (state != obj_state::free) return 0.f;
    return timer->get_duration();
}
void game_object::set_position(const position &p) { pos = p; }
void game_object::set_role(const obj_role &r) { role = r; }
void game_object::set_model_width(float w) { model.width = w; }
void game_object::set_default_texture(texture_ptr t, float window_ratio) {
    model.texture = t;
    float animation_texture_ratio = t->get_scale_rate(window_ratio);
    model.vao = generate_vao_buffer(model.width, generation_type::to_top,
                                    animation_texture_ratio);
}
void game_object::set_up_animations(const animation_map &m) { animations = m; }
void game_object::act(const obj_state &s, bool start, const direction &d) {
    if (s == obj_state::stay || s == obj_state::free) state = s;
    else {
        std::cerr << "Cannot do action for the passive game_object!"
                  << std::endl;
    }
}
void game_object::update(const game_world_map &) {}
void game_object::take_damage(int) {}

// GAME_WORLD_MAP
game_world_map::game_world_map()
    : game_object_width_ndc(static_cast<float>(game_object_width) * 2.f /
                            static_cast<float>(map_x)),
      step_x(2.f / map_x), step_y(2.f / map_y) {}
void game_world_map::get_current_figure(std::vector<size_t> *v,
                                        const game_object *cur_obj) const {
    // finding current index (in the game_objects list)
    uint8_t current_index;
    auto it_index =
        std::find(game_objects.begin(), game_objects.end(), cur_obj);
    if (it_index != game_objects.end()) {
        current_index = it_index - game_objects.begin();
    } else {
        throw std::runtime_error("Error: Cannot find current active "
                                 "game_object into the objects list!");
    }
    // finding indexes of the object figure
    for (size_t i = 0; i < map_size; ++i) {
        std::vector<uint8_t> cell = world.at(i);
        if (std::any_of(cell.begin(), cell.end(),
                        [current_index](uint8_t obj_id) {
                            return obj_id == current_index;
                        })) {
            v->push_back(i);
        }
    }
    uint8_t needed_count = game_object_width * 3;
    if (v->size() != needed_count) {
        std::cerr << "Needed size - " << static_cast<int>(needed_count)
                  << std::endl
                  << "Only " << v->size() << " elements have been found!"
                  << std::endl;
        throw std::runtime_error(
            "Error: Cannot find all figure elements (in the world map)");
    }
}
void game_world_map::fill_world() {
    // by default, map is empty
    // if cell.size == 0 -> empty cell
    // clearing worldn map
    std::for_each(world.begin(), world.end(),
                  [](std::vector<uint8_t> &v) { v.clear(); });
    // creating default walls (box)
    auto push_index = [](std::vector<uint8_t> &v) { v.push_back(0); };
    std::for_each(world.begin(), world.begin() + map_x, push_index); // top
    std::for_each(world.end() - map_x, world.end(), push_index);     // bottom
    for (size_t i = 0; i < map_y; ++i) {
        world.at(i * map_x).push_back(0);             // left
        world.at(i * map_x + map_x - 1).push_back(0); // right
    }
    // adding walls by map config
    for (auto wall : custom_walls) {
        direction wall_pos = wall.first;
        std::vector<float> w = wall.second;
        for (size_t i = 0; i < w.size(); ++i) {
            // todo add for all sides
            if (wall_pos == direction::up || wall_pos == direction::down) {
                float y = w.at(i);
                size_t y_in_map =
                    static_cast<size_t>(y * static_cast<float>(map_y));
                if (wall_pos == direction::up) {
                    for (size_t j = 0; j <= y_in_map; ++j) {
                        world.at(j * map_x + i).push_back(0);
                    }
                } else {
                    // y_in_map + 2 - visual hack (animation offset fixing)
                    for (size_t j = y_in_map + 2; j < map_y; ++j) {
                        world.at(j * map_x + i).push_back(0);
                    }
                }
            }
        }
    }
}
float game_world_map::get_map_step_x() const { return 1.f / map_x; }
float game_world_map::get_map_step_y() const { return 1.f / map_y; }
float game_world_map::get_default_model_width_ndc() const {
    return game_object_width_ndc;
}
const position &game_world_map::get_exit_pos() const { return exit_pos; }
void game_world_map::set_custom_walls(std::string_view path,
                                      const direction &dir) {
    // config file has format [0;1], where (0;0) is left top vertex
    // it works only for horizontal walls (from 0 to 1)
    // wall map initialization
    custom_walls.insert(std::pair(dir, std::vector<float>{}));
    // getting file source
    std::string file_src = om::read_file(path);
    std::vector<std::string> file_by_lines =
        om::get_split_string(file_src, '\n');
    // filling
    float start_x = 0.f;
    size_t i = 0;
    for (auto line : file_by_lines) {
        if (!line.empty()) {
            std::vector<std::string> v = om::get_split_string(line, ' ');
            float x = om::str_to_float(v.at(0));
            float current_x = start_x + i * step_x / 2.f;
            if (x > current_x) {
                ++i;
                custom_walls.at(dir).push_back(om::str_to_float(v.at(1)));
            }
        }
    }
}
void game_world_map::set_exit(const position &pos) { exit_pos = pos; }
void game_world_map::update(std::vector<std::unique_ptr<game_object>> *objs) {
    // reserving 0 index as default map value (custom wall)
    // 0 - wall
    game_objects.clear();
    game_objects.push_back(nullptr);
    auto get_pointer = [](const std::unique_ptr<game_object> &o) {
        return o.get();
    };
    std::transform(objs->begin(), objs->end(), std::back_inserter(game_objects),
                   get_pointer);
    // the world_map filling
    fill_world();
    /* generated cells:
     *
     *   _#######_     -|
     *   _###*###_      | game_object_height = 3
     *   _#######_     -|
     *
     *    |_____|
     * game_object_width
     *
     * _ - empty or wall (0 or 1 as a default value)
     * # - generated uint (that is the index in the indexes list)
     * * - start position of object (value is the same #) */

    // start by 1 cause of 0 is reserved
    for (uint8_t i = 1; i < game_objects.size(); ++i) {
        game_object *current_obj = game_objects.at(i);
        if (current_obj == nullptr) {
            throw std::runtime_error(
                "Error: the current game_object is nullptr!");
        }
        const position pos = current_obj->get_position();
        position top_left_pos;
        top_left_pos.x = pos.x - game_object_width_ndc / 2.f;
        top_left_pos.y = pos.y + 0.03f / 2.f;
        size_t row =
            static_cast<size_t>(map_y - (top_left_pos.y + 1.f) * map_y / 2.f);
        size_t col = static_cast<size_t>((top_left_pos.x + 1.f) * map_x / 2.f);
        size_t map_index = row * map_x + col;
        auto it = world.begin() + map_index;
        auto push_index = [i](std::vector<uint8_t> &v) { v.push_back(i); };
        std::for_each(it, it + game_object_width, push_index);
        it += map_x;
        std::for_each(it, it + game_object_width, push_index);
        it += map_x;
        std::for_each(it, it + game_object_width, push_index);
    }
}
bool game_world_map::is_collision(const game_object *cur_obj,
                                  direction &dir) const {
    // getting each cell offset (by moving direction)
    int cell_offset = 0;
    // clang-format off
    if (dir == direction::up) cell_offset = static_cast<int>(map_x) * -1;
    else if (dir == direction::down) cell_offset = static_cast<int>(map_x);
    else if (dir == direction::left) cell_offset = -1;
    else if (dir == direction::right) cell_offset = 1;
    // clang-format on
    // getting current index and figure
    std::vector<size_t> current_figure;
    get_current_figure(&current_figure, cur_obj);
    // checking for the collision
    // barrier:
    //   - wall
    //   - spawn
    //   - alive enemies (for main_hero only)
    //   - main_hero (for enemies)
    auto is_barrier = [this, cur_obj](uint8_t i) {
        // if it's wall
        if (i == 0) return true;
        const obj_role cur_obj_role = cur_obj->get_role();
        game_object *target_obj = this->game_objects.at(i);
        const obj_role target_role = target_obj->get_role();
        const obj_state target_state = target_obj->get_state();
        // if it's spawn
        return ((target_role == obj_role::spawn) ||
                // if there're main_hero and alive enemy
                (cur_obj_role == obj_role::main_hero &&
                 target_role == obj_role::enemy &&
                 target_state != obj_state::dying &&
                 target_state != obj_state::dead) ||
                (cur_obj_role == obj_role::enemy &&
                 target_role == obj_role::main_hero));
    };
    for (auto cell : current_figure) {
        size_t next_cell_index =
            static_cast<size_t>(static_cast<int>(cell) + cell_offset);
        std::vector<uint8_t> next_cell = world.at(next_cell_index);
        if (next_cell.size() != 0) {
            if (std::any_of(next_cell.begin(), next_cell.end(), is_barrier)) {
                return true;
            }
        }
    }
    return false;
}
game_object *game_world_map::is_attack(const game_object *cur_obj,
                                       direction &dir) const {
    // getting current position
    const position cur_pos = cur_obj->get_position();
    // searching for the target
    for (auto obj : game_objects) {
        // obj could be nullptr in index = 0 (wall)
        if (obj != nullptr && obj != cur_obj) {
            // getting target position
            const position pos = obj->get_position();
            // checking direction
            bool is_current_dir = true;
            // clang-format off
            if (dir == direction::up) is_current_dir = (cur_pos.y < pos.y);
            else if (dir == direction::down) is_current_dir = (cur_pos.y > pos.y);
            else if (dir == direction::left) is_current_dir = (cur_pos.x > pos.x);
            else if (dir == direction::right) is_current_dir = (cur_pos.x < pos.x);
            // clang-format on
            if (!is_current_dir) continue;
            // getting difference
            float x_dif = std::abs(pos.x - cur_pos.x);
            float y_dif = std::abs(pos.y - cur_pos.y);
            // +30% range to x
            float x_range = static_cast<float>(game_object_width) /
                            static_cast<float>(map_x) * 2.f * 1.3f;
            // +54% range to y
            float y_range = 3.f / static_cast<float>(map_y) * 2.f * 1.54f;
            // checking range conditions
            if (x_dif < x_range && y_dif < y_range) return obj;
        }
    }
    return nullptr;
}
bool game_world_map::is_empty_place(const position &pos) const {
    position top_left_pos;
    top_left_pos.x = pos.x - game_object_width_ndc / 2.f;
    top_left_pos.y = pos.y + 0.03f / 2.f;
    size_t row =
        static_cast<size_t>(map_y - (top_left_pos.y + 1.f) * map_y / 2.f);
    size_t col = static_cast<size_t>((top_left_pos.x + 1.f) * map_x / 2.f);
    size_t map_index = row * map_x + col;
    auto it = world.begin() + map_index;
    auto is_busy = [this](const std::vector<uint8_t> &v) {
        for (uint8_t i : v) {
            game_object *obj = this->game_objects.at(i);
            obj_role role = obj->get_role();
            // block spawn if the main_hero is here or
            // if any enemy is rising here now
            if (role == obj_role::main_hero ||
                (role == obj_role::enemy &&
                 obj->get_state() == obj_state::stay))
                return true;
        }
        return false;
    };
    if (std::any_of(it, it + game_object_width, is_busy)) return false;
    it += map_x;
    if (std::any_of(it, it + game_object_width, is_busy)) return false;
    it += map_x;
    return !std::any_of(it, it + game_object_width, is_busy);
}

// ACTIVE_GAME_OBJECT
active_game_object::active_game_object() {
    start_pos = pos;
    role = obj_role::enemy;
    act(state, true, dir);
}
active_game_object::active_game_object(const enemy_prototype &p) {
    start_pos = pos;
    role = obj_role::enemy;
    // from prototype
    model = p.model;
    set_up_animations(p.animations);
    chars = p.chars;
    act(state, true, dir);
}
active_game_object::~active_game_object() = default;
const characteristics &active_game_object::get_characteristics() const {
    return chars;
}
int active_game_object::get_health() const { return chars.health; }
float active_game_object::get_health_percent() const {
    float out = static_cast<float>(chars.health) /
                static_cast<float>(chars.initial_health);
    return out;
}
void active_game_object::set_characteristics(const characteristics &c) {
    chars = c;
    if (role == obj_role::main_hero) {
        chars.health = chars.initial_health;
    } else if (role == obj_role::enemy) {
        // because of health will be increased by the timer
        chars.health = 0;
    }
}
void active_game_object::set_direction(const direction &d) { dir = d; }
void active_game_object::act(const obj_state &s, bool start,
                             const direction &d) {
    // clang-format off
    if (s == obj_state::stay) stay();
    else if (s == obj_state::move) move(d, start);
    else if (s == obj_state::attack) attack(start);
    else if (s == obj_state::dying) die();
    // clang-format on
}
void active_game_object::update(const game_world_map &world_map) {
    using namespace std::chrono;
    steady_clock::time_point current_time = steady_clock::now();

    // moving
    if (state == obj_state::move) {
        duration<float> pasted_time =
            duration_cast<duration<float>>(current_time - start_time);
        const float way = chars.speed * (pasted_time.count() - waiting_time);
        // checking for the collision
        bool is_collision = false;
        int no_checked_way = 0;
        if (dir == direction::up || dir == direction::down) {
            no_checked_way = static_cast<int>(way / world_map.get_map_step_y());
        } else if (dir == direction::left || dir == direction::right) {
            no_checked_way = static_cast<int>(way / world_map.get_map_step_x());
        }
        if (no_checked_way > checked_way) {
            checked_way = no_checked_way;
            is_collision = world_map.is_collision(this, dir);
        }
        if (!is_collision) {
            // clang-format off
            if (dir == direction::left) pos.x = start_pos.x - way;
            else if (dir == direction::right) pos.x = start_pos.x + way;
            else if (dir == direction::up) pos.y = start_pos.y + way;
            else if (dir == direction::down) pos.y = start_pos.y - way;
            // clang-format on
            if (role == obj_role::enemy) {
                const position target_pos = world_map.get_exit_pos();
                if (pos == target_pos) {
                    if (dir != direction::up) {
                        auto it = animations.find("move_up");
                        if (it == animations.end()) current_animation = nullptr;
                        else {
                            current_animation = &it->second;
                            current_animation->start();
                        }
                    }
                    state = obj_state::free;
                    timer->start(2.f);
                } else if (!check_path(dir, pos, target_pos)) {
                    direction new_dir = find_path(dir, pos, world_map);
                    move(new_dir, true);
                }
            }
        } else {
            if (role == obj_role::main_hero) {
                start_waiting_time = steady_clock::now();
                state = obj_state::wait;
            } else if (role == obj_role::enemy) {
                direction new_dir = find_path(dir, pos, world_map);
                move(new_dir, true);
            }
        }
    }
    // waiting
    else if (state == obj_state::wait) {
        duration<float> pasted_waiting_time =
            duration_cast<duration<float>>(current_time - start_waiting_time);
        // is collision still exist
        if (!world_map.is_collision(this, dir)) {
            waiting_time += pasted_waiting_time.count();
            state = obj_state::move;
        }
    }
    // attack
    else if (state == obj_state::attack) {
        duration<float> pasted_from_last_attack_time =
            duration_cast<duration<float>>(current_time - last_attack_time);
        if (pasted_from_last_attack_time.count() > chars.attack_speed) {
            // updating last_attack_time
            last_attack_time = steady_clock::now();
            // looking for targets
            game_object *target = world_map.is_attack(this, dir);
            if (target != nullptr) {
                if (target->get_role() == obj_role::enemy) {
                    target->take_damage(chars.damage);
                }
            }
        }
    }
    // enemies rising
    else if (state == obj_state::stay) {
        if (role == obj_role::enemy) {
            duration<float> pasted_time =
                duration_cast<duration<float>>(current_time - start_time);
            // appearing time = 5s
            float pasted_time_by_rising_time = pasted_time.count() / 5.f;
            chars.health =
                static_cast<int>(pasted_time_by_rising_time *
                                 static_cast<float>(chars.initial_health)) -
                total_damage;
            // animation lock
            float animation_percent =
                (float)chars.health / float(chars.initial_health);
            current_animation->lock_on_one_sprite(animation_percent);
            if (chars.health == chars.initial_health) {
                direction new_dir = find_path(dir, pos, world_map);
                move(new_dir, true);
            }
        }
    }
    // dying
    else if (state == obj_state::dying || state == obj_state::free) {
        if (timer->is_expired()) {
            timer->stop();
            state = obj_state::dead;
        }
    }
}
void active_game_object::update_animation() {
    std::string action_name;
    if (state == obj_state::stay) {
        action_name = get_action_name("idle", dir);
    } else if (state == obj_state::move) {
        action_name = get_action_name("move", dir);
    } else if (state == obj_state::attack) {
        action_name = get_action_name("attack", dir);
    } else if (state == obj_state::dying) {
        action_name = "dying";
    }
    auto it = animations.find(action_name);
    if (it == animations.end()) current_animation = nullptr;
    else {
        current_animation = &it->second;
        current_animation->start();
    }
}
void active_game_object::take_damage(int d) {
    if (state == obj_state::dead || state == obj_state::dying) return;
    chars.health -= d;
    total_damage += d;
    if (chars.health <= 0) die();
}
bool active_game_object::is_alive() const {
    return state != obj_state::dead && state != obj_state::dying;
}
void active_game_object::stay() {
    state = obj_state::stay;
    // saving time for staying duration check
    start_time = std::chrono::steady_clock::now();
    // setting animation
    update_animation();
}
void active_game_object::move(const direction &d, bool start) {
    if (!start) {
        if (state == obj_state::move && dir != d) return;
        stay();
        return;
    }
    state = obj_state::move;
    start_time = std::chrono::steady_clock::now();
    start_pos = pos;
    waiting_time = 0;
    dir = d;
    // checked_way should be -1 for calling is_collision()
    // in the update method (when compare with current way)
    checked_way = -1;
    // setting animation
    update_animation();
}
void active_game_object::attack(bool start) {
    if (!start) {
        stay();
        return;
    }
    state = obj_state::attack;
    // setting animation
    update_animation();
}
void active_game_object::die() {
    state = obj_state::dying;
    // setting timer
    timer->start(3);
    // setting animation
    update_animation();
}

// others
static bool is_equal(float l, float r) { return (std::abs(l - r) < 0.01f); }
bool operator==(const position &l, const position &r) {
    return (is_equal(l.x, r.x) && is_equal(l.y, r.y));
}
static std::string get_action_name(const std::string &action_name,
                                   const direction &d) {
    std::string direction_name;
    if (d == direction::up) direction_name = "up";
    if (d == direction::down) direction_name = "down";
    if (d == direction::left) direction_name = "left";
    if (d == direction::right) direction_name = "right";
    std::string out = action_name + '_' + direction_name;
    return out;
}
static void get_target_direction(const position &pos,
                                 const position &target_pos,
                                 direction &dir_by_x, direction &dir_by_y) {
    // clang-format off
    if (pos.x < target_pos.x) dir_by_x = direction::right;
    else dir_by_x = direction::left;
    if (pos.y < target_pos.y) dir_by_y = direction::up;
    else dir_by_y = direction::down;
    // clang-format on
}
static bool check_path(const direction &dir, const position &pos,
                       const position &target_pos) {
    // getting target directions
    direction dir_by_x, dir_by_y;
    get_target_direction(pos, target_pos, dir_by_x, dir_by_y);
    // checking
    bool out = true;
    if (dir == dir_by_x) {
        out = !(dir == dir_by_x && is_equal(pos.x, target_pos.x));
    } else if (dir == dir_by_y) {
        out = !(dir == dir_by_y && is_equal(pos.y, target_pos.y));
    }
    return out;
}
static direction find_path(const direction &old_dir, const position &pos,
                           const game_world_map &world_map) {
    // todo add recursive best way finding!
    // getting target directions
    const position target_pos = world_map.get_exit_pos();
    direction dir_by_x, dir_by_y;
    get_target_direction(pos, target_pos, dir_by_x, dir_by_y);
    // getting new direction
    direction dir;
    // clang-format off
    if (old_dir == dir_by_x) dir = dir_by_y;
    else dir = dir_by_x;
    // clang-format on
    return dir;
}
} // namespace game