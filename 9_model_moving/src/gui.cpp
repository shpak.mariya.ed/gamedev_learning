#include <algorithm>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "animation_2d.h"
#include "gui.h"
#include "matrix.h"
#include "parser.h"

namespace game {
void menu_field::set_up(om::config_map_t &config_map,
                        const game_textures &textures) {
    auto texture = config_map["texture"];
    if (texture.empty()) {
        std::cerr << "Cannot load menu field texture!" << std::endl;
    } else {
        field_model.texture = textures.get_texture(texture);
        float ratio = field_model.texture->get_scale_rate(1.778f);
        field_model.vao =
            generate_vao_buffer(1.1f, generation_type::centered, ratio);
    }
}
std::vector<button> menu_field::get_buttons(const menu_state &s) const {
    std::vector<button> out;
    for (button b : buttons) {
        button_action action = b.action;
        if (s == menu_state::start) {
            if (action == button_action::start ||
                action == button_action::exit) {
                out.push_back(b);
            }
        } else if (s == menu_state::final_win || s == menu_state::final_lose) {
            if (action == button_action::retry ||
                action == button_action::exit) {
                out.push_back(b);
            }
        }
    }
    return out;
}
button_action menu_field::get_selected_action() const {
    button_action out = button_action::none;
    for (auto b : buttons) {
        if (b.state == button_state::hover) out = b.action;
    }
    return out;
}
void menu_field::select_next_button() {
    for (size_t i = 0; i < buttons.size(); ++i) {
        button &b = buttons.at(i);
        if (i != buttons.size() - 1 && b.state == button_state::hover) {
            b.state = button_state::normal;
            buttons.at(i + 1).state = button_state::hover;
        }
    }
}
void menu_field::select_prev_button() {
    for (size_t i = 0; i < buttons.size(); ++i) {
        button &b = buttons.at(i);
        if (i != 0 && b.state == button_state::hover) {
            b.state = button_state::normal;
            buttons.at(i - 1).state = button_state::hover;
        }
    }
}
const object_model &menu_field::get_field_model() const { return field_model; }
void menu_field::create_button(om::config_map_t &config_map,
                               const game_textures &textures) {
    button b;
    // action
    auto action = config_map["action"];
    if (action == "start") {
        b.action = button_action::start;
    } else if (action == "exit") {
        b.action = button_action::exit;
    } else if (action == "retry") {
        b.action = button_action::retry;
    }
    // texture
    auto texture = config_map["texture"];
    if (!texture.empty()) b.texture = textures.get_texture(texture);
    else {
        std::cerr << "Cannot create button texture!" << std::endl;
    }
    // vao_buffer generating
    float ratio = b.texture->get_scale_rate(1.778f);
    // position
    auto position = config_map["position"];
    auto position_v = om::get_split_string(position, ' ');
    if (position_v.size() == 2) {
        b.pos.x = om::str_to_float(position_v.at(0));
        b.pos.y = om::str_to_float(position_v.at(1));
    }
    // normal vao_buffer
    std::vector<std::string> button_names = {"normal", "clicked", "hover"};
    for (auto n : button_names) {
        auto top_left_str = config_map[n + "_top_left"];
        std::vector<std::string> top_left_splitted =
            om::get_split_string(top_left_str, ' ');
        om::texture_position top_left = {
            om::str_to_float(top_left_splitted.at(0)),
            om::str_to_float(top_left_splitted.at(1))};
        auto bottom_right_str = config_map[n + "_bottom_right"];
        std::vector<std::string> bottom_right_splitted =
            om::get_split_string(bottom_right_str, ' ');
        om::texture_position bottom_right = {
            om::str_to_float(bottom_right_splitted.at(0)),
            om::str_to_float(bottom_right_splitted.at(1))};

        vao_buffer vao =
            generate_vao_buffer(b.width, generation_type::centered, ratio,
                                {b.pos.x, b.pos.y}, top_left, bottom_right);
        // clang-format off
        if (n == "normal") b.normal_vao = vao;
        else if (n == "clicked") b.clicked_vao = vao;
        else if (n == "hover") b.hover_vao = vao;
        // clang-format on
    }
    // pushing
    buttons.push_back(b);
    // sorting by y
    auto is_higher = [](const button &left, const button &right) {
        return left.pos.y > right.pos.y;
    };
    std::sort(buttons.begin(), buttons.end(), is_higher);
    buttons.at(0).state = button_state::hover;
}

game_menu::game_menu() = default;
game_menu::game_menu(om::engine *e, uint8_t shader_id)
    : engine(e), background_shader_id(shader_id) {
    if (engine == nullptr) {
        throw std::runtime_error(
            "Error: Cannot initialize menu - engine is null!");
    }
}
void game_menu::set_up(std::string_view path) {
    if (engine == nullptr) throw std::runtime_error("Menu: Engine is nullptr!");
    std::vector<om::config_map_t> config_maps =
        om::get_list_of_config_map_from_file(path);
    for (auto config_map : config_maps) {
        // type
        auto type = config_map["type"];
        if (type.empty()) {
            auto textures_list_path = config_map["textures_list_path"];
            if (textures_list_path.empty()) {
                throw std::runtime_error(
                    "Error menu init: Cannot find textures_list_path!");
            } else {
                textures.load_from_file(engine, textures_list_path);
            }
            auto sounds_list_path = config_map["sounds_list_path"];
            if (sounds_list_path.empty()) {
                std::cerr << "Warning menu init: Cannot find sounds_list_path!"
                          << std::endl;
            } else {
                sounds.load_from_file(engine, sounds_list_path);
            }
        } else if (type == "button") {
            field.create_button(config_map, textures);
        } else if (type == "menu_field") {
            field.set_up(config_map, textures);
        } else if (type == "menu") {
            // texture_back
            auto texture_back = config_map["texture_back"];
            background_model.texture = textures.get_texture(texture_back);
            background_model.vao = generate_vao_buffer(2.f);
            // texture_logo
            auto texture_logo = config_map["texture_logo"];
            logo_model.texture = textures.get_texture(texture_logo);
            float ratio = logo_model.texture->get_scale_rate(1.778f);
            logo_model.vao =
                generate_vao_buffer(1.2f, generation_type::centered, ratio);
            // texture_lose
            auto texture_lose = config_map["texture_lose"];
            lose_model.texture = textures.get_texture(texture_lose);
            ratio = lose_model.texture->get_scale_rate(1.778f);
            lose_model.vao =
                generate_vao_buffer(0.77f, generation_type::centered, ratio);
            // texture_win
            auto texture_win = config_map["texture_win"];
            win_model.texture = textures.get_texture(texture_win);
            ratio = win_model.texture->get_scale_rate(1.778f);
            win_model.vao =
                generate_vao_buffer(0.75f, generation_type::centered, ratio);
            // loading
            float window_ratio = engine->get_window_ratio();
            auto loading_config_path = config_map["loading_config_path"];
            loading_animation.load_sprites_from_file(
                loading_config_path, textures, 0.3f, window_ratio);
            loading_animation.start();
            // main_hero_animations
            auto main_hero_path = config_map["main_hero_config_path"];
            main_hero_animation.load_sprites_from_file(main_hero_path, textures,
                                                       0.5f, window_ratio);
            auto main_hero_lose_path = config_map["main_hero_lose_config_path"];
            main_hero_lose_animation.load_sprites_from_file(
                main_hero_lose_path, textures, 0.5f, window_ratio);
            auto main_hero_win_path = config_map["main_hero_win_config_path"];
            main_hero_win_animation.load_sprites_from_file(
                main_hero_win_path, textures, 0.5f, window_ratio);
        }
    }
    std::cout << "ENDED MENU SETTING UP" << std::endl;
}
const menu_state &game_menu::get_state() const { return state; }
void game_menu::set_state(const menu_state &s) {
    state = s;
    // if start - start music
    // if off - stop all music
    if (state == menu_state::start) {
        main_hero_animation.start();
        sounds.get_sound("main")->play(
            om::sound_buffer::sound_playing_type::looped);
    } else if (state == menu_state::final_lose) {
        main_hero_lose_animation.start_once();
        sounds.get_sound("lose")->play();
    } else if (state == menu_state::final_win) {
        main_hero_win_animation.start();
        sounds.get_sound("win")->play();
    } else if (state == menu_state::off) {
        sounds.get_sound("main")->stop();
    }
}
void game_menu::finish_loading() {
    std::cout << "GAME LOADED!" << std::endl;
    is_game_loading = false;
}

bool game_menu::read_event(const om::event &e) {
    if (e.key == om::enter) {
        button_action current_action = field.get_selected_action();
        if (current_action == button_action::exit) return false;
        else if (!is_game_loading && (current_action == button_action::start ||
                                      current_action == button_action::retry)) {
            set_state(menu_state::off);
        }
    }
    // clang-format off
    else if (e.key == om::up) field.select_prev_button();
    else if (e.key == om::down) field.select_next_button();
    // clang-format on
    return true;
}
void game_menu::update() {}
void game_menu::render() {
    // background
    engine->use_shader(background_shader_id);
    engine->render(background_model.vao, background_model.texture);
    // main_hero
    const sprite *main_hero_sprite = nullptr;
    if (state == menu_state::start) {
        main_hero_sprite = main_hero_animation.get_current_sprite();
    } else if (state == menu_state::final_lose) {
        main_hero_sprite = main_hero_lose_animation.get_current_sprite();
    } else if (state == menu_state::final_win) {
        main_hero_sprite = main_hero_win_animation.get_current_sprite();
    }
    if (main_hero_sprite != nullptr) {
        om::matrix_2x3 matrix = om::matrix_2x3::move({-0.65f, -0.7f});
        engine->use_default_shader();
        engine->render(main_hero_sprite->vao, matrix,
                       main_hero_sprite->texture);
    }
    // menu_field
    object_model field_model = field.get_field_model();
    om::matrix_2x3 matrix = om::matrix_2x3::move({0.35f, -0.05f});
    engine->use_default_shader();
    engine->render(field_model.vao, matrix, field_model.texture);
    // buttons
    std::vector<button> buttons = field.get_buttons(state);
    for (button b : buttons) {
        button_state bs = b.state;
        // clang-format off
        engine->use_shader(background_shader_id);
        if (bs == button_state::normal) engine->render(b.normal_vao, b.texture);
        else if (bs == button_state::clicked) engine->render(b.clicked_vao, b.texture);
        else if (bs == button_state::hover) engine->render(b.hover_vao, b.texture);
        // clang-format on
    }
    // logo/win/lose
    matrix = om::matrix_2x3::move({0.f, 0.75f});
    engine->use_default_shader();
    if (state == menu_state::start) {
        engine->render(logo_model.vao, matrix, logo_model.texture);
    } else if (state == menu_state::final_lose) {
        engine->render(lose_model.vao, matrix, lose_model.texture);
    } else if (state == menu_state::final_win) {
        engine->render(win_model.vao, matrix, win_model.texture);
    }
    // loading animation
    if (is_game_loading) {
        const sprite *loading_sprite = loading_animation.get_current_sprite();
        matrix = om::matrix_2x3::move({0.8f, -0.95f});
        engine->render(loading_sprite->vao, matrix, loading_sprite->texture);
    }
}
} // namespace game