layout (location = 0) in vec2 a_pos;
layout (location = 1) in vec2 a_text_pos;

out vec2 out_text_pos;

uniform mat3 matrix_;

void main() {
    vec3 final_pos = matrix_ * vec3(a_pos, 1.f);

    out_text_pos = a_text_pos;
    gl_Position = vec4(final_pos, 1.f);
}