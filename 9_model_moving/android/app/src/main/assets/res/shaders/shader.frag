in vec2 out_text_pos;
out vec4 fragColor;

uniform sampler2D texture_;

void main() {
    fragColor = texture(texture_, out_text_pos);
}