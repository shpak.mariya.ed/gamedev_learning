id: move_left_0
texture: skeleton_move_left
top_left: 0 1
bottom_right: 0.2 0.8
scale: 0.7

id: move_left_1
texture: skeleton_move_left
top_left: 0.2 1
bottom_right: 0.4 0.8
scale: 0.7

id: move_left_2
texture: skeleton_move_left
top_left: 0.4 1
bottom_right: 0.6 0.8
scale: 0.7

id: move_left_3
texture: skeleton_move_left
top_left: 0.6 1
bottom_right: 0.8 0.8
scale: 0.7

id: move_left_4
texture: skeleton_move_left
top_left: 0.8 1
bottom_right: 1 0.8
scale: 0.7

id: move_left_5
texture: skeleton_move_left
top_left: 0 0.8
bottom_right: 0.2 0.6
scale: 0.7

id: move_left_6
texture: skeleton_move_left
top_left: 0.2 0.8
bottom_right: 0.4 0.6
scale: 0.7

id: move_left_7
texture: skeleton_move_left
top_left: 0.4 0.8
bottom_right: 0.6 0.6
scale: 0.7

id: move_left_8
texture: skeleton_move_left
top_left: 0.6 0.8
bottom_right: 0.8 0.6
scale: 0.7

id: move_left_9
texture: skeleton_move_left
top_left: 0.8 0.8
bottom_right: 1 0.6
scale: 0.7

id: move_left_10
texture: skeleton_move_left
top_left: 0 0.6
bottom_right: 0.2 0.4
scale: 0.7

id: move_left_11
texture: skeleton_move_left
top_left: 0.2 0.6
bottom_right: 0.4 0.4
scale: 0.7

id: move_left_12
texture: skeleton_move_left
top_left: 0.4 0.6
bottom_right: 0.6 0.4
scale: 0.7

id: move_left_13
texture: skeleton_move_left
top_left: 0.6 0.6
bottom_right: 0.8 0.4
scale: 0.7

id: move_left_14
texture: skeleton_move_left
top_left: 0.8 0.6
bottom_right: 1 0.4
scale: 0.7

id: move_left_15
texture: skeleton_move_left
top_left: 0 0.4
bottom_right: 0.2 0.2
scale: 0.7

id: move_left_16
texture: skeleton_move_left
top_left: 0.2 0.4
bottom_right: 0.4 0.2
scale: 0.7

id: move_left_17
texture: skeleton_move_left
top_left: 0.4 0.4
bottom_right: 0.6 0.2
scale: 0.7

id: move_left_18
texture: skeleton_move_left
top_left: 0.6 0.4
bottom_right: 0.8 0.2
scale: 0.7

id: move_left_19
texture: skeleton_move_left
top_left: 0.8 0.4
bottom_right: 1 0.2
scale: 0.7

id: move_left_20
texture: skeleton_move_left
top_left: 0 0.2
bottom_right: 0.2 0
scale: 0.7

id: move_left_21
texture: skeleton_move_left
top_left: 0.2 0.2
bottom_right: 0.4 0
scale: 0.7

id: move_left_22
texture: skeleton_move_left
top_left: 0.4 0.2
bottom_right: 0.6 0
scale: 0.7

id: move_left_23
texture: skeleton_move_left
top_left: 0.6 0.2
bottom_right: 0.8 0
scale: 0.7