id: idle_down_0
texture: main_hero_idle_down
top_left: 0 1
bottom_right: 0.25 0.75

id: idle_down_1
texture: main_hero_idle_down
top_left: 0.25 1
bottom_right: 0.5 0.75

id: idle_down_2
texture: main_hero_idle_down
top_left: 0.5 1
bottom_right: 0.75 0.75

id: idle_down_3
texture: main_hero_idle_down
top_left: 0.75 1
bottom_right: 1 0.75

id: idle_down_4
texture: main_hero_idle_down
top_left: 0 0.75
bottom_right: 0.25 0.5

id: idle_down_5
texture: main_hero_idle_down
top_left: 0.25 0.75
bottom_right: 0.5 0.5

id: idle_down_6
texture: main_hero_idle_down
top_left: 0.5 0.75
bottom_right: 0.75 0.5

id: idle_down_7
texture: main_hero_idle_down
top_left: 0.75 0.75
bottom_right: 1 0.5

id: idle_down_8
texture: main_hero_idle_down
top_left: 0 0.5
bottom_right: 0.25 0.25

id: idle_down_9
texture: main_hero_idle_down
top_left: 0.25 0.5
bottom_right: 0.5 0.25

id: idle_down_10
texture: main_hero_idle_down
top_left: 0.5 0.5
bottom_right: 0.75 0.25

id: idle_down_11
texture: main_hero_idle_down
top_left: 0.75 0.5
bottom_right: 1 0.25

id: idle_down_12
texture: main_hero_idle_down
top_left: 0 0.25
bottom_right: 0.25 0