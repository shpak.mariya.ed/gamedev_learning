#pragma once

#include <string_view>
#include <memory>
#include <vector>

#include "animation_2d.h"
#include "game_objects.h"
#include "object_model.h"
#include "parser.h"

namespace game {
struct health_view_model : public object_model {
    int health;
};

class health_view {
    // where view starts
    position pos;
    std::vector<std::unique_ptr<health_view_model>> views;

public:
    health_view();
    health_view(std::string_view, const game_textures &);
    health_view_model* get_model_by_health(int) const;
    const position &get_start_position() const;
    void set_start_position(const position &);
};
} // namespace game