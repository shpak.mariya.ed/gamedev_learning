#pragma once

#include <chrono>

#include "engine.h"
#include "game_textures.h"
#include "object_model.h"

namespace game {
enum class direction { up, down, left, right };
enum class animation_type { looped, ended, locked };

// SPRITE
struct sprite {
    std::string id;
    texture_ptr texture;
    om::texture_position texture_top_left;
    om::texture_position texture_bottom_right;
    float scale = 1.f;
    om::position moving = {0.f, 0.f};
    vao_buffer vao;
};

// ANIMATION_2D
class animation_2d {
    float total_time = 0;
    uint16_t sprites_count = 0;
    uint16_t current_id = 0;
    std::vector<sprite> sprites;
    std::chrono::steady_clock::time_point start_time;
    animation_type type = animation_type::looped;

public:
    animation_2d();
    // todo is it need?
    animation_2d(std::string_view, const game_textures &, float, float);
    explicit animation_2d(std::vector<sprite> &);
    void start();
    void start_once();
    const sprite *get_current_sprite();
    void load_sprites_from_file(std::string_view, const game_textures &, float,
                                float);
    // call after loading only
    void set_total_time(float);
    // setting sprite (by the percent of passed time)
    // as current sprite
    // todo do better
    // float should be from 0 to 1, where 0 - the first sprite, 1 - the last one
    void lock_on_one_sprite(float);
};
using animation_map = std::unordered_map<std::string, animation_2d>;

// others
enum class generation_type { centered, to_top };
vao_buffer generate_vao_buffer(
    float width, const generation_type &gt = generation_type::centered,
    float ratio = 1.f, const om::position &moving = {0.f, 0.f},
    om::texture_position t0 = {0.f, 1.f}, om::texture_position t3 = {1.f, 0.f});
vao_buffer_colored generate_progress_bar(const om::position &pos,
                                         float model_width, float progress);
} // namespace game