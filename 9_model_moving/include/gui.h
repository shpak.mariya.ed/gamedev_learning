#pragma once

#include <string_view>
#include <vector>

#include "animation_2d.h"
#include "engine.h"
#include "game_objects.h"
#include "game_sounds.h"
#include "game_textures.h"
#include "parser.h"

namespace game {
enum class button_state { normal, clicked, hover };
enum class button_action { none, start, exit, retry };
enum class menu_state { off, start, final_win, final_lose };

struct button {
    std::string value;
    button_state state = button_state::normal;
    button_action action = button_action::none;
    position pos;
    texture_ptr texture = nullptr;
    vao_buffer normal_vao, clicked_vao, hover_vao;
    float width = 0.5f;
};

class menu_field {
    std::vector<button> buttons;
    object_model field_model;

public:
    void set_up(om::config_map_t &, const game_textures &);
    const object_model &get_field_model() const;
    std::vector<button> get_buttons(const menu_state &) const;
    button_action get_selected_action() const;
    void select_next_button();
    void select_prev_button();
    void create_button(om::config_map_t &, const game_textures &);
};

class game_menu {
    om::engine *engine = nullptr;
    menu_state state = menu_state::off;
    bool is_game_loading = true;
    game_textures textures;
    game_sounds sounds;
    menu_field field;
    object_model background_model;
    object_model logo_model;
    object_model lose_model;
    object_model win_model;
    animation_2d main_hero_animation;
    animation_2d main_hero_lose_animation;
    animation_2d main_hero_win_animation;
    animation_2d loading_animation;
    // shaders id
    uint8_t background_shader_id;

public:
    game_menu();
    game_menu(om::engine *, uint8_t);
    void set_up(std::string_view);
    const menu_state &get_state() const;
    void set_state(const menu_state &);
    void finish_loading();
    // return value - is still playing?
    bool read_event(const om::event &);
    void update();
    void render();
};
} // namespace game