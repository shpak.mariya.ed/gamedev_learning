#pragma once

#include <string_view>
#include <vector>

#include "object_model.h"

namespace game {
// DIGITS
class digits {
    texture_ptr texture = nullptr;
    std::array<vao_buffer, 11> vaos = {};

public:
    void load(std::string_view, texture_ptr);
    std::vector<vao_buffer> get_vaos(int) const;
    texture_ptr get_texture() const;
};

// COUNTER
class counter {
    texture_ptr texture = nullptr;
    vao_buffer vao;
    digits dig;

public:
    void load_icon(texture_ptr);
    void load_digits(std::string_view, texture_ptr);
    texture_ptr get_texture() const;
    vao_buffer get_vao() const;
    texture_ptr get_digit_texture() const;
    std::vector<vao_buffer> get_digit_vaos(int) const;
};
} // namespace game
