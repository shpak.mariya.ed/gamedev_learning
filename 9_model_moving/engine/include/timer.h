#pragma once

namespace om {
enum timer_state { play, pause, stop };

class timer {
public:
    virtual ~timer() = 0;
    virtual void start(float) = 0;
    virtual void stop() = 0;
    virtual bool is_expired() = 0;
    virtual void pause() = 0;
    virtual void unpause() = 0;
    virtual timer_state get_state() const = 0;
    virtual float get_duration() const = 0;
    virtual float get_percent() const = 0;
    // check point - point to detect how much time have been pasted
    virtual float get_duration_from_check_point() const = 0;
    virtual void set_check_point() = 0;
};

timer *create_timer();
void destroy_timer(timer *t);
} // namespace om