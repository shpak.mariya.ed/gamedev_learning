#include <algorithm>
#include <array>
#include <cassert>
#include <iostream>
#include <istream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

#include <SDL.h>
#include <SDL_audio.h>
#include <SDL_thread.h>

#include "engine.h"
#include "glad.h"
#include "parser.h"
#include "picopng.hxx"

namespace om {
// multithreading
SDL_GLContext global_gl_context = nullptr, global_gl_context_sub = nullptr;
SDL_Window *global_window = nullptr;

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam);

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

// todo do better
std::istream &operator>>(std::istream &is, vertex_full &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.col.r >> v.col.g >> v.col.b;
    is >> v.text_pos.u >> v.text_pos.v;
    return is;
}

std::istream &operator>>(std::istream &is, vertex_colored &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.col.r >> v.col.g >> v.col.b;
    return is;
}

std::istream &operator>>(std::istream &is, vertex_textured &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.text_pos.u >> v.text_pos.v;
    return is;
}

// todo find better case
std::istream &operator>>(std::istream &is, triangle_full &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream &operator>>(std::istream &is, triangle_colored &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream &operator>>(std::istream &is, triangle_textured &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

// TRIANGLES
triangle::~triangle() = default;

triangle_full::~triangle_full() = default;

triangle_colored::~triangle_colored() = default;

triangle_textured::~triangle_textured() = default;

triangle_internal *triangle::get() const { return (triangle_internal *)this; }

void triangle_full::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_full);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // u, v - texture
    glEnableVertexAttribArray(2);
    position_p += sizeof(color);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void triangle_colored::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_colored);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void triangle_textured::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_textured);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // u, v - texture
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

std::vector<triangle_full> triangle_full::parse_buffer(std::string_view path) {
    std::vector<triangle_full> out;
    std::ifstream file(path.data());
    assert(!!file);
    while (!file.eof()) {
        triangle_full tr;
        file >> tr;
        out.push_back(tr);
    }
    return out;
}

std::vector<triangle_colored>
triangle_colored::parse_buffer(std::string_view path) {
    std::vector<triangle_colored> out;
    std::ifstream file(path.data());
    assert(!!file);
    while (!file.eof()) {
        triangle_colored tr;
        file >> tr;
        out.push_back(tr);
    }
    return out;
}

std::vector<triangle_textured>
triangle_textured::parse_buffer(std::string_view path) {
    std::vector<triangle_textured> out;
    std::ifstream file(path.data());
    assert(!!file);
    while (!file.eof()) {
        triangle_textured tr;
        file >> tr;
        out.push_back(tr);
    }
    return out;
}

// TEXTURE_2D
class texture_2d final : public texture {
public:
    explicit texture_2d(std::string_view);
    ~texture_2d() override;
    uint8_t get_id() const final;
    uint16_t get_width() const final;
    uint16_t get_height() const final;
    float get_scale_rate(float) const final;
    void bind() const final;

private:
    GLuint texture_id = 0;
    GLuint width = 0;
    GLuint height = 0;
};

texture_2d::texture_2d(std::string_view path) {
    std::vector<std::byte> png_src;
    read_file(path, png_src);

    std::vector<std::byte> image;
    unsigned long w = 0;
    unsigned long h = 0;
    int error = decodePNG(image, w, h, png_src.data(), png_src.size(), false);
    if (error != 0) {
        std::cerr << "Error: " << error << std::endl;
        throw std::runtime_error("Cannot load texture.");
    }
    width = static_cast<GLuint>(w);
    height = static_cast<GLuint>(h);

    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    GLint mipmap_level = 0;
    GLint border = 0;
    auto w_sizei = static_cast<GLsizei>(w);
    auto h_sizei = static_cast<GLsizei>(h);
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, w_sizei, h_sizei, border,
                 GL_RGBA, GL_UNSIGNED_BYTE, image.data());
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

texture::~texture() = default;

texture_2d::~texture_2d() { glDeleteTextures(1, &texture_id); }

uint8_t texture_2d::get_id() const { return texture_id; }

uint16_t texture_2d::get_width() const { return static_cast<uint16_t>(width); }

uint16_t texture_2d::get_height() const {
    return static_cast<uint16_t>(height);
}

float texture_2d::get_scale_rate(float window_ratio) const {
    float out = 0.f;
    float texture_ratio =
        static_cast<float>(width) / static_cast<float>(height);
    out = window_ratio / texture_ratio;
    return out;
}

void texture_2d::bind() const { glBindTexture(GL_TEXTURE_2D, texture_id); }

// SOUND_BUFFER
class sound_buffer_impl final : public sound_buffer {
public:
    bool is_looped = false;
    bool is_playing = false;
    bool is_converted = false;

    uint8_t *buffer = nullptr;
    uint32_t current_index = 0;
    SDL_AudioDeviceID device;
    uint32_t length = 0;

    sound_buffer_impl(std::string_view path, SDL_AudioDeviceID device_id,
                      SDL_AudioSpec device_spec)
        : device(device_id) {
        SDL_AudioSpec file_spec;
        if (SDL_LoadWAV(path.data(), &file_spec, &buffer, &length) == nullptr)
            throw std::runtime_error(std::string("Error: Cannot load ") +
                                     path.data());

        if (file_spec.channels != device_spec.channels ||
            file_spec.format != device_spec.format ||
            file_spec.freq != device_spec.freq) {
            SDL_AudioCVT cvt;
            SDL_BuildAudioCVT(&cvt, file_spec.format, file_spec.channels,
                              file_spec.freq, device_spec.format,
                              device_spec.channels, device_spec.freq);
            if (cvt.needed) {
                is_converted = true;
                cvt.len = static_cast<int>(length);
                size_t new_buffer_size =
                    static_cast<size_t>(cvt.len * cvt.len_mult);
                uint8_t *tmp_buffer = new uint8_t[new_buffer_size];
                std::copy_n(buffer, length, tmp_buffer);
                SDL_FreeWAV(buffer);
                cvt.buf = tmp_buffer;
                if (SDL_ConvertAudio(&cvt) != 0) {
                    std::cerr << "Cannot convert audio file " << path
                              << " into the audio device format!" << std::endl;
                }
                buffer = tmp_buffer;
                length = static_cast<uint32_t>(cvt.len_cvt);
            }
        }
    }
    ~sound_buffer_impl() override {
        if (!is_converted) SDL_FreeWAV(buffer);
    }
    void play(const sound_playing_type t) final {
        SDL_LockAudioDevice(device);
        is_playing = true;
        if (t == sound_buffer::sound_playing_type::looped) is_looped = true;
        current_index = 0;
        SDL_UnlockAudioDevice(device);
    }
    void stop() final { is_playing = false; }
};
sound_buffer::~sound_buffer() = default;

// SHADER
class shader {
public:
    shader(std::string_view, std::string_view);
    ~shader();
    uint8_t get_id() const;
    GLuint load(std::string_view, GLenum);
    void compile(GLuint, GLenum);
    void create_program();
    void use();
    void set_uniform(std::string_view, float) const;
    void set_uniform(std::string_view, const texture &) const;
    void set_uniform(std::string_view, const GLfloat *) const;

private:
    GLuint program_id = 0;
    GLuint vert_shader_id = 0;
    GLuint frag_shader_id = 0;
};

shader::shader(std::string_view vert_path, std::string_view frag_path) {
    vert_shader_id = load(vert_path, GL_VERTEX_SHADER);
    frag_shader_id = load(frag_path, GL_FRAGMENT_SHADER);

    compile(vert_shader_id, GL_VERTEX_SHADER);
    compile(frag_shader_id, GL_FRAGMENT_SHADER);

    create_program();
}

shader::~shader() {
    glDeleteShader(vert_shader_id);
    glDeleteShader(frag_shader_id);
}

GLuint shader::load(std::string_view path, GLenum type) {
    std::string gl_version = "#version ";
    // getting OpenGL major and minor versions
    int major, minor;
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &major);
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &minor);
    gl_version += std::to_string(major) + std::to_string(minor) + "0 ";
    // getting OpenGL mask (CORE/ES)
    int mask;
    SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &mask);
    if (mask == SDL_GL_CONTEXT_PROFILE_CORE) {
        gl_version += "core";
    } else if (mask == SDL_GL_CONTEXT_PROFILE_ES) {
        gl_version += "es";
    } else {
        throw std::runtime_error(
            "Error: Cannot load shader not on CORE/ES context!");
    }
    gl_version += "\n";
    // only for ES: precision
    if (mask == SDL_GL_CONTEXT_PROFILE_ES) {
        gl_version += "precision highp float;\n";
    }
    // generation final shader src
    std::string shader_src = read_file(path.data());
    gl_version += shader_src;
    char *final_string = gl_version.data();

    GLuint shader_id = glCreateShader(type);
    if (shader_id == 0) {
        throw std::runtime_error(
            "Error: Cannot create shader! glCreateShader returned 0.");
    }
    glShaderSource(shader_id, 1, &final_string, nullptr);
    return shader_id;
}

void shader::compile(GLuint shader_id, GLenum type) {
    glCompileShader(shader_id);

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);

    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
        glDeleteShader(shader_id);

        std::string_view shader_type_str = "vertex";
        if (type == GL_FRAGMENT_SHADER) shader_type_str = "fragment";

        std::cerr << "Error compiling " << shader_type_str
                  << " shader:" << std::endl
                  << shader_id << std::endl
                  << info_chars.data();
        throw std::runtime_error("Error compiling shader!");
    }
}

void shader::create_program() {
    program_id = glCreateProgram();
    if (0 == program_id) {
        throw std::runtime_error("Failed to create gl program.");
    }

    glAttachShader(program_id, vert_shader_id);
    glAttachShader(program_id, frag_shader_id);

    glLinkProgram(program_id);
    GLint linked_status = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_status);
    if (linked_status == 0) {
        GLint info_len = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_log(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id, info_len, nullptr, info_log.data());
        std::cerr << "Error linking program:" << std::endl << info_log.data();
        glDeleteProgram(program_id);
    }
}

uint8_t shader::get_id() const { return static_cast<uint8_t>(program_id); }

void shader::use() { glUseProgram(program_id); }

void shader::set_uniform(std::string_view name, float value) const {
    GLint uniform_location = glGetUniformLocation(program_id, name.data());
    if (uniform_location == -1) {
        std::cerr << "Cannot set uniform with the name " << name << std::endl;
        throw std::runtime_error("Setting uniform error!");
    }
    glUniform1f(uniform_location, value);
}

void shader::set_uniform(std::string_view name, const texture &t) const {
    // todo check texture == null
    GLint uniform_location = glGetUniformLocation(program_id, name.data());
    if (uniform_location == -1) {
        std::cerr << "Cannot set uniform with the name " << name << std::endl;
        throw std::runtime_error("Setting uniform error!");
    }
    glActiveTexture(GL_TEXTURE0);
    t.bind();
    glUniform1i(uniform_location, 0);
}

void shader::set_uniform(std::string_view name, const GLfloat *m) const {
    GLint uniform_location = glGetUniformLocation(program_id, name.data());
    // todo uniform_location returns -1
    // https://www.khronos.org/opengl/wiki/GLSL_:_common_mistakes#glGetUniformLocation_and_glGetActiveUniform
    glUniformMatrix3fv(uniform_location, 1, GL_TRUE, &m[0]);
}

// ENGINE_IMPL
class engine_impl final : public engine {
public:
    ~engine_impl() override;
    std::string initialize(std::string_view) final;
    float get_time() const final;
    uint16_t get_window_width() const final;
    uint16_t get_window_height() const final;
    float get_window_ratio() const final;
    void set_window_width(uint16_t) final;
    void set_window_height(uint16_t) final;
    void update_view_port();
    bool pool_event(event &) final;
    bool check_input(event &);
    void create_thread(void (*)()) final;
    texture *create_texture(std::string_view) final;
    sound_buffer *create_sound_buffer(std::string_view) final;
    uint8_t create_shader(std::string_view, std::string_view) final;
    void check_texture(const texture *);
    void check_shader(const shader *);
    void use_shader(uint8_t) final;
    void use_default_shader() final;
    void set_default_shader(uint8_t) final;
    void set_uniform(std::string_view, float) final;
    void render(const triangle &) final;
    void render(const triangle &, const texture *) final;
    void render(const triangle_buffer<triangle_colored> &) final;
    void render(const triangle_buffer<triangle_textured> &,
                const texture *) final;
    void render(const triangle_buffer<triangle_textured> &, const matrix_2x3 &,
                const texture *) final;
    void morph(const triangle_colored &, const triangle_colored &, float) final;
    void swap_buffers() final;
    void terminate() final;
    static void audio_callback(void *, uint8_t *, int);

private:
    SDL_Window *window = nullptr;
    SDL_GLContext gl_context = nullptr;
    SDL_GLContext gl_context_sub = nullptr;
    // textures
    std::vector<std::unique_ptr<texture>> textures;
    // sounds
    SDL_AudioDeviceID audio_device;
    SDL_AudioSpec audio_device_spec;
    std::vector<std::unique_ptr<sound_buffer>> sounds;
    // shaders data
    std::map<uint8_t, std::unique_ptr<shader>> shaders;
    shader *current_shader = nullptr;
    shader *default_shader = nullptr;
    // default buttons map
    std::array<bool, 6> events;
    // default config
    uint16_t window_width = 640;
    uint16_t window_height = 480;
    static constexpr float window_ratio = 16.f / 9.f;
};

static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (e == nullptr) {
        throw std::runtime_error("Engine is nullptr. Nothing to destroy!");
    }
    delete e;
}

engine::~engine() = default;

engine_impl::~engine_impl() = default;

triangle_internal *engine::get_triangle(const triangle &tr) { return tr.get(); }

std::string engine_impl::initialize(std::string_view path) {
    using namespace std;
    stringstream serr;

    SDL_version compiled = {0, 0, 0};
    SDL_version linked = {0, 0, 0};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        std::string msg("Error: failed call SDL_Init: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    config_map_t config_map;
    config_map = get_config_map_from_file(path);

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    auto window_width_str = config_map["window_width"];
    auto window_height_str = config_map["window_height"];
    window_width = str_to_int(window_width_str, window_width);
    window_height = str_to_int(window_height_str, window_height);

    window = SDL_CreateWindow(
        title.data(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        window_width, window_height,
        ::SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_RESIZABLE);

    if (window == nullptr) {
        std::string msg("Error: failed call SDL_CreateWindow: ");
        msg += SDL_GetError();
        serr << msg << endl;
        SDL_Quit();
        return serr.str();
    }
    // multithreading
    global_window = window;

    int min_window_width = 640;
    int min_window_height = 480;
    SDL_SetWindowMinimumSize(window, min_window_width, min_window_height);

    // OpenGl context initialization
    int gl_major_ver = 3;
    int gl_minor_ver = 2;
    int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

    std::string_view platform = SDL_GetPlatform();
    using namespace std::string_view_literals;
    auto list = {"Windows"sv, "Apple"sv, "Linux"sv};
    auto it = std::find(std::begin(list), std::end(list), platform);
    if (it != end(list)) {
        gl_minor_ver = 3;
        gl_context_profile = SDL_GL_CONTEXT_PROFILE_CORE;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);
    // for multithreading objects sharing
    SDL_GL_SetAttribute(SDL_GL_SHARE_WITH_CURRENT_CONTEXT, 1);

    gl_context_sub = SDL_GL_CreateContext(window);
    if (gl_context_sub == nullptr) {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context_sub = SDL_GL_CreateContext(window);
    }
    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_CORE);
        gl_context = SDL_GL_CreateContext(window);
    }
    // multithreading
    global_gl_context = gl_context;
    global_gl_context_sub = gl_context_sub;

    if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
        std::clog << "Error: failed to initialize glad" << std::endl;
    }

    glEnable(GL_DEBUG_OUTPUT);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glDebugMessageCallback(callback_opengl_debug, nullptr);
    glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr,
                          GL_TRUE);

    // if common window ratio (from settings) != ratio
    update_view_port();

    // audio initialization
    audio_device_spec.freq = 48000;
    audio_device_spec.format = AUDIO_S16LSB;
    audio_device_spec.channels = 2;
    audio_device_spec.samples = 1024;
    audio_device_spec.callback = engine_impl::audio_callback;
    audio_device_spec.userdata = this;

    if (std::string_view("Windows") == SDL_GetPlatform()) {
        const char *selected_audio_driver = SDL_GetAudioDriver(1);

        if (0 != SDL_AudioInit(selected_audio_driver)) {
            serr << "Cannot init SDL_audio." << std::flush;
        }
    }

    const char *default_audio_device_name = nullptr;
    audio_device =
        SDL_OpenAudioDevice(default_audio_device_name, 0, &audio_device_spec,
                            nullptr, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (audio_device == 0) {
        std::cerr << SDL_GetError();
        throw std::runtime_error("Error: Cannot open audio device!");
    } else {
        SDL_PauseAudioDevice(audio_device, SDL_FALSE);
    }

    // buffers initialize
    GLuint vbo = 0;
    GLuint ebo = 0;
    GLuint vao = 0;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // todo add using ebo

    return "";
}

float engine_impl::get_time() const { return SDL_GetTicks(); }

uint16_t engine_impl::get_window_width() const { return window_width; }

uint16_t engine_impl::get_window_height() const { return window_height; }

float engine_impl::get_window_ratio() const { return window_ratio; }

void engine_impl::set_window_width(uint16_t w) { window_width = w; }

void engine_impl::set_window_height(uint16_t h) { window_height = h; }

void engine_impl::update_view_port() {
    int w, h;
    SDL_GetWindowSize(window, &w, &h);
    set_window_width(static_cast<uint16_t>(w));
    set_window_height(static_cast<uint16_t>(h));
    float ratio = float(w) / float(h);
    if (ratio < window_ratio) {
        int new_height = int(float(w) / window_ratio);
        int y_offset = int(float(h - new_height) / 2.f);
        glViewport(0, y_offset, window_width, new_height);
    } else {
        int new_width = int(float(h) * window_ratio);
        int x_offset = int(float(w - new_width) / 2.f);
        glViewport(x_offset, 0, new_width, window_height);
    }
}

bool engine_impl::check_input(event &updated_event) {
    int i = updated_event.key;
    if (events[i] != updated_event.is_pressed) {
        // updating inputs map
        events[i] = updated_event.is_pressed;
        return true;
    }
    return false;
}

bool engine_impl::pool_event(event &e) {
    SDL_Event sdl_event;
    while (SDL_PollEvent(&sdl_event) > 0) {
        if (sdl_event.type == SDL_QUIT) {
            e.key = keys::turn_off;
            e.is_pressed = true;
            return true;
        }
        if (sdl_event.type == SDL_WINDOWEVENT &&
            sdl_event.window.event == SDL_WINDOWEVENT_RESIZED) {
            update_view_port();
            return false;
        }
        bool is_key_down = sdl_event.type == SDL_KEYDOWN;
        bool is_key_up = sdl_event.type == SDL_KEYUP;
        if (is_key_down || is_key_up) {
            SDL_Keycode key_code = sdl_event.key.keysym.sym;
            switch (key_code) {
            case SDLK_ESCAPE:
                e.key = keys::esc;
                break;
            case SDLK_RETURN:
                e.key = keys::enter;
                break;
            case SDLK_SPACE:
                e.key = keys::space;
                break;
            case SDLK_UP:
            case SDLK_w:
                e.key = keys::up;
                break;
            case SDLK_DOWN:
            case SDLK_s:
                e.key = keys::down;
                break;
            case SDLK_LEFT:
            case SDLK_a:
                e.key = keys::left;
                break;
            case SDLK_RIGHT:
            case SDLK_d:
                e.key = keys::right;
                break;
            default:
                return false;
            }
            e.is_pressed = is_key_down;
            return check_input(e);
        }
    }
    return false;
}

static int load_gl_in_background(void *foo) {
    int res = SDL_GL_MakeCurrent(global_window, global_gl_context_sub);
    if (res != 0) {
        std::cout << "Cannot change context: " << SDL_GetError() << std::endl;
    }
    ((void (*)())foo)();
    res = SDL_GL_MakeCurrent(global_window, NULL);
    if (res != 0) {
        std::cout << "Cannot change context: " << SDL_GetError() << std::endl;
    }
    return 0;
}

void engine_impl::create_thread(void (*foo)()) {
    SDL_Thread *thread =
        SDL_CreateThread(load_gl_in_background, "SDL thread", (void *)foo);
    if (thread == nullptr) {
        std::cerr << "Cannot create thread: " << SDL_GetError() << std::endl;
        throw std::runtime_error("Error: Thread creation!");
    }
}

texture *engine_impl::create_texture(std::string_view path) {
    textures.push_back(std::make_unique<texture_2d>(path));
    texture *out = textures.back().get();
    if (out == nullptr) {
        throw std::runtime_error(
            std::string("Error: Cannot create and save texture by path ") +
            path.data());
    }
    return out;
}

sound_buffer *engine_impl::create_sound_buffer(std::string_view path) {
    SDL_LockAudioDevice(audio_device);
    sounds.push_back(std::make_unique<sound_buffer_impl>(path, audio_device,
                                                         audio_device_spec));
    sound_buffer *buf = sounds.back().get();
    if (buf == nullptr) {
        throw std::runtime_error("Error: Created sound is nullptr!");
    }
    SDL_UnlockAudioDevice(audio_device);
    return buf;
}

uint8_t engine_impl::create_shader(std::string_view vert_path,
                                   std::string_view frag_path) {
    auto s = std::make_unique<shader>(vert_path, frag_path);
    uint8_t shader_id = s->get_id();
    shaders.insert(std::make_pair(shader_id, std::move(s)));
    return shader_id;
}

void engine_impl::check_texture(const texture *t) {
    if (t == nullptr) {
        throw std::runtime_error("Cannot use null texture!");
    }
}

void engine_impl::check_shader(const shader *shader_ptr) {
    if (shader_ptr == nullptr) {
        throw std::runtime_error("Cannot use unassigned shader!");
    }
}

void engine_impl::use_shader(uint8_t shader_id) {
    current_shader = shaders.at(shader_id).get();
    check_shader(current_shader);
    current_shader->use();
}

void engine_impl::use_default_shader() {
    check_shader(default_shader);
    current_shader = default_shader;
    current_shader->use();
}

void engine_impl::set_default_shader(uint8_t shader_id) {
    default_shader = shaders.at(shader_id).get();
    check_shader(default_shader);
}

void engine_impl::set_uniform(std::string_view name, float value) {
    check_shader(current_shader);
    current_shader->set_uniform(name, value);
}

void engine_impl::render(const triangle &tr) {
    check_shader(current_shader);
    get_triangle(tr)->draw();
}

void engine_impl::render(const triangle &tr, const texture *t) {
    check_texture(t);
    check_shader(current_shader);
    current_shader->set_uniform("texture_", *t);
    get_triangle(tr)->draw();
}

void engine_impl::render(const triangle_buffer<triangle_colored> &tb) {
    check_shader(current_shader);
    std::vector<triangle_colored> buffer = tb.buffer();
    for (auto tr : buffer) {
        get_triangle(tr)->draw();
    }
}

void engine_impl::render(const triangle_buffer<triangle_textured> &tb,
                         const texture *t) {
    check_texture(t);
    check_shader(current_shader);
    current_shader->set_uniform("texture_", *t);
    std::vector<triangle_textured> buffer = tb.buffer();
    for (auto tr : buffer) {
        get_triangle(tr)->draw();
    }
}

void engine_impl::render(const triangle_buffer<triangle_textured> &tb,
                         const matrix_2x3 &m, const texture *t) {
    check_texture(t);
    check_shader(current_shader);
    GLfloat tmp_matrix[9] = {m.col0.x, m.col0.y, m.col2.x, m.col1.x, m.col1.y,
                             m.col2.y, 0.f,      0.f,      1.f};

    current_shader->set_uniform("texture_", *t);
    current_shader->set_uniform("matrix_", tmp_matrix);

    std::vector<triangle_textured> buffer = tb.buffer();
    for (auto tr : buffer) {
        get_triangle(tr)->draw();
    }
}

void engine_impl::morph(const triangle_colored &tr0,
                        const triangle_colored &tr1, float t) {
    check_shader(current_shader);
    current_shader->set_uniform("t", t);

    std::vector<vertex_colored> buffer;
    for (int i = 0; i < 3; ++i) {
        buffer.push_back(tr0.v[i]);
        buffer.push_back(tr1.v[i]);
    }

    glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(vertex_colored),
                 &buffer.front(), GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_colored) * 2;
    GLintptr position_p = 0;

    // 1th triangle: x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // 1th triangle: r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // 2th triangle: x, y
    glEnableVertexAttribArray(2);
    position_p += sizeof(color);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // triangle from 2th buffer: r, g, b
    position_p += sizeof(position);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    GLuint drawing_count = buffer.size() / 2;
    glDrawArrays(GL_TRIANGLES, 0, drawing_count);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
}

void engine_impl::swap_buffers() {
    SDL_GL_SwapWindow(window);
    glClearColor(0.f, 0.f, 0.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void engine_impl::terminate() {
    SDL_GL_DeleteContext(gl_context);
    SDL_GL_DeleteContext(gl_context_sub);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void engine_impl::audio_callback(void *engine_ptr, uint8_t *stream,
                                 int stream_size) {
    std::fill_n(stream, stream_size, '\0');
    engine_impl *e = static_cast<engine_impl *>(engine_ptr);

    for (auto &sound : e->sounds) {
        sound_buffer_impl *snd = dynamic_cast<sound_buffer_impl *>(sound.get());
        if (snd->is_playing) {
            uint32_t rest = snd->length - snd->current_index;
            uint8_t *current_buff = &snd->buffer[snd->current_index];

            if (rest <= static_cast<uint32_t>(stream_size)) {
                SDL_MixAudioFormat(stream, current_buff,
                                   e->audio_device_spec.format, rest,
                                   SDL_MIX_MAXVOLUME);
                snd->current_index += rest;
            } else {
                SDL_MixAudioFormat(
                    stream, current_buff, e->audio_device_spec.format,
                    static_cast<uint32_t>(stream_size), SDL_MIX_MAXVOLUME);
                snd->current_index += static_cast<uint32_t>(stream_size);
            }

            if (snd->current_index == snd->length) {
                if (snd->is_looped) {
                    snd->current_index = 0;
                } else {
                    snd->is_playing = false;
                }
            }
        }
    }
}

// glad debug using
static const char *source_to_strv(GLenum source) {
    switch (source) {
    case GL_DEBUG_SOURCE_API:
        return "API";
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        return "SHADER_COMPILER";
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        return "WINDOW_SYSTEM";
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        return "THIRD_PARTY";
    case GL_DEBUG_SOURCE_APPLICATION:
        return "APPLICATION";
    case GL_DEBUG_SOURCE_OTHER:
        return "OTHER";
    }
    return "unknown";
}

static const char *type_to_strv(GLenum type) {
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
        return "ERROR";
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        return "DEPRECATED_BEHAVIOR";
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        return "UNDEFINED_BEHAVIOR";
    case GL_DEBUG_TYPE_PERFORMANCE:
        return "PERFORMANCE";
    case GL_DEBUG_TYPE_PORTABILITY:
        return "PORTABILITY";
    case GL_DEBUG_TYPE_MARKER:
        return "MARKER";
    case GL_DEBUG_TYPE_PUSH_GROUP:
        return "PUSH_GROUP";
    case GL_DEBUG_TYPE_POP_GROUP:
        return "POP_GROUP";
    case GL_DEBUG_TYPE_OTHER:
        return "OTHER";
    }
    return "unknown";
}

static const char *severity_to_strv(GLenum severity) {
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
        return "HIGH";
    case GL_DEBUG_SEVERITY_MEDIUM:
        return "MEDIUM";
    case GL_DEBUG_SEVERITY_LOW:
        return "LOW";
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        return "NOTIFICATION";
    }
    return "unknown";
}

static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam) {
    auto &buff{local_log_buff};
    int num_chars = std::snprintf(
        buff.data(), buff.size(), "%s %s %d %s %.*s\n", source_to_strv(source),
        type_to_strv(type), id, severity_to_strv(severity), length, message);

    if (num_chars > 0) {
        std::cerr.write(buff.data(), num_chars);
    }
}
} // namespace om