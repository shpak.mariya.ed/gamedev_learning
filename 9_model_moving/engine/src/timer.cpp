#include <chrono>
#include <iostream>

#include "timer.h"

namespace om {
class timer_impl final : public timer {
    std::chrono::steady_clock::time_point start_time;
    float total_duration = 0.f;
    float current_duration = 0.f;
    timer_state state = timer_state::stop;
    float check_point = 0;

public:
    ~timer_impl() override = default;
    void start(float td) final {
        if (td <= 0.f) {
            std::cerr << "Cannot set timer for " << td << "seconds!"
                      << std::endl;
            throw std::runtime_error("Error setting timer!");
        }
        start_time = std::chrono::steady_clock::now();
        check_point = 0.f;
        total_duration = td;
        current_duration = 0.f;
        state = timer_state::play;
    }
    void stop() final {
        total_duration = 0.f;
        current_duration = 0.f;
        state = timer_state::stop;
    }
    bool is_expired() final { return (get_duration() > total_duration); }
    void pause() final {
        if (state == timer_state::stop) {
            std::cerr << "Cannot pause stopped timer! Start timer before."
                      << std::endl;
            return;
        } else if (state == timer_state::pause) {
            std::cerr << "Timer is already paused!" << std::endl;
            return;
        } else {
            current_duration = get_duration();
            state = timer_state::pause;
        }
    }
    void unpause() final {
        if (state != timer_state::pause) {
            std::cerr << "Cannot unpause not paused timer!" << std::endl;
            return;
        }

        start_time = std::chrono::steady_clock::now();
        state = timer_state::play;
    }
    timer_state get_state() const final { return state; }
    float get_duration() const final {
        if (state == timer_state::stop) {
            std::cerr << "Cannot check stopped timer!" << std::endl;
            return current_duration;
        }
        if (state == timer_state::pause) return current_duration;

        using namespace std::chrono;
        steady_clock::time_point current_time = steady_clock::now();
        duration<float> pasted_time =
            duration_cast<duration<float>>(current_time - start_time);

        return (current_duration + pasted_time.count());
    }
    float get_percent() const final {
        if (state == timer_state::stop) {
            std::cerr << "Cannot check stopped timer!" << std::endl;
            return 0.f;
        }
        return (get_duration() / total_duration);
    }
    float get_duration_from_check_point() const final {
        return (get_duration() - check_point);
    }
    void set_check_point() final { check_point = get_duration(); }
};

timer *create_timer() {
    timer *result = new timer_impl();
    return result;
}
void destroy_timer(timer *t) {
    if (t == nullptr) {
        throw std::runtime_error("Timer is nullptr. Nothing to destroy!");
    }
    delete t;
}
timer::~timer() = default;
} // namespace om