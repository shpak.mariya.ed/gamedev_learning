#include <algorithm>
#include <charconv>
#include <iostream>
#include <sstream>

#include <SDL.h>

#include "parser.h"

namespace om {
// only for android using
// using sdl loader because of usual C++ streams cannot read archives
// Android .apk is archive
char *read_file(std::string_view path) {
    SDL_RWops *rw = SDL_RWFromFile(path.data(), "rb");
    if (rw == nullptr) {
        std::cerr << "Cannot open file: " << path << ". " << SDL_GetError();
        throw std::runtime_error("Error: Cannot load file: " +
                                 std::string(path));
    }
    // getting size
    uint64_t res_size = SDL_RWsize(rw);
    char *out = (char *)malloc(res_size + 1);
    // filling
    uint64_t nb_read_total = 0, nb_read = 1;
    char *buf = out;
    while (nb_read_total < res_size && nb_read != 0) {
        nb_read = SDL_RWread(rw, buf, 1, (res_size - nb_read_total));
        nb_read_total += nb_read;
        buf += nb_read;
    }
    SDL_RWclose(rw);
    if (nb_read_total != res_size) {
        free(out);
        std::cerr << "Cannot read all content from file: " << path << ". "
                  << SDL_GetError();
        throw std::runtime_error("Error: Cannot load file: " +
                                 std::string(path));
    }
    out[nb_read_total] = '\0';
    return out;
}
// for textures loading
void read_file(const std::string_view path, std::vector<std::byte> &buffer) {
    SDL_RWops *rw = SDL_RWFromFile(path.data(), "rb");
    if (rw == nullptr) {
        std::cerr << "Cannot open texture file: " << path << ". "
                  << SDL_GetError();
        throw std::runtime_error("Error: Cannot load file: " +
                                 std::string(path));
    }
    // getting size
    uint64_t file_size = rw->size(rw);
    buffer.resize(file_size);
    // filling
    uint64_t nb_read = rw->read(rw, buffer.data(), file_size, 1);
    if (nb_read != 1) {
        rw->close(rw);
        std::cerr << "Cannot read all content from file: " << path << ". "
                  << SDL_GetError();
        throw std::runtime_error("Error: Cannot load file: " +
                                 std::string(path));
    }
    rw->close(rw);
}
// todo fix size bug random values, debug start point is incorrect
/*std::stringstream load_file(std::string_view path) {
    SDL_RWops *io = SDL_RWFromFile(path.data(), "rb");
    if (io == nullptr) {
        throw std::runtime_error("Cannot load file: " + std::string(path));
    }
    Sint64 file_size = SDL_RWsize(io);
    Sint64 file_size_2 = io->size(io);
    if (file_size == -1) {
        throw std::runtime_error("Cannot determine size of file: " +
                                 std::string(path));
    }
    size_t size = static_cast<size_t>(file_size);
    size_t size2 = static_cast<size_t>(file_size_2);
    char mem[size];
    SDL_RWread(io, &mem, size, 1);
    char mem_2[size2];
    size_t num_readed_objects = io->read(io, &mem_2, size2, 1);
    if (io->close(io) != 0) {
        throw std::runtime_error("Failed close file: " + std::string(path));
    }
    std::stringstream out;
    out >> mem;
    return out;
}*/

// parsing
void trim(std::string &str) {
    str.erase(std::find_if_not(str.rbegin(), str.rend(), ::isspace).base(),
              str.end());
    str.erase(str.begin(), std::find_if_not(str.begin(), str.end(), ::isspace));
}

int str_to_int(std::string in) {
    int res = 0;
    auto check_res = std::from_chars(in.data(), in.data() + in.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << in << " into integer!"
                  << std::endl;
    }
    return res;
}

int str_to_int(std::string in, int d) {
    if (in.empty()) return d;
    int res = 0;
    auto check_res = std::from_chars(in.data(), in.data() + in.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << in << " into integer!"
                  << std::endl;
        res = d;
    }
    return res;
}

float str_to_float(std::string in) {
    float res;
    try {
        res = std::stof(in);
    } catch (std::invalid_argument) {
        std::cerr << "Could not convert " << in << " into float!" << std::endl;
        throw std::runtime_error("Error: Cannot convert string to float!");
    }
    return res;
}

float str_to_float(std::string in, float d) {
    if (in.empty()) return d;
    float res;
    try {
        res = std::stof(in);
    } catch (std::invalid_argument) {
        std::cerr << "Could not convert " << in << " into float!" << std::endl;
        res = d;
    }
    return res;
}

std::vector<std::string> get_split_string(std::string in, const char del) {
    std::vector<std::string> out;
    if (in.empty()) {
        std::cerr << "Line for split is empty!" << std::endl;
        return out;
    }

    size_t i, part_start = 0;
    for (i = 0; i < in.size(); ++i) {
        char c = in[i];
        if (c == del) {
            if (i > part_start) {
                std::string sub = in.substr(part_start, i - part_start);
                out.push_back(sub);
            } else if (i == part_start) {
                out.push_back("");
            }
            part_start = i + 1;
        }
    }
    // pushing final string part
    if (i > part_start) {
        std::string sub = in.substr(part_start, i - part_start);
        out.push_back(sub);
    }

    return out;
}

config_map_t get_config_map_from_string(std::string in, const char major_del,
                                        const char minor_del) {
    config_map_t out;
    if (in.empty()) return out;

    std::vector<std::string> lines;
    lines = get_split_string(in, major_del);
    for (auto line : lines) {
        std::vector<std::string> config_pair =
            get_split_string(line, minor_del);
        if (config_pair.size() != 2) {
            std::cerr << "Parsing error! Line \"" << line << "\" is incorrect."
                      << std::endl
                      << "Needed format - key" << minor_del << "value"
                      << major_del << std::endl;
        } else {
            std::string key = config_pair.at(0);
            std::string value = config_pair.at(1);
            trim(key);
            trim(value);
            out.insert(std::make_pair(key, value));
        }
    }

    return out;
}

config_map_t get_config_map_from_file(std::string_view path,
                                      const char major_del,
                                      const char minor_del) {
    std::string config = read_file(path);
    return get_config_map_from_string(config, major_del, minor_del);
}

const std::vector<config_map_t>
get_list_of_config_map_from_file(std::string_view path, const char major_del,
                                 const char minor_del) {
    // if major_del is '\n' global objects delimiter is empty line
    // else global delimiter is '\n'
    std::vector<config_map_t> out;
    std::string config = read_file(path);
    // todo add get_list_of_config_map_from_string() method
    std::stringstream ss;
    ss << config;

    if (major_del != '\n') {
        while (!ss.eof()) {
            std::string line;
            std::getline(ss, line);
            if (line.empty()) continue;
            config_map_t tmp_map;
            tmp_map = get_config_map_from_string(line, major_del, minor_del);
            out.push_back(tmp_map);
        }
        return out;
    }

    std::string elem_config;
    while (!ss.eof()) {
        std::string line;
        std::getline(ss, line);
        if (line.empty()) {
            if (!elem_config.empty()) {
                config_map_t tmp_map;
                tmp_map = get_config_map_from_string(elem_config, major_del,
                                                     minor_del);
                out.push_back(tmp_map);
                elem_config.clear();
            }
        } else {
            elem_config += line + major_del;
        }
    }
    if (!elem_config.empty()) {
        config_map_t tmp_map;
        tmp_map = get_config_map_from_string(elem_config, major_del, minor_del);
        out.push_back(tmp_map);
    }

    return out;
}
} // namespace om