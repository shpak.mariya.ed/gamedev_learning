#include <cmath>

#include "matrix.h"

namespace om {
// scale
matrix_2x3 matrix_2x3::scale(float s) {
    matrix_2x3 out;
    out.col0.x = s;
    out.col1.y = s;
    return out;
}
matrix_2x3 matrix_2x3::scale(vec2 s) {
    matrix_2x3 out;
    out.col0.x = s.x;
    out.col1.y = s.y;
    return out;
}
// move
matrix_2x3 matrix_2x3::move(float d) {
    matrix_2x3 out;
    out.col0.x = 1.f;
    out.col1.y = 1.f;
    out.col2.x = d;
    out.col2.y = d;
    return out;
}
matrix_2x3 matrix_2x3::move(vec2 d) {
    matrix_2x3 out;
    out.col0.x = 1.f;
    out.col1.y = 1.f;
    out.col2.x = d.x;
    out.col2.y = d.y;
    return out;
}
// rotate
matrix_2x3 matrix_2x3::rotate(float alpha) {
    matrix_2x3 out;
    out.col0.x = std::cos(alpha);
    out.col0.y = std::sin(alpha);
    out.col1.x = std::sin(alpha) * (-1.f);
    out.col1.y = std::cos(alpha);
    return out;
}

vec2 operator*(const matrix_2x3 &m, const vec2 &v) {
    vec2 out;
    out.x = m.col0.x * v.x + m.col1.x * v.y;
    out.y = m.col0.y * v.x + m.col1.y * v.y;
    return out;
}

matrix_2x3 operator*(const matrix_2x3 &m0, const matrix_2x3 &m1) {
    matrix_2x3 out;
    out.col0.x = m0.col0.x * m1.col0.x + m0.col1.x * m1.col0.y;
    out.col1.x = m0.col0.x * m1.col1.x + m0.col1.x * m1.col1.y;
    out.col0.y = m0.col0.y * m1.col0.x + m0.col1.y * m1.col0.y;
    out.col1.y = m0.col0.y * m1.col1.x + m0.col1.y * m1.col1.y;
    out.col2.x = m0.col2.x * m1.col0.x + m0.col2.y * m1.col0.y + m1.col2.x;
    out.col2.y = m0.col2.x * m1.col1.x + m0.col2.y * m1.col1.y + m1.col2.y;
    return out;
}
} // namespace om