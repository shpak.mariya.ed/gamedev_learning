#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <istream>
#include <memory>
#include <string_view>

#include "engine.h"

int main(int /*argc*/, char * /*argv*/[]) {
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error = engine->initialize(
        "title=MORPHING;window_width=640;window_height=480;");
    if (!error.empty()) {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    float speed = 0.5;
    bool continue_loop = true;
    while (continue_loop) {
        om::event event;
        while (engine->read_input(event)) {
            if (event == om::event::turn_off) {
                continue_loop = false;
            } else if (event == om::event::down_pressed) {
                speed -= 0.1;
                if (speed < 0.1) speed = 0.1;
            } else if (event == om::event::up_pressed) {
                speed += 0.1;
                if (speed > 1.0) speed = 1.0;
            } else if (event == om::event::space_pressed) {
                speed = 0.5;
            }
        }

        om::triangle_buffer tb0;
        std::ifstream buffer_0("../buffer_0.txt");
        assert(!!buffer_0);
        buffer_0 >> tb0;

        om::triangle_buffer tb1;
        std::ifstream buffer_1("../buffer_1.txt");
        assert(!!buffer_1);
        buffer_1 >> tb1;

        float time = engine->get_time();
        float step = time / 1000.0f * speed;
        float int_part = 0;
        float frac_part = modff(step, &int_part);

        float t = 0;
        bool reverse = int(int_part) % 2 != 1;
        if (reverse) t = frac_part;
        else
            t = 1 - frac_part;

        engine->render(tb0, tb1, t);

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}