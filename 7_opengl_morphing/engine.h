#include <iosfwd>
#include <string>
#include <string_view>
#include <vector>

#ifndef OM_DECLSPEC
#define OM_DECLSPEC
#endif

namespace om {
enum class event { up_pressed, down_pressed, space_pressed, turn_off };

class engine;

OM_DECLSPEC engine *create_engine();
OM_DECLSPEC void destroy_engine(engine *e);

struct OM_DECLSPEC vertex {
    float x = 0.f;
    float y = 0.f;
    float z = 0.f;
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
};

struct OM_DECLSPEC triangle {
    triangle() {
        v[0] = vertex();
        v[1] = vertex();
        v[2] = vertex();
    }
    vertex v[3];
};

using triangle_buffer = std::vector<triangle>;

OM_DECLSPEC std::istream &operator>>(std::istream &is, vertex &);
OM_DECLSPEC std::istream &operator>>(std::istream &is, triangle &);
OM_DECLSPEC std::istream &operator>>(std::istream &is, triangle_buffer &);

class OM_DECLSPEC engine {
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view config) = 0;
    virtual float get_time() = 0;
    virtual void set_uniform(std::string_view, float) = 0;
    virtual bool read_input(event &e) = 0;
    virtual void render(const triangle_buffer &, const triangle_buffer &,
                        float) = 0;
    virtual void swap_buffers() = 0;
    virtual void uninitialize() = 0;
};

} // end namespace om