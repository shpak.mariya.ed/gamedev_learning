#version 330 core
layout (location = 0) in vec3 v0;
layout (location = 1) in vec3 c0;
layout (location = 2) in vec3 v1;
layout (location = 3) in vec3 c1;

out vec3 color;

uniform float t;

float interpolate(float f0, float f1) {
    return (1.0f - t) * f0 + t * f1;
}

void main() {
    float x = interpolate(v0.x, v1.x);
    float y = interpolate(v0.y, v1.y);
    float z = interpolate(v0.z, v1.z);
    float r = interpolate(c0.r, c1.r);
    float g = interpolate(c0.g, c1.g);
    float b = interpolate(c0.b, c1.b);

    color = vec3(r, g, b);
    gl_Position = vec4(x, y, z, 1.0);
}