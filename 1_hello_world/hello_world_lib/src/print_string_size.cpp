#include <iostream>

void print_string_size(std::ostream &os, std::string_view str) {
    os << "string size: " << str.size() << std::endl;
}