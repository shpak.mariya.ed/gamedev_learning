#include <iostream>

void print_string(std::ostream &os, std::string_view str) {
    os << "string: " << str << std::endl;
}