#pragma once
#include <iosfwd>
#include <string_view>

void print_string(std::ostream &, std::string_view);