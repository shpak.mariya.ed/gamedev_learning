#pragma once
#include <iosfwd>
#include <string_view>

void print_string_size(std::ostream &, std::string_view);