#include <iostream>
#include <print_string.h>
#include <print_string_size.h>

int main(int, char **args, char **envs) {
    std::string_view hello = "Hello";
    print_string(std::cout, hello);
    print_string_size(std::cout, hello);
    return std::cout.fail();
}