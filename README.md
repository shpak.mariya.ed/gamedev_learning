# On the way to gamedev

## Status             

[![Build status](https://gitlab.com/shpak.mariya.ed/gamedev_learning/badges/master/pipeline.svg)](https://gitlab.com/shpak.mariya.ed/gamedev_learning/pipelines)


## Projects

- [01_hello_world](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/hello_world)
- [02_sdl_dynamic](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/2_sdl_dynamic)
- [02_sdl_static](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/2_sdl_static)
- [03_engine](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/3_engine)
- [04_render](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/4_render)
- [05_opengl_minimal](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/5_opengl_minimal)
- [05_opengl_triangles](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/5_opengl_triangles)
- [05_opengl_renderdoc](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/5_opengl_renderdoc)
- [06_opengl_grid](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/6_opengl_grid)
- [06_opengl_grid_runtime](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/6_opengl_grid_runtime)
- [07_opengl_morphing](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/7_opengl_morphing)
- [07_opengl_texture](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/7_opengl_texture)
- [08_opengl_general](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/8_opengl_general)
- [09_model_moving](https://gitlab.com/shpak.mariya.ed/gamedev_learning/-/tree/master/9_model_moving)

   
## Software stack

Technologies which I use for development:

- Ubuntu 20.04.0
- GCC 9.2.0+
- CMake 3.17.0+
- Git 2.20.0+


## Docker

You can look at the docker image I use [on Docker Hub](https://hub.docker.com/r/shpakmariya/gamedev_learning/) or just pull that:

    docker pull shpakmariya/gamedev_learning