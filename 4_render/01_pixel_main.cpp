#include "01_pixel.h"

int main() {
    const std::string file_name_1 = "01_pixel_one_color.ppm";
    const std::string file_name_2 = "01_pixel_random_colors.ppm";
    const std::string file_name_3 = "01_pixel_one.ppm";
    const coord tmp_coord = {static_cast<double>(settings::image_width) / 2,
                             static_cast<double>(settings::image_height) / 2};

    pixel_render pixels;
    pixels.clear(colors::def);
    pixels.draw(file_name_1, true);

    pixels.set_random_pixels();
    pixels.draw(file_name_2, true);
    pixels.clear(colors::black);

    pixels.set_pixel(tmp_coord, colors::def);
    pixels.draw(file_name_3, true);

    return EXIT_SUCCESS;
}