#include "01_pixel.h"

bool operator==(const coord &c0, const coord &c1) {
    return c0.x == c1.x && c0.y == c1.y;
}

coord operator-(const coord &c0, const coord &c1) {
    return {c0.x - c1.x, c0.y - c0.y};
}

pixel_render::pixel_render() { pixel_buffer = new canvas; }

pixel_render::~pixel_render() { delete pixel_buffer; }

void pixel_render::clear(const color &color) { (*pixel_buffer).fill(color); }

void pixel_render::draw(const std::string &file_name, bool check) {
    pixel_buffer->save_image(file_name);

    if (check) {
        canvas tmp;
        tmp.load_image(file_name);
        if (tmp != (*pixel_buffer)) {
            throw std::runtime_error(
                "Error writing file: Saved and loaded images are not equal!");
        }
    }
}

color pixel_render::get_random_color() {
    std::random_device dev;
    std::uniform_int_distribution<uint8_t> dist(0, 255);
    color c = {dist(dev), dist(dev), dist(dev)};

    return c;
}

void pixel_render::set_pixel(const coord &coord, const color &color) {
    if (coord.x < 0) {
        std::cerr << "Incorrect x value: " << coord.x << "!" << std::endl;
        throw std::runtime_error(
            "Error during recording element in the array.");
    }
    if (coord.y < 0) {
        std::cerr << "Incorrect y value: " << coord.y << "!" << std::endl;
        throw std::runtime_error(
            "Error during recording element in the array.");
    }

    uint32_t x = coord.x;
    uint32_t y = coord.y;
    uint32_t n = (y * settings::image_width) + x;

    if (n >= pixel_buffer->size()) {
        std::cerr << "Cannot write item with index " << n
                  << " to the container!" << std::endl;
        throw std::runtime_error(
            "Error during recording element in the array.");
    }

    (*pixel_buffer)[n] = color;
}

void pixel_render::set_random_pixels() {
    for (auto &pixel : (*pixel_buffer)) {
        pixel = get_random_color();
    }
}