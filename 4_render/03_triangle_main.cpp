#include "03_triangle.h"

int main() {
    const std::string file_name_1 = "03_triangle_one.ppm";
    const std::string file_name_2 = "03_triangles_by_vb.ppm";
    const std::string file_name_3 = "03_triangles_by_ib.ppm";
    const std::string file_name_4 = "03_triangles_grid.ppm";

    triangle_render triangles;
    triangles.set_triangle({20, 20}, {20, 100}, {100, 100}, colors::def);
    triangles.draw(file_name_1);
    triangles.clear(colors::black);

    const vertex_buffer vb_1 = {{0, 0},   {0, 100},   {100, 0},
                                {100, 0}, {100, 100}, {0, 100}};
    triangles.set_triangles_by_vb(vb_1);
    triangles.draw(file_name_2);
    triangles.clear(colors::black);

    const vertex_buffer vb_2 = {
        {0, 0}, {0, 100}, {100, 0}, {100, 100}, {50, 150}};
    const index_buffer ib_1 = {0, 1, 2, 3, 2, 1, 1, 3, 4};
    triangles.set_triangles_by_ib(vb_2, ib_1);
    triangles.draw(file_name_3);
    triangles.clear(colors::black);

    buffers bf = triangle_render::get_triangular_grid(10);
    triangles.set_triangles_by_ib(bf.vb, bf.ib);
    triangles.draw(file_name_4);

    return EXIT_SUCCESS;
}