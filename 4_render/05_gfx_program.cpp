#include "05_gfx_program.h"

void program::set_uniforms(const uniforms &u) {
    mouse_x = u.f0;
    mouse_y = u.f1;
    radius = u.f2;

    if (radius <= 0) {
        throw std::runtime_error("Error: Radius cannot be 0 or less!");
    }
}

vertex program::vertex_shader(const vertex &v) {
    vertex out = v;

    double dx = v.f0 - mouse_x;
    double dy = v.f1 - mouse_y;

    if ((dx * dx + dy * dy) <= radius * radius) {
        double hyp = std::sqrt(dx * dx + dy * dy);
        double tx = dx / hyp;
        double ty = dy / hyp;
        double radius_x = tx * radius + mouse_x;
        double radius_y = ty * radius + mouse_y;

        out.f0 = (out.f0 + radius_x) / 2;
        out.f1 = (out.f1 + radius_y) / 2;
    }

    if (out.f0 < 0) out.f0 = 0;
    if (out.f0 >= settings::image_width) out.f0 = settings::image_width - 1;
    if (out.f1 < 0) out.f1 = 0;
    if (out.f1 >= settings::image_height) out.f1 = settings::image_height - 1;

    return out;
}

color program::fragment_shader(const vertex &v) {
    color c = {static_cast<uint8_t>(v.f2), static_cast<uint8_t>(v.f3),
               static_cast<uint8_t>(v.f4)};

    double dx = v.f0 - mouse_x;
    double dy = v.f1 - mouse_y;
    double pow_r = radius * radius;

    if (dx * dx + dy * dy <= pow_r) {
        double hyp = sqrt(pow(dx, 2) + pow(dy, 2));
        double t = hyp / radius;
        c.r = uint8_t(interpolate(colors::red.r, v.f2, t));
        c.g = uint8_t(interpolate(colors::red.g, v.f3, t));
        c.b = uint8_t(interpolate(colors::red.b, v.f4, t));
    }

    return c;
}