#include "04_interpolated_triangle.h"

double interpolate(const double f0, const double f1, const double t) {
    if (t < 0 || t > 1)
        throw std::runtime_error("Value t is not in range [0, 1]!");
    return f0 + (f1 - f0) * t;
}

vertex interpolate(const vertex &v0, const vertex &v1, const double t) {
    return {interpolate(v0.f0, v1.f0, t), interpolate(v0.f1, v1.f1, t),
            interpolate(v0.f2, v1.f2, t), interpolate(v0.f3, v1.f3, t),
            interpolate(v0.f4, v1.f4, t), interpolate(v0.f5, v1.f5, t),
            interpolate(v0.f6, v1.f6, t), interpolate(v0.f7, v1.f7, t)};
}

static vertex_buffer interpolate_x_line(const vertex &v0, const vertex &v1) {
    vertex_buffer out;

    double dx = std::round(v1.f0 - v0.f0);
    int x_start_idx = static_cast<int>(std::round(v0.f0));
    int x_end_idx = static_cast<int>(std::round(v1.f0));
    int x_count = x_end_idx - x_start_idx + 1;
    double f1 = std::round(v0.f1);

    for (int x = 0; x < x_count; ++x) {
        double t;
        if (dx == 0.0) t = 0;
        else
            t = static_cast<double>(x) / x_count;

        vertex tmp_v = interpolate(v0, v1, t);
        tmp_v.f0 = x_start_idx + x;
        tmp_v.f1 = f1;
        out.push_back(tmp_v);
    }
    return out;
}

static vertex_buffer interpolate_triangle(const vertex &v0, const vertex &v1,
                                          const vertex &v2) {
    vertex_buffer out;

    double dy = std::abs(v2.f1 - v0.f1);
    for (size_t y = 0; y <= dy; ++y) {
        double t;
        if (dy == 0) t = 0;
        else
            t = static_cast<double>(y) / dy;
        vertex tmp_v1 = interpolate(v0, v1, t);
        vertex tmp_v2 = interpolate(v0, v2, t);

        vertex_buffer tmp_vb = interpolate_x_line(tmp_v1, tmp_v2);
        for (auto &tmp_v : tmp_vb) {
            out.push_back(tmp_v);
        }
    }
    return out;
}

struct sort_by_y {
    inline bool operator()(const vertex &v0, const vertex &v1) {
        if (v0.f1 == v1.f1) {
            return (v0.f0 < v1.f0);
        }
        return (v0.f1 < v1.f1);
    }
};

vertex_buffer int_triangle_render::set_int_triangle(const vertex &v0,
                                                    const vertex &v1,
                                                    const vertex &v2,
                                                    bool is_render) {
    vertex_buffer out;

    // 1) sort by y key
    vertex_buffer vertexes;
    vertexes.push_back(v0);
    vertexes.push_back(v1);
    vertexes.push_back(v2);

    std::sort(begin(vertexes), end(vertexes), sort_by_y());

    vertex top = vertexes.at(0);
    vertex middle = vertexes.at(1);
    vertex bottom = vertexes.at(2);

    // 2.0) 0th case: need to set only one line
    if (top.f1 == middle.f1 && middle.f1 == bottom.f1) {
        vertex_buffer tmp_vb = interpolate_x_line(top, bottom);
        for (auto &tmp_v : tmp_vb) {
            out.push_back(tmp_v);
        }
    }
    // 2.1) 1th case: middle.y == bottom.y:
    else if (middle.f1 == bottom.f1) {
        out = interpolate_triangle(top, middle, bottom);
    }
    // 2.2) 2th case: middle.y == top.y:
    else if (middle.f1 == top.f1) {
        out = interpolate_triangle(bottom, top, middle);
    } else {
        // 2.3) 3th case: need to set horizontal dividing line
        double t =
            std::abs(bottom.f1 - middle.f1) / std::abs(bottom.f1 - top.f1);
        vertex sep = interpolate(bottom, top, t);

        vertex_buffer out_top = interpolate_triangle(top, middle, sep);
        vertex_buffer out_bottom = interpolate_triangle(bottom, middle, sep);

        for (auto &tmp_v : out_top) {
            out.push_back(tmp_v);
        }
        for (auto &tmp_v : out_bottom) {
            out.push_back(tmp_v);
        }
    }

    if (is_render) {
        for (auto &v : out) {
            coord coord = {v.f0, v.f1};
            color color = {static_cast<uint8_t>(v.f2),
                           static_cast<uint8_t>(v.f3),
                           static_cast<uint8_t>(v.f4)};
            set_pixel(coord, color);
        }
    }

    return out;
}

void int_triangle_render::set_gfx_program(gfx_program &p) { program = &p; }

void int_triangle_render::draw_int_triangles(const vertex_buffer &vb,
                                             const index_buffer &ib) {
    for (size_t index = 0; index < ib.size(); index += 3) {
        const uint16_t index0 = ib.at(index + 0);
        const uint16_t index1 = ib.at(index + 1);
        const uint16_t index2 = ib.at(index + 2);

        const vertex &v0 = vb.at(index0);
        const vertex &v1 = vb.at(index1);
        const vertex &v2 = vb.at(index2);

        const vertex v0_ = program->vertex_shader(v0);
        const vertex v1_ = program->vertex_shader(v1);
        const vertex v2_ = program->vertex_shader(v2);

        // false - don't set pixels before calling fragment shaders
        vertex_buffer fragments = set_int_triangle(v0_, v1_, v2_, false);

        for (auto &f : fragments) {
            coord coord = {f.f0, f.f1};
            color color = program->fragment_shader(f);
            set_pixel(coord, color);
        }
    }
}