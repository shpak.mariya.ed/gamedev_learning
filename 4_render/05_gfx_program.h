#pragma once
#include "04_interpolated_triangle.h"

struct program : public gfx_program {
    ~program() = default;

    void set_uniforms(const uniforms &) override;
    vertex vertex_shader(const vertex &) override;
    color fragment_shader(const vertex &) override;

private:
    double mouse_x;
    double mouse_y;
    double radius;
};