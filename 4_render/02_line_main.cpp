#include "02_line.h"

int main() {
    std::string file_name_1 = "02_line_one.ppm";
    std::string file_name_2 = "02_line_random_lines.ppm";

    line_render lines;
    lines.set_line({0, 100}, {60, 80}, colors::def);
    lines.draw(file_name_1);
    lines.clear(colors::black);

    lines.set_random_lines(20);
    lines.draw(file_name_2);

    return EXIT_SUCCESS;
}