#pragma once
#include "00_canvas.h"

struct coord {
    double x = 0;
    double y = 0;

    friend bool operator==(const coord &, const coord &);
    friend coord operator-(const coord &, const coord &);
};

struct render {
    virtual ~render() = default;
    virtual void clear(const color&) = 0;
    virtual void set_pixel(const coord&, const color&) = 0;
};

struct pixel_render : public render {
    canvas *pixel_buffer;

    pixel_render();
    ~pixel_render() override;

    void clear(const color& color) override;
    // "check = true" starts auto checking of the saved file
    // by loading that file and comparing
    void draw(const std::string &, bool check = false);
    void set_pixel(const coord&, const color&) override;
    void set_random_pixels();
    static color get_random_color();
};