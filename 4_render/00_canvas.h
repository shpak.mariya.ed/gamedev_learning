#include <algorithm>
#include <array>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <random>
#include <string>

namespace settings {
constexpr size_t image_width = 480;
constexpr size_t image_height = 360;
const size_t buffer_size = image_width * image_height;
} // namespace settings

#pragma pack(push, 1)
struct color {
    uint8_t r = 0;
    uint8_t g = 0;
    uint8_t b = 0;

    friend bool operator==(const color &, const color &);
};
#pragma pack(pop)

namespace colors {
const color def = {0, 255, 0};
const color black = {0, 0, 0};
const color red = {255, 0, 0};
const color green = {0, 255, 0};
const color blue = {0, 0, 255};
} // namespace colors

class canvas : public std::array<color, settings::buffer_size> {
public:
    void save_image(const std::string &);
    void load_image(const std::string &);
};