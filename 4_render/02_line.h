#pragma once
#include "01_pixel.h"

struct line_render : public pixel_render {
    void set_line(const coord &, const coord &, const color &);
    void set_random_lines(size_t);
};