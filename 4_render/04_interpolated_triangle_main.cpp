#include "04_interpolated_triangle.h"

int main() {
    const std::string file_name = "04_int_triangle.ppm";
    vertex v0 = {100, 30, 255, 0, 0};
    vertex v1 = {30, 100, 0, 255, 0};
    vertex v2 = {150, 150, 0, 0, 255};

    int_triangle_render triangle;
    triangle.set_int_triangle(v0, v1, v2);
    triangle.draw(file_name, true);

    return EXIT_SUCCESS;
}