#include "05_gfx_program.h"
#include <SDL.h>

int main() {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Window *window = SDL_CreateWindow(
        "runtime soft render", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        settings::image_width, settings::image_height, SDL_WINDOW_OPENGL);
    if (window == nullptr) {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    SDL_Renderer *renderer =
        SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) {
        std::cerr << SDL_GetError() << std::endl;
        return EXIT_FAILURE;
    }

    int_triangle_render int_render;
    program gfx;
    int_render.set_gfx_program(gfx);

    void *pixels = int_render.pixel_buffer->data();
    const int depth = sizeof(color) * 8;
    const int pitch = settings::image_width * sizeof(color);
    const int rmask = 0x000000ff;
    const int gmask = 0x0000ff00;
    const int bmask = 0x00ff0000;
    const int amask = 0;

    double mouse_x = 0.0;
    double mouse_y = 0.0;
    double radius = 50.0;
    buffers bf = int_triangle_render::get_linear_grid(20);
    bool continue_loop = true;

    while (continue_loop) {
        SDL_Event e;
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                continue_loop = false;
                break;
            } else if (e.type == SDL_MOUSEMOTION) {
                mouse_x = e.motion.x;
                mouse_y = e.motion.y;
            }
        }

        int_render.clear({0, 0, 0});
        gfx.set_uniforms(uniforms{mouse_x, mouse_y, radius});
        int_render.set_gfx_program(gfx);
        int_render.draw_int_triangles(bf.vb, bf.ib);

        SDL_Surface *bitmapSurface = SDL_CreateRGBSurfaceFrom(
            pixels, settings::image_width, settings::image_height, depth, pitch,
            rmask, gmask, bmask, amask);
        if (bitmapSurface == nullptr) {
            std::cerr << SDL_GetError() << std::endl;
            return EXIT_FAILURE;
        }

        SDL_Texture *bitmapTex =
            SDL_CreateTextureFromSurface(renderer, bitmapSurface);
        if (bitmapTex == nullptr) {
            std::cerr << SDL_GetError() << std::endl;
            return EXIT_FAILURE;
        }

        SDL_FreeSurface(bitmapSurface);
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, bitmapTex, nullptr, nullptr);
        SDL_RenderPresent(renderer);
        SDL_DestroyTexture(bitmapTex);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return EXIT_SUCCESS;
}