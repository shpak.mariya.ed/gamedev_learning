#include "02_line.h"
#include <cmath>
#include <vector>

void line_render::set_line(const coord &c0, const coord &c1,
                           const color &color) {
    if (c0 == c1) {
        set_pixel({c0}, color);
        return;
    }

    double dx = c0.x - c1.x;
    double dy = c0.y - c1.y;
    uint16_t abs_x = std::abs(dx);
    uint16_t abs_y = std::abs(dy);
    uint16_t count = abs_x + 1;
    double x0 = c0.x, x1 = c1.x, y0 = c0.y, y1 = c1.y;
    bool hor = true;

    // if line is more vertical
    if (abs_x < abs_y) {
        hor = false;
        std::swap(x0, y0);
        std::swap(x1, y1);
        count = abs_y + 1;
    }

    double d = (y1 - y0) / (x1 - x0);
    if ((x1 - x0) == 0) d = y1 - y0;
    bool to_right = (static_cast<int16_t>(x0 - x1) > 0);
    uint16_t x = x0, y;

    for (size_t i = 0; i < count; ++i) {
        y = d * (x - x0) + y0;
        if (hor) {
            set_pixel({static_cast<double>(x), static_cast<double>(y)},
                      color);
        } else {
            set_pixel({static_cast<double>(y), static_cast<double>(x)},
                      color);
        }
        if (!to_right) ++x;
        else
            --x;
    }
}

void line_render::set_random_lines(size_t n) {
    std::random_device dev;
    std::uniform_int_distribution<int> dist_x(0, settings::image_width - 1);
    std::uniform_int_distribution<int> dist_y(0, settings::image_height - 1);

    coord c0, c1;
    color color;

    for (size_t i = 0; i < n; ++i) {
        c0 = {static_cast<double>((dist_x(dev))), static_cast<double>(dist_y(dev))};
        c1 = {static_cast<double>(dist_x(dev)), static_cast<double>(dist_y(dev))};

        color = get_random_color();
        set_line(c0, c1, color);
    }
}