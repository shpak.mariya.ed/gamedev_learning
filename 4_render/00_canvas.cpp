#include "00_canvas.h"
#include <fstream>

bool operator==(const color &c0, const color &c1) {
    return (c0.r == c1.r && c0.g == c1.g && c0.b == c1.b);
}

void canvas::save_image(const std::string &file_name) {
    try {
        std::ofstream file(file_name, std::ios_base::binary);
        // adding header info
        file << "P6" << std::endl
             << settings::image_width << ' ' << settings::image_height
             << std::endl
             << 255 << std::endl;

        auto s = static_cast<std::streamsize>(sizeof(color) * size());
        file.write(reinterpret_cast<const char *>(this), s);

        file.close();
    } catch (std::ofstream::failure &e) {
        throw e;
    }
}

void canvas::load_image(const std::string &file_name) {
    try {
        std::string line;
        std::ifstream file(file_name, std::ios_base::binary);
        std::string header, color_max_value;
        size_t width, height;

        file >> header >> width >> height >> color_max_value >> std::ws;
        if ((width * height) != size()) {
            std::cerr << "Cannot load file with other size!";
            throw std::runtime_error("Wrong loaded image's size.");
        }

        auto s = static_cast<std::streamsize>(sizeof(color) * size());
        file.read(reinterpret_cast<char *>(this), s);

        file.close();
    } catch (std::ofstream::failure &e) {
        throw e;
    }
}