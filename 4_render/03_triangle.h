#pragma once
#include "02_line.h"

struct vertex {
    double f0; // x
    double f1; // y
    double f2; // r
    double f3; // g
    double f4; // b
    double f5;
    double f6;
    double f7;
};

using vertex_buffer = std::vector<vertex>;
using index_buffer = std::vector<uint32_t>;

struct buffers {
    vertex_buffer vb;
    index_buffer ib;
};

struct triangle_render : public line_render {
    void set_triangle(const coord &, const coord &, const coord &,
                      const color &color = colors::def);
    // using only vertex buffer
    void set_triangles_by_vb(const vertex_buffer &,
                             const color &color = colors::def);
    // using vertex + index buffers
    void set_triangles_by_ib(const vertex_buffer &, const index_buffer &,
                             const color &color = colors::def);

    // get buffers of the same grids, but
    // 1) consists of triangles - 2 in 1 block
    static buffers get_triangular_grid(uint8_t, const color &c = colors::def);
    // 2) consists of lines (that are essentially triangles) - 5 in 1 block
    static buffers get_linear_grid(uint8_t, const color &c = colors::def);
};