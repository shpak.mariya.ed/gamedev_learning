#pragma once
#include "03_triangle.h"

struct uniforms {
    double f0 = 0;
    double f1 = 0;
    double f2 = 0;
    double f3 = 0;
    double f4 = 0;
    double f5 = 0;
    double f6 = 0;
    double f7 = 0;
};

struct gfx_program {
    ~gfx_program() = default;
    virtual void set_uniforms(const uniforms &) = 0;
    virtual vertex vertex_shader(const vertex &) = 0;
    virtual color fragment_shader(const vertex &) = 0;
};

double interpolate(const double, const double, const double);
vertex interpolate(const vertex &, const vertex &, const double);

struct int_triangle_render : public triangle_render {
    // flag "is_render=true" call rendering of triangle
    vertex_buffer set_int_triangle(const vertex &, const vertex &,
                                   const vertex &, bool is_render = true);
    void set_gfx_program(gfx_program &);
    void draw_int_triangles(const vertex_buffer &, const index_buffer &);

private:
    gfx_program *program = nullptr;
};