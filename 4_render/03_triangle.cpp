#include "03_triangle.h"

void triangle_render::set_triangle(const coord &c0, const coord &c1,
                                   const coord &c2, const color &color) {
    set_line(c0, c1, color);
    set_line(c1, c2, color);
    set_line(c2, c0, color);
}

void triangle_render::set_triangles_by_vb(const vertex_buffer &vb,
                                          const color &color) {
    const uint8_t step = 3;
    uint16_t mod = vb.size() % step;
    if (mod != 0) {
        std::cerr << "Vertex buffer is not correct. The last " << mod
                  << " element(s) will not be used." << std::endl;
    }

    size_t i = 0;
    while ((i + step) <= vb.size()) {
        const coord v0 = {vb.at(i).f0, vb.at(i).f1};
        const coord v1 = {vb.at(i + 1).f0, vb.at(i + 1).f1};
        const coord v2 = {vb.at(i + 2).f0, vb.at(i + 2).f1};

        set_triangle(v0, v1, v2, color);
        i += step;
    }
}

void triangle_render::set_triangles_by_ib(const vertex_buffer &vb,
                                          const index_buffer &ib,
                                          const color &color) {
    const uint8_t step = 3;
    uint16_t mod = ib.size() % step;
    if (mod != 0) {
        std::cerr << "Index buffer is not correct. The last " << mod
                  << " element(s) will not be used." << std::endl;
    }

    size_t i = 0;
    while ((i + step) <= ib.size()) {
        const uint32_t i0 = ib.at(i);
        const uint32_t i1 = ib.at(i + 1);
        const uint32_t i2 = ib.at(i + 2);

        const coord v0 = {vb.at(i0).f0, vb.at(i0).f1};
        const coord v1 = {vb.at(i1).f0, vb.at(i1).f1};
        const coord v2 = {vb.at(i2).f0, vb.at(i2).f1};

        set_triangle(v0, v1, v2, color);
        i += step;
    }
}

buffers triangle_render::get_triangular_grid(uint8_t n, const color &c) {
    uint32_t h = (settings::image_height - 1) / n;
    uint32_t w = (settings::image_width - 1) / n;

    double r = c.r;
    double g = c.g;
    double b = c.b;

    vertex_buffer vb;

    for (size_t i = 0; i <= h; ++i) {
        for (size_t j = 0; j <= w; ++j) {
            double x = static_cast<double>(j) * n;
            double y = static_cast<double>(i) * n;
            vb.push_back(vertex{x, y, r, g, b});
        }
    }

    index_buffer ib;
    uint32_t i_0 = 0, i_1 = 0, i_2 = 0, i_3 = 0;

    // to calculate which part of figure used
    bool check_left_down = (w % 2 == 0);
    uint8_t left_down = 0;
    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            i_0 = i * (w + 1) + j;
            i_1 = i_0 + 1;
            i_2 = i_0 + w + 1;
            i_3 = i_2 + 1;
            if (left_down % 2 == 0) {
                if (i % 2 == 0) {
                    ib.push_back(i_0);
                    ib.push_back(i_1);
                    ib.push_back(i_2);

                    ib.push_back(i_1);
                    ib.push_back(i_2);
                    ib.push_back(i_3);
                } else {
                    ib.push_back(i_0);
                    ib.push_back(i_1);
                    ib.push_back(i_3);

                    ib.push_back(i_0);
                    ib.push_back(i_2);
                    ib.push_back(i_3);
                }
            } else {
                if (i % 2 == 0) {
                    ib.push_back(i_0);
                    ib.push_back(i_1);
                    ib.push_back(i_3);

                    ib.push_back(i_0);
                    ib.push_back(i_2);
                    ib.push_back(i_3);
                } else {
                    ib.push_back(i_0);
                    ib.push_back(i_1);
                    ib.push_back(i_2);

                    ib.push_back(i_1);
                    ib.push_back(i_2);
                    ib.push_back(i_3);
                }
            }
            ++left_down;
        }
        if (!check_left_down) ++left_down;
    }

    return {vb, ib};
}

buffers triangle_render::get_linear_grid(uint8_t n, const color &c) {
    uint32_t h = (settings::image_height - 1) / n;
    uint32_t w = (settings::image_width - 1) / n;

    double r = c.r;
    double g = c.g;
    double b = c.b;

    vertex_buffer vb;

    for (size_t i = 0; i <= h; ++i) {
        for (size_t j = 0; j <= w; ++j) {
            double x = static_cast<double>(j) * n;
            double y = static_cast<double>(i) * n;
            vb.push_back(vertex{x, y, r, g, b});
        }
    }

    index_buffer ib;
    uint32_t i_0 = 0, i_1 = 0, i_2 = 0, i_3 = 0;

    // to calculate which part of figure used
    bool continue_priority = (w % 2 != 0);
    uint8_t left_down = 0;

    for (size_t i = 0; i < h; ++i) {
        for (size_t j = 0; j < w; ++j) {
            i_0 = i * (w + 1) + j;
            i_1 = i_0 + 1;
            i_2 = i_0 + w + 1;
            i_3 = i_2 + 1;

            ib.push_back(i_0);
            ib.push_back(i_0);
            ib.push_back(i_1);

            ib.push_back(i_0);
            ib.push_back(i_0);
            ib.push_back(i_2);

            ib.push_back(i_2);
            ib.push_back(i_2);
            ib.push_back(i_3);

            ib.push_back(i_1);
            ib.push_back(i_1);
            ib.push_back(i_3);

            if (left_down % 2 == 0) {
                ib.push_back(i_1);
                ib.push_back(i_1);
                ib.push_back(i_2);
            } else {
                ib.push_back(i_0);
                ib.push_back(i_0);
                ib.push_back(i_3);
            }
            ++left_down;
        }
        if (!continue_priority) ++left_down;
    }

    return {vb, ib};
}