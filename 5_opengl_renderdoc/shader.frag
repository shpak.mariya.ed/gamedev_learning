#version 330 core
in vec4 v_position;
in vec3 v_color;
out vec4 FragColor;

void main() {
    float z = v_position.z;
    float r = v_color.r;
    float g = v_color.g;
    float b = v_color.b;
    float def_green = 0.8f;

    if (z == 0.0) {
        FragColor = vec4(r, g, b, 1.0);
    } else {
        float t = z * 0.5;
        FragColor = vec4(r - t, g - t, b - t, 1.0);
    }
}