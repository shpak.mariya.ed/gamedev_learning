#include "engine.h"
#include "glad/glad.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <charconv>
#include <chrono>
#include <cmath>
#include <exception>
#include <iostream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <vector>

#include <SDL.h>

#define OM_GL_CHECK()                                                          \
    {                                                                          \
        const int err = static_cast<int>(glGetError());                        \
        if (err != GL_NO_ERROR) {                                              \
            switch (err) {                                                     \
            case GL_INVALID_ENUM:                                              \
                std::cerr << "GL_INVALID_ENUM" << std::endl;                   \
                break;                                                         \
            case GL_INVALID_VALUE:                                             \
                std::cerr << "GL_INVALID_VALUE" << std::endl;                  \
                break;                                                         \
            case GL_INVALID_OPERATION:                                         \
                std::cerr << "GL_INVALID_OPERATION" << std::endl;              \
                break;                                                         \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                             \
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;  \
                break;                                                         \
            case GL_OUT_OF_MEMORY:                                             \
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                  \
                break;                                                         \
            }                                                                  \
            assert(false);                                                     \
        }                                                                      \
    }

namespace om {

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam);

static std::array<std::string_view, 17> event_names = {
    {"left_pressed", "left_released", "right_pressed", "right_released",
     "up_pressed", "up_released", "down_pressed", "down_released",
     "select_pressed", "select_released", "start_pressed", "start_released",
     "button1_pressed", "button1_released", "button2_pressed",
     "button2_released", "turn_off"}};

std::ostream &operator<<(std::ostream &stream, const event &e) {
    auto value = static_cast<std::uint32_t>(e);
    auto minimal = static_cast<std::uint32_t>(event::left_pressed);
    auto maximal = static_cast<std::uint32_t>(event::turn_off);
    if (value >= minimal && value <= maximal) {
        stream << event_names[value];
        return stream;
    } else {
        throw std::runtime_error("Too big event value!");
    }
}

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

std::istream &operator>>(std::istream &is, vertex &v) {
    is >> v.x;
    is >> v.y;
    return is;
}

std::istream &operator>>(std::istream &is, triangle &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

struct bind {
    SDL_Keycode key;
    std::string_view name;
    event event_pressed;
    event event_released;
};

const std::array<bind, 8> keys{
    {{SDLK_w, "up", event::up_pressed, event::up_released},
     {SDLK_a, "left", event::left_pressed, event::left_released},
     {SDLK_s, "down", event::down_pressed, event::down_released},
     {SDLK_d, "right", event::right_pressed, event::right_released},
     {SDLK_LCTRL, "button1", event::button1_pressed, event::button1_released},
     {SDLK_SPACE, "button2", event::button2_pressed, event::button2_released},
     {SDLK_ESCAPE, "select", event::select_pressed, event::select_released},
     {SDLK_RETURN, "start", event::start_pressed, event::start_released}}};

static bool check_input(const SDL_Event &e, const bind *&result) {
    using namespace std;

    const auto it = find_if(begin(keys), end(keys), [&](const bind &b) {
        return b.key == e.key.keysym.sym;
    });

    if (it != end(keys)) {
        result = &(*it);
        return true;
    }
    return false;
}

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }
    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        res = d;
    }
    return res;
}

class engine_impl final : public engine {
public:
    std::string initialize(std::string_view config) final {
        config_map_t config_map = parse_config(config);

        using namespace std;

        stringstream serr;

        SDL_version compiled = {0, 0, 0};
        SDL_version linked = {0, 0, 0};

        SDL_VERSION(&compiled)
        SDL_GetVersion(&linked);

        if (SDL_COMPILEDVERSION !=
            SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
            serr << "Warning: SDL2 compiled and linked version mismatch: "
                 << compiled << " " << linked << endl;
        }

        const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
        if (init_result != 0) {
            const char *err_message = SDL_GetError();
            serr << "Error: failed call SDL_Init: " << err_message << endl;
            return serr.str();
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

        auto title = config_map["title"];
        if (title.empty()) title = "title";
        int window_width = str_to_int(config_map["window_width"], 640);
        int window_height = str_to_int(config_map["window_height"], 480);

        window = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED, window_width,
                                  window_height, ::SDL_WINDOW_OPENGL);

        if (window == nullptr) {
            const char *err_message = SDL_GetError();
            serr << "Error: failed call SDL_CreateWindow: " << err_message
                 << endl;
            SDL_Quit();
            return serr.str();
        }

        int gl_major_ver = 3;
        int gl_minor_ver = 2;
        int gl_context_profile = SDL_GL_CONTEXT_PROFILE_ES;

        std::string_view platform = SDL_GetPlatform();
        using namespace std::string_view_literals;
        auto list = {"Windows"sv, "Apple"sv};
        auto it = std::find(std::begin(list), std::end(list), platform);
        if (it != end(list)) {
            gl_minor_ver = 3;
            gl_context_profile = SDL_GL_CONTEXT_PROFILE_CORE;
        }

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, gl_context_profile);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, gl_major_ver);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, gl_minor_ver);

        SDL_GLContext gl_context = SDL_GL_CreateContext(window);
        if (gl_context == nullptr) {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                                SDL_GL_CONTEXT_PROFILE_CORE);
            gl_context = SDL_GL_CreateContext(window);
        }
        assert(gl_context != nullptr);

        int result =
            SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
        assert(result == 0);

        result =
            SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
        assert(result == 0);

        if (gl_major_ver != 3 || gl_minor_ver != 2) {
            std::clog << "Current context opengl version: " << gl_major_ver
                      << '.' << gl_minor_ver << std::endl
                      << "Need OPENGL ES version at least: 3.2" << std::endl
                      << std::flush;
            throw std::runtime_error("opengl version too low");
        }

        if (gladLoadGLES2Loader(SDL_GL_GetProcAddress) == 0) {
            std::clog << "Error: failed to initialize glad" << std::endl;
        }

        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(callback_opengl_debug, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0,
                              nullptr, GL_TRUE);

        return "";
    }

    bool read_input(event &e) final {
        using namespace std;
        SDL_Event sdl_event;
        if (SDL_PollEvent(&sdl_event)) {
            const bind *binding = nullptr;

            if (sdl_event.type == SDL_QUIT) {
                e = event::turn_off;
                return true;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_pressed;
                    return true;
                }
            } else if (sdl_event.type == SDL_KEYUP) {
                if (check_input(sdl_event, binding)) {
                    e = binding->event_released;
                    return true;
                }
            }
        }
        return false;
    }

    void render_triangle(const triangle &) final {
        auto time_point = std::chrono::steady_clock::now();
        auto time_since_epoch = time_point.time_since_epoch();
        auto ns = time_since_epoch.count();
        float seconds = ns / 1e+9f;
        auto current_color = (std::sin(seconds) + 1.0f) * 0.5f;

        glClearColor(0.f, current_color, 1.f - current_color, 0.0f);
        OM_GL_CHECK()
        glClear(GL_COLOR_BUFFER_BIT);
        OM_GL_CHECK()
    }

    void swap_buffers() final { SDL_GL_SwapWindow(window); }
    void uninitialize() final {}

private:
    SDL_Window *window = nullptr;
};

static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr!");
    }
    delete e;
}

engine::~engine() = default;

static const char *source_to_strv(GLenum source) {
    switch (source) {
    case GL_DEBUG_SOURCE_API:
        return "API";
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        return "SHADER_COMPILER";
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        return "WINDOW_SYSTEM";
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        return "THIRD_PARTY";
    case GL_DEBUG_SOURCE_APPLICATION:
        return "APPLICATION";
    case GL_DEBUG_SOURCE_OTHER:
        return "OTHER";
    }
    return "unknown";
}

static const char *type_to_strv(GLenum type) {
    switch (type) {
    case GL_DEBUG_TYPE_ERROR:
        return "ERROR";
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        return "DEPRECATED_BEHAVIOR";
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        return "UNDEFINED_BEHAVIOR";
    case GL_DEBUG_TYPE_PERFORMANCE:
        return "PERFORMANCE";
    case GL_DEBUG_TYPE_PORTABILITY:
        return "PORTABILITY";
    case GL_DEBUG_TYPE_MARKER:
        return "MARKER";
    case GL_DEBUG_TYPE_PUSH_GROUP:
        return "PUSH_GROUP";
    case GL_DEBUG_TYPE_POP_GROUP:
        return "POP_GROUP";
    case GL_DEBUG_TYPE_OTHER:
        return "OTHER";
    }
    return "unknown";
}

static const char *severity_to_strv(GLenum severity) {
    switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:
        return "HIGH";
    case GL_DEBUG_SEVERITY_MEDIUM:
        return "MEDIUM";
    case GL_DEBUG_SEVERITY_LOW:
        return "LOW";
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        return "NOTIFICATION";
    }
    return "unknown";
}

static std::array<char, GL_MAX_DEBUG_MESSAGE_LENGTH> local_log_buff;

static void APIENTRY callback_opengl_debug(
    GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
    const GLchar *message, [[maybe_unused]] const void *userParam) {
    auto &buff{local_log_buff};
    int num_chars = std::snprintf(
        buff.data(), buff.size(), "%s %s %d %s %.*s\n", source_to_strv(source),
        type_to_strv(type), id, severity_to_strv(severity), length, message);

    if (num_chars > 0) {
        std::cerr.write(buff.data(), num_chars);
    }
}

} // end namespace om