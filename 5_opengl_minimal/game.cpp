#include "engine.h"
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

int main() {
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    engine->initialize("title=OPENGL_TEST_0;window_width=640;window_height=480;");

    bool continue_loop = true;
    while (continue_loop) {
        om::event event;

        while (engine->read_input(event)) {
            std::cout << event << std::endl;
            switch (event) {
            case om::event::turn_off:
                continue_loop = false;
                break;
            default:
                break;
            }
        }

        std::ifstream file("../vertexes.txt");
        assert(!!file);

        om::triangle tr;

        file >> tr;

        engine->render_triangle(tr);

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}