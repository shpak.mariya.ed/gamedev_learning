#include "engine.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <charconv>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

#define OM_GL_CHECK()                                                          \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR) {                                              \
            switch (err) {                                                     \
            case GL_INVALID_ENUM:                                              \
                std::cerr << "GL_INVALID_ENUM" << std::endl;                   \
                break;                                                         \
            case GL_INVALID_VALUE:                                             \
                std::cerr << "GL_INVALID_VALUE" << std::endl;                  \
                break;                                                         \
            case GL_INVALID_OPERATION:                                         \
                std::cerr << "GL_INVALID_OPERATION" << std::endl;              \
                break;                                                         \
            case GL_INVALID_FRAMEBUFFER_OPERATION:                             \
                std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION" << std::endl;  \
                break;                                                         \
            case GL_OUT_OF_MEMORY:                                             \
                std::cerr << "GL_OUT_OF_MEMORY" << std::endl;                  \
                break;                                                         \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }

PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;
PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
PFNGLBUFFERDATAPROC glBufferData = nullptr;

PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = nullptr;
PFNGLUNIFORM1FPROC glUniform1f = nullptr;
PFNGLUNIFORM2FPROC glUniform2f = nullptr;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;

template <typename T>
static void load_gl_func(const char *func_name, T &result) {
    void *gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("Cannot load GL function: ") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

namespace om {

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

using vertex_buffer = std::vector<vertex>;
using index_buffer = std::vector<uint32_t>;

// GRID
class OM_DECLSPEC grid {
    vertex_buffer *vb = nullptr;
    index_buffer *ib = nullptr;

public:
    grid();
    ~grid();
    void init(int, int);
    void generate(int, int, int n = 20);
    void render();
};

grid::grid() = default;
grid::~grid() {
    delete vb;
    delete ib;
}

void grid::init(int w, int h) {
    // setting buffers as fields
    generate(w, h);
}

void grid::generate(int width, int height, int n) {
    float z = 0.0;
    float r = 0.0;
    float g = 0.0;
    float b = 0.0;

    int x_count = (width - 1) / n;
    int y_count = (height - 1) / n;
    float x_step = 2.0f / float(x_count);
    float y_step = 2.0f / float(y_count);

    vb = new vertex_buffer;
    float y_start = 1.0;
    float x_start = -1.0;
    for (int y = 0; y <= y_count; ++y) {
        for (int x = 0; x <= x_count; ++x) {
            float cur_x = x_start + float(x) * x_step;
            float cur_y = y_start - float(y) * y_step;
            vertex v{cur_x, cur_y, z, r, g, b};
            vb->push_back(v);
        }
    }

    ib = new index_buffer;
    // to calculate which part of figure used
    bool continue_priority = (x_count % 2 != 0);
    uint8_t left_down = 0;
    for (size_t i = 0; i < y_count; ++i) {
        for (size_t j = 0; j < x_count; ++j) {
            uint32_t i_0 = i * (x_count + 1) + j;
            uint32_t i_1 = i_0 + 1;
            uint32_t i_2 = i_0 + x_count + 1;
            uint32_t i_3 = i_2 + 1;

            ib->push_back(i_0);
            ib->push_back(i_1);

            ib->push_back(i_0);
            ib->push_back(i_2);

            ib->push_back(i_2);
            ib->push_back(i_3);

            ib->push_back(i_1);
            ib->push_back(i_3);

            if (left_down % 2 == 0) {
                ib->push_back(i_1);
                ib->push_back(i_2);
            } else {
                ib->push_back(i_0);
                ib->push_back(i_3);
            }
            ++left_down;
        }
        if (!continue_priority) ++left_down;
    }
}

void grid::render() {
    // vbo
    glBindBuffer(GL_ARRAY_BUFFER, 1);
    OM_GL_CHECK()
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex) * vb->size(), vb->data(),
                 GL_STATIC_DRAW);

    // ebo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 2);
    OM_GL_CHECK()
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * ib->size(),
                 ib->data(), GL_STATIC_DRAW);
    OM_GL_CHECK()

    glEnableVertexAttribArray(0);
    OM_GL_CHECK()
    GLintptr position_attr_offset = 0;
    OM_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(position_attr_offset));
    OM_GL_CHECK()
    glEnableVertexAttribArray(1);
    OM_GL_CHECK()
    GLintptr color_attr_offset = sizeof(float) * 3;
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(color_attr_offset));
    OM_GL_CHECK()

    glDrawElements(GL_LINES, ib->size(), GL_UNSIGNED_INT, 0);
    OM_GL_CHECK()
}

// ENGINE_IMPL
class engine_impl final : public engine {
public:
    ~engine_impl() override;
    std::string initialize(std::string_view) final;
    void update_user_state(const om::user_state &) final;
    void set_uniform() final;
    bool read_input(event &) final;
    void render_grid() final;
    void render_triangle(const triangle &) final;
    void swap_buffers() final;
    void uninitialize() final;

private:
    SDL_Window *window = nullptr;
    SDL_GLContext gl_context = nullptr;
    GLuint program_id_ = 0;

    grid *linear_grid = nullptr;
    om::user_state *user_state = nullptr;
};

static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr.");
    }
    delete e;
}

engine::~engine() = default;
engine_impl::~engine_impl() {
    delete linear_grid;
    delete user_state;
};

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }

    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        res = d;
    }

    return res;
}

static GLuint load_shader(const std::string &file_name, GLenum shader_type) {
    std::string line;
    std::ifstream file(file_name, std::ios::in);
    if (!file) throw std::runtime_error("Cannot load shader from file!");
    // todo find better way
    unsigned long pos = file.tellg();
    file.seekg(0, std::ios::end);
    unsigned long len = file.tellg();
    file.seekg(std::ios::beg);
    if (len == 0) throw std::runtime_error("Loaded shader is empty!");

    auto *shader_src = (GLchar *)calloc(len + 1, 1);

    file.read(shader_src, len);
    file.close();

    GLuint shader = glCreateShader(shader_type);
    OM_GL_CHECK()
    glShaderSource(shader, 1, &shader_src, nullptr);
    OM_GL_CHECK()

    glCompileShader(shader);
    OM_GL_CHECK()

    GLint compiled_status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled_status);
    OM_GL_CHECK()

    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
        OM_GL_CHECK()
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader, info_len, nullptr, info_chars.data());
        OM_GL_CHECK()
        glDeleteShader(shader);
        OM_GL_CHECK()

        std::string_view shader_type_str = "vertex";
        if (shader_type == GL_FRAGMENT_SHADER) shader_type_str = "fragment";

        std::cerr << "Error compiling " << shader_type_str
                  << " shader:" << std::endl
                  << shader << std::endl
                  << info_chars.data();
        throw std::runtime_error("Error compiling shader!");
    }

    return shader;
}

std::string engine_impl::initialize(std::string_view config) {
    config_map_t config_map = parse_config(config);

    using namespace std;
    stringstream serr;

    SDL_version compiled = {0, 0, 0};
    SDL_version linked = {0, 0, 0};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << err_message << endl;
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    int window_width = str_to_int(config_map["window_width"], 640);
    int window_height = str_to_int(config_map["window_height"], 480);

    window =
        SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, window_width, window_height,
                         ::SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("Cannot create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    const int gl_min_major_ver = 2;
    const int gl_min_minor_ver = 1;

    if (gl_major_ver <= gl_min_major_ver && gl_minor_ver < gl_min_minor_ver) {
        serr << "Current context opengl version: " << gl_major_ver << "."
             << gl_minor_ver << std::endl
             << "Need opengl version at least: " << gl_min_major_ver << "."
             << gl_min_minor_ver << std::endl
             << std::flush;
        return serr.str();
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
        load_gl_func("glBindBuffer", glBindBuffer);
        load_gl_func("glGenBuffers", glGenBuffers);
        load_gl_func("glGenVertexArrays", glGenVertexArrays);
        load_gl_func("glBindVertexArray", glBindVertexArray);
        load_gl_func("glBufferData", glBufferData);

        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniform1f", glUniform1f);
        load_gl_func("glUniform2f", glUniform2f);
        load_gl_func("glDeleteBuffers", glDeleteBuffers);
        load_gl_func("glDisableVertexAttribArray", glDisableVertexAttribArray);
    } catch (std::exception &ex) {
        return ex.what();
    }

    user_state = new om::user_state;

    GLuint vbo = 0;
    GLuint ebo = 0;
    GLuint vao = 0;

    glGenBuffers(1, &vbo);
    OM_GL_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    OM_GL_CHECK()

    glGenBuffers(1, &ebo);
    OM_GL_CHECK()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    OM_GL_CHECK()

    glGenVertexArrays(1, &vao);
    OM_GL_CHECK()
    glBindVertexArray(vao);
    OM_GL_CHECK()

    linear_grid = new grid;
    linear_grid->init(window_width, window_height);

    GLuint vertex_shader = load_shader("../shader.vert", GL_VERTEX_SHADER);
    GLuint fragment_shader = load_shader("../shader.frag", GL_FRAGMENT_SHADER);

    program_id_ = glCreateProgram();
    OM_GL_CHECK()
    if (0 == program_id_) {
        serr << "Failed to create gl program.";
        return serr.str();
    }

    glAttachShader(program_id_, vertex_shader);
    OM_GL_CHECK()
    glAttachShader(program_id_, fragment_shader);
    OM_GL_CHECK()

    glLinkProgram(program_id_);
    OM_GL_CHECK()
    GLint linked_status = 0;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
    OM_GL_CHECK()
    if (linked_status == 0) {
        GLint info_len = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &info_len);
        OM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id_, info_len, nullptr, infoLog.data());
        OM_GL_CHECK()
        serr << "Error linking program:" << std::endl << infoLog.data();
        glDeleteProgram(program_id_);
        OM_GL_CHECK()
        return serr.str();
    }

    glUseProgram(program_id_);
    OM_GL_CHECK()

    glDisable(GL_DEPTH_TEST);

    return "";
}

void engine_impl::update_user_state(const om::user_state &us) {
    user_state->x = us.x;
    user_state->y = us.y;
    user_state->z = us.z;
}

void engine_impl::set_uniform() {
    glUseProgram(program_id_);
    OM_GL_CHECK()
    GLint uniform_location = glGetUniformLocation(program_id_, "mouse");
    OM_GL_CHECK()
    glUniform2f(uniform_location, user_state->x, user_state->y);
    OM_GL_CHECK()
    uniform_location = glGetUniformLocation(program_id_, "radius");
    OM_GL_CHECK()
    glUniform1f(uniform_location, user_state->z);
    OM_GL_CHECK()
}

bool engine_impl::read_input(event &e) {
    SDL_Event sdl_event;

    if (SDL_PollEvent(&sdl_event)) {
        if (sdl_event.type == SDL_QUIT) {
            e = event::turn_off;
            return true;
        } else if (sdl_event.type == SDL_MOUSEMOTION) {
            float original_x = sdl_event.motion.x;
            float original_y = sdl_event.motion.y;
            float x = 0.0, y = 0.0;

            int width = 0, height = 0;
            SDL_GetWindowSize(window, &width, &height);
            if (width == 0 || height == 0) {
                throw std::runtime_error("Error! Cannot get window's size.");
            }

            float x_center = float(width) / 2.0f;
            float y_center = float(height) / 2.0f;
            x = original_x / float(width) * 2.0f - 1.0f;
            y = original_y / float(height) * 2.0f - 1.0f;
            y *= -1.0f;
            user_state->x = x;
            user_state->y = y;

            e = event::mouse_move;
            return true;
        } else if (sdl_event.type == SDL_MOUSEWHEEL) {
            float original_z = sdl_event.wheel.y;
            float z = user_state->z;
            if (original_z < 0) z -= 0.05;
            if (original_z > 0) z += 0.05;
            user_state->z = z;

            e = event::mouse_scroll;
            return true;
        }
    }

    return false;
}

void engine_impl::render_grid() { linear_grid->render(); }

void engine_impl::render_triangle(const triangle &t) {
    glBufferData(GL_ARRAY_BUFFER, sizeof(t), &t, GL_STATIC_DRAW);
    OM_GL_CHECK()
    glEnableVertexAttribArray(0);

    GLintptr position_attr_offset = 0;

    OM_GL_CHECK()
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(position_attr_offset));
    OM_GL_CHECK()
    glEnableVertexAttribArray(1);
    OM_GL_CHECK()

    GLintptr color_attr_offset = sizeof(float) * 3;

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void *>(color_attr_offset));
    OM_GL_CHECK()
    glValidateProgram(program_id_);
    OM_GL_CHECK()
    GLint validate_status = 0;
    glGetProgramiv(program_id_, GL_VALIDATE_STATUS, &validate_status);
    OM_GL_CHECK()
    if (validate_status == GL_FALSE) {
        GLint infoLen = 0;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
        OM_GL_CHECK()
        std::vector<char> infoLog(static_cast<size_t>(infoLen));
        glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
        OM_GL_CHECK()
        std::cerr << "Error linking program:" << std::endl << infoLog.data();
        throw std::runtime_error("Error!");
    }
    glDrawArrays(GL_TRIANGLES, 0, 3);
    OM_GL_CHECK()
}

void engine_impl::swap_buffers() {
    SDL_GL_SwapWindow(window);
    glClearColor(0.3f, 0.3f, 1.0f, 0.0f);
    OM_GL_CHECK()
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    OM_GL_CHECK()
}

void engine_impl::uninitialize() {
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace om