#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.h"

int main(int /*argc*/, char * /*argv*/[]) {
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error =
        engine->initialize("title=GRID;window_width=640;window_height=480;");
    if (!error.empty()) {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    om::user_state us;
    us.x = 0.0;
    us.y = 0.0;
    // todo add z for each axis
    us.z = 0.3;
    engine->update_user_state(us);

    bool continue_loop = true;
    while (continue_loop) {
        om::event event;

        while (engine->read_input(event)) {
            if (event == om::event::turn_off) {
                continue_loop = false;
            }
        }

        engine->set_uniform();
        engine->render_grid();
        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}