#version 330 core
layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_color;
out vec4 v_color;
out vec4 v_position;

uniform vec2 mouse;
uniform float radius;

void main() {
    float x = a_position.x;
    float y = a_position.y;
    float mouse_x = mouse.x;
    float mouse_y = mouse.y;

    float dx = x - mouse_x;
    float dy = y - mouse_y;

    float d = dx * dx + dy * dy;

    if (d <= (radius * radius)) {
        float hyp = sqrt(d);
        float tx = dx / hyp;
        float ty = dy / hyp;
        float radius_x = tx * radius + mouse_x;
        float radius_y = ty * radius + mouse_y;

        x = (x + radius_x) / 2;
        y = (y + radius_y) / 2;
    }

    v_position = vec4(x, y, 0.0, 1.0);
    v_color = vec4(a_color, 1.0);
    gl_Position = vec4(x, y, 0.0, 1.0);
}