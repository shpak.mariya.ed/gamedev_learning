#version 330 core
in vec4 v_position;
in vec4 v_color;

uniform vec2 mouse;
uniform float radius;

void main() {
    vec3 def_color = vec3(1.0, 0.0, 1.0);

    float x = v_position.x;
    float y = v_position.y;
    float r = v_color.r;
    float g = v_color.g;
    float b = v_color.b;

    float mouse_x = mouse.x;
    float mouse_y = mouse.y;

    float dx = x - mouse_x;
    float dy = y - mouse_y;

    float d = dx * dx + dy * dy;

    if (d <= (radius * radius)) {
        float hyp = sqrt(d);
        float t = hyp / radius;
        r = def_color.r + (r - def_color.r) * t;
        g = def_color.g + (g - def_color.g) * t;
        b = def_color.b + (b - def_color.b) * t;
    }

    gl_FragColor = vec4(r, g, b, 1.0);
}