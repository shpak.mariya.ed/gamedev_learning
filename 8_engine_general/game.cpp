#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <istream>

#include "engine.h"

void rotate(om::texture_position &tp, const float sin_a, const float cos_a) {
    float u = tp.u - 0.5f;
    float v = tp.v - 0.5f;
    tp.u = u * cos_a - v * sin_a + 0.5f;
    tp.v = u * sin_a + v * cos_a + 0.5f;
}

using positions = std::vector<om::position>;

void generate_circle_dots(positions &buffer, float r, om::position center) {
    float step = 0.001f;
    int v_count = int(r * 2 / step);
    float start_x = center.x - r;

    for (int i = 0; i <= v_count; ++i) {
        float x = start_x + step * float(i);
        float y = std::sqrt(r * r - (x - center.x) * (x - center.x)) + center.y;
        om::position tmp_pos = {x, y};
        buffer.push_back(tmp_pos);
    }

    for (int i = 0; i <= v_count; ++i) {
        float x = start_x + step * float(i);
        float y =
            2.f * center.y -
            (std::sqrt(r * r - (x - center.x) * (x - center.x)) + center.y);
        om::position tmp_pos = {x, y};
        buffer.push_back(tmp_pos);
    }
}

int main(int /*argc*/, char * /*argv*/[]) {
    std::unique_ptr<om::engine, void (*)(om::engine *)> engine(
        om::create_engine(), om::destroy_engine);

    const std::string error = engine->initialize(
        "title=ENGINE_EXAMPLES;window_width=480;window_height=480;");
    if (!error.empty()) {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    uint8_t mode_1 = engine->create_shader("../shaders/mode_1.vert",
                                           "../shaders/mode_1.frag");
    uint8_t mode_2 = engine->create_shader("../shaders/mode_2.vert",
                                           "../shaders/mode_2.frag");
    uint8_t mode_3 = engine->create_shader("../shaders/mode_3.vert",
                                           "../shaders/mode_3.frag");
    auto spiral = engine->create_texture("../resources/spiral.png");

    // generation circles for 3th mode
    positions dots_0;
    positions dots_1;
    om::position center_0 = {-0.5, 0.5};
    om::position center_1 = {0.5, 0.5};
    generate_circle_dots(dots_0, 0.3, center_0);
    generate_circle_dots(dots_1, 0.3, center_1);

    if (dots_0.size() != dots_1.size()) {
        throw std::runtime_error("Buffers sizes are not the same!");
    }

    om::color center_color = {1.f, 0.f, 0.f};
    om::color main_color = {0.f, 0.f, 0.f};

    std::vector<om::triangle_colored> circle_buffer_0;
    std::vector<om::triangle_colored> circle_buffer_1;

    // todo remove hardcode
    for (int i = 0; i < dots_0.size() - 1; ++i) {
        om::triangle_colored tmp_tr0;
        om::triangle_colored tmp_tr1;
        // center
        tmp_tr0.v[0].pos = center_0;
        tmp_tr0.v[0].col = center_color;
        tmp_tr1.v[0].pos = center_1;
        tmp_tr1.v[0].col = center_color;
        // left vertex
        tmp_tr0.v[1].pos = {dots_0.at(i).x, dots_0.at(i).y};
        tmp_tr0.v[1].col = main_color;
        tmp_tr1.v[1].pos = {dots_1.at(i).x, dots_1.at(i).y};
        tmp_tr1.v[1].col = main_color;
        // right vertex
        tmp_tr0.v[2].pos = {dots_0.at(i + 1).x, dots_0.at(i + 1).y};
        tmp_tr0.v[2].col = main_color;
        tmp_tr1.v[2].pos = {dots_1.at(i + 1).x, dots_1.at(i + 1).y};
        tmp_tr1.v[2].col = main_color;

        circle_buffer_0.push_back(tmp_tr0);
        circle_buffer_1.push_back(tmp_tr1);
    }

    int speed_mode_1 = 50;
    float speed_mode_3 = 0.5f;
    uint8_t current_mode = 1;
    bool continue_loop = true;

    while (continue_loop) {
        om::event event;
        while (engine->read_input(event)) {
            switch (event) {
            case om::event::turn_off:
                continue_loop = false;
                break;
            case om::event::button1_pressed:
                current_mode = 1;
                break;
            case om::event::button2_pressed:
                current_mode = 2;
                break;
            case om::event::button3_pressed:
                current_mode = 3;
                break;
            case om::event::space_pressed:
                ++current_mode;
                if (current_mode > 3) current_mode = 1;
                break;
            case om::event::up_pressed:
                if (current_mode == 1) {
                    speed_mode_1 -= 5;
                    if (speed_mode_1 < 1) speed_mode_1 = 1;
                }
                if (current_mode == 3) {
                    speed_mode_3 += 0.1f;
                    if (speed_mode_3 > 1.f) speed_mode_3 = 1.f;
                }
                break;
            case om::event::down_pressed:
                if (current_mode == 1) {
                    speed_mode_1 += 5;
                }
                if (current_mode == 3) {
                    speed_mode_3 -= 0.1f;
                    if (speed_mode_3 < 0.1f) speed_mode_3 = 0.1f;
                }
                break;
            case om::event::ctrl_pressed:
                if (current_mode == 1) {
                    speed_mode_1 = 50;
                }
                if (current_mode == 3) {
                    speed_mode_3 = 0.5f;
                }
            default:
                break;
            }
        }

        if (current_mode == 1) {
            // working with textures
            engine->use_shader(mode_1);

            om::triangle_textured tt0;
            om::triangle_textured tt1;

            std::ifstream file("../resources/buffer_1.txt");
            assert(!!file);
            file >> tt0 >> tt1;

            const int n = speed_mode_1 * 10;
            auto time = static_cast<int>(engine->get_time());
            int cell = time % n;
            float rotate_angle = 2.f * float(M_PI) / float(n) * float(cell);
            float cos_a = std::cos(rotate_angle);
            float sin_a = std::sin(rotate_angle);

            for (int i = 0; i < 3; ++i) {
                rotate(tt0.v[i].text_pos, sin_a, cos_a);
                rotate(tt1.v[i].text_pos, sin_a, cos_a);
            }

            engine->render(tt0, *spiral);
            engine->render(tt1, *spiral);
        }
        if (current_mode == 2) {
            // rendering interpolated triangles
            engine->use_shader(mode_2);
            om::triangle_colored tc0;
            om::triangle_colored tc1;

            std::ifstream file("../resources/buffer_2.txt");
            assert(!!file);
            file >> tc0 >> tc1;

            engine->render(tc0);
            engine->render(tc1);
        }
        if (current_mode == 3) {
            // using morphing
            engine->use_shader(mode_3);

            float time = engine->get_time();
            float step = time / 1000.0f * speed_mode_3;
            float int_part = 0;
            float frac_part = modff(step, &int_part);

            float t = 0.f;
            bool reverse = int(int_part) % 2 != 1;
            if (reverse) t = frac_part;
            else
                t = 1 - frac_part;

            for (int i = 0; i < circle_buffer_0.size(); ++i) {
                om::triangle_colored tmp_tc0 = {circle_buffer_0.at(i)};
                om::triangle_colored tmp_tc1 = {circle_buffer_1.at(i)};
                engine->morph(tmp_tc0, tmp_tc1, t);
            }
        }

        engine->swap_buffers();
    }

    engine->terminate();

    return EXIT_SUCCESS;
}