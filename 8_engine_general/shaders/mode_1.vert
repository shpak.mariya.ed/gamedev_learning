#version 330 core
layout (location = 0) in vec2 a_pos;
layout (location = 1) in vec2 a_text_pos;

out vec2 out_text_pos;

void main() {
    out_text_pos = a_text_pos;
    gl_Position = vec4(a_pos, 0.0, 1.0);
}