#include <iosfwd>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

namespace om {
enum class event {
    space_pressed,
    button1_pressed,
    button2_pressed,
    button3_pressed,
    up_pressed,
    down_pressed,
    ctrl_pressed,
    turn_off
};

class engine;

engine *create_engine();
void destroy_engine(engine *e);

struct position {
    float x = 0.f;
    float y = 0.f;
};

struct color {
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
};

struct texture_position {
    float u = 0.f;
    float v = 0.f;
};

struct vertex_full {
    position pos;
    color col;
    texture_position text_pos;
};

struct vertex_colored {
    position pos;
    color col;
};

struct vertex_textured {
    position pos;
    texture_position text_pos;
};

struct triangle {
    virtual ~triangle();
    virtual void draw() const = 0;
};

struct triangle_full : public triangle {
    ~triangle_full() override;
    void draw() const final;
    vertex_full v[3];
};

struct triangle_colored : public triangle {
    ~triangle_colored() override;
    void draw() const final;
    vertex_colored v[3];
};

struct triangle_textured : public triangle {
    ~triangle_textured() override;
    void draw() const final;
    vertex_textured v[3];
};

std::istream &operator>>(std::istream &, triangle_full &);
std::istream &operator>>(std::istream &, triangle_colored &);
std::istream &operator>>(std::istream &, triangle_textured &);

// TEXTURE
class texture {
public:
    virtual ~texture();
    virtual void bind() const = 0;
    virtual uint8_t get_id() const = 0;
};

// ENGINE
class engine {
public:
    virtual ~engine();
    virtual std::string initialize(std::string_view) = 0;
    virtual float get_time() = 0;
    virtual bool read_input(event &e) = 0;

    virtual std::unique_ptr<texture> create_texture(std::string_view) = 0;
    virtual uint8_t create_shader(std::string_view, std::string_view) = 0;
    virtual void use_shader(uint8_t) = 0;
    virtual void set_uniform(std::string_view, float) = 0;

    virtual void render(const triangle &) = 0;
    // setting uniform "texture_"
    virtual void render(const triangle &, const texture &) = 0;

    // morphing: 3th arg should be [-1.0; 1.0]
    virtual void morph(const triangle_colored &, const triangle_colored &,
                       float) = 0;

    virtual void swap_buffers() = 0;
    virtual void terminate() = 0;
};

} // end namespace om