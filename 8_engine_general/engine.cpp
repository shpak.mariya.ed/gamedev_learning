#include "engine.h"
#include "picopng.hxx"

#include <algorithm>
#include <array>
#include <cassert>
#include <charconv>
#include <cmath>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <string_view>

#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>

PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;
PFNGLDELETESHADERPROC glDeleteShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLGETPROGRAMIVPROC glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLDELETEPROGRAMPROC glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = nullptr;
PFNGLVALIDATEPROGRAMPROC glValidateProgram = nullptr;
PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = nullptr;
PFNGLBUFFERDATAPROC glBufferData = nullptr;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = nullptr;
PFNGLUNIFORM1FPROC glUniform1f = nullptr;
PFNGLUNIFORM1FPROC glUniform1i = nullptr;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;

template <typename T>
static void load_gl_func(const char *func_name, T &result) {
    void *gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer) {
        throw std::runtime_error(std::string("Cannot load GL function: ") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

namespace om {

static std::ostream &operator<<(std::ostream &out, const SDL_version &v) {
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

// todo do better
std::istream &operator>>(std::istream &is, vertex_full &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.col.r >> v.col.g >> v.col.b;
    is >> v.text_pos.u >> v.text_pos.v;
    return is;
}

std::istream &operator>>(std::istream &is, vertex_colored &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.col.r >> v.col.g >> v.col.b;
    return is;
}

std::istream &operator>>(std::istream &is, vertex_textured &v) {
    is >> v.pos.x >> v.pos.y;
    is >> v.text_pos.u >> v.text_pos.v;
    return is;
}

// todo find better case
std::istream &operator>>(std::istream &is, triangle_full &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream &operator>>(std::istream &is, triangle_colored &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

std::istream &operator>>(std::istream &is, triangle_textured &t) {
    is >> t.v[0];
    is >> t.v[1];
    is >> t.v[2];
    return is;
}

// TRIANGLES
triangle::~triangle() = default;
triangle_full::~triangle_full() = default;
triangle_colored::~triangle_colored() = default;
triangle_textured::~triangle_textured() = default;

void triangle_full::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_full);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // u, v - texture
    glEnableVertexAttribArray(2);
    position_p += sizeof(color);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void triangle_colored::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_colored);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

void triangle_textured::draw() const {
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), &v, GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_textured);
    GLintptr position_p = 0;
    // x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // u, v - texture
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

// TEXTURE_2D
class texture_2d final : public texture {
public:
    explicit texture_2d(std::string_view);
    ~texture_2d() override;
    uint8_t get_id() const final;
    void bind() const final;

private:
    GLuint texture_id = 0;
};

texture_2d::texture_2d(std::string_view path) {
    std::vector<unsigned char> png_file_in_memory;
    std::ifstream ifs(path.data(), std::ios_base::binary);
    if (!ifs) {
        throw std::runtime_error("Cannot load texture.");
    }
    ifs.seekg(0, std::ios_base::end);
    std::streamoff pos_in_file = ifs.tellg();
    png_file_in_memory.resize(static_cast<size_t>(pos_in_file));
    ifs.seekg(0, std::ios_base::beg);
    if (!ifs) {
        throw std::runtime_error("Cannot load texture.");
    }

    ifs.read(reinterpret_cast<char *>(png_file_in_memory.data()), pos_in_file);
    if (!ifs.good()) {
        throw std::runtime_error("Cannot load texture.");
    }

    std::vector<unsigned char> image;
    unsigned long w = 0;
    unsigned long h = 0;
    int error = decodePNG(image, w, h, &png_file_in_memory[0],
                          png_file_in_memory.size(), false);

    if (error != 0) {
        std::cerr << "Error: " << error << std::endl;
        throw std::runtime_error("Cannot load texture.");
    }

    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    GLint mipmap_level = 0;
    GLint border = 0;
    auto width = static_cast<GLsizei>(w);
    auto height = static_cast<GLsizei>(h);
    glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width, height, border,
                 GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

texture::~texture() = default;
texture_2d::~texture_2d() { glDeleteTextures(1, &texture_id); }

uint8_t texture_2d::get_id() const { return texture_id; }

void texture_2d::bind() const { glBindTexture(GL_TEXTURE_2D, texture_id); }

// SHADER
class shader {
public:
    shader(std::string_view, std::string_view);
    ~shader();
    uint8_t get_id() const;
    GLuint load(std::string_view, GLenum);
    void compile(GLuint, GLenum);
    void create_program();
    void use();

    void set_uniform(std::string_view, float) const;
    void set_uniform(std::string_view, const texture &) const;

private:
    GLuint program_id = 0;
    GLuint vert_shader_id = 0;
    GLuint frag_shader_id = 0;
};

shader::shader(std::string_view vert_path, std::string_view frag_path) {
    vert_shader_id = load(vert_path, GL_VERTEX_SHADER);
    frag_shader_id = load(frag_path, GL_FRAGMENT_SHADER);

    compile(vert_shader_id, GL_VERTEX_SHADER);
    compile(frag_shader_id, GL_FRAGMENT_SHADER);

    create_program();
}

shader::~shader() {
    glDeleteShader(vert_shader_id);
    glDeleteShader(frag_shader_id);
}

GLuint shader::load(std::string_view path, GLenum type) {
    std::string line;
    std::ifstream file(path.data(), std::ios::in);
    assert(!!file);
    file.seekg(0, std::ios::end);
    unsigned long len = file.tellg();
    file.seekg(std::ios::beg);
    if (len == 0) {
        throw std::runtime_error("Loaded shader is empty!");
    }

    auto *shader_src = (GLchar *)calloc(len + 1, 1);

    file.read(shader_src, len);
    file.close();

    GLuint shader_id = glCreateShader(type);
    if (shader_id == 0) {
        throw std::runtime_error(
            "Cannot create shader! glCreateShader returned 0.");
    }

    glShaderSource(shader_id, 1, &shader_src, nullptr);
    delete shader_src;

    return shader_id;
}

void shader::compile(GLuint shader_id, GLenum type) {
    glCompileShader(shader_id);

    GLint compiled_status = 0;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);

    if (compiled_status == 0) {
        GLint info_len = 0;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_chars(static_cast<size_t>(info_len));
        glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
        glDeleteShader(shader_id);

        std::string_view shader_type_str = "vertex";
        if (type == GL_FRAGMENT_SHADER) shader_type_str = "fragment";

        std::cerr << "Error compiling " << shader_type_str
                  << " shader:" << std::endl
                  << shader_id << std::endl
                  << info_chars.data();
        throw std::runtime_error("Error compiling shader!");
    }
}

void shader::create_program() {
    program_id = glCreateProgram();
    if (0 == program_id) {
        throw std::runtime_error("Failed to create gl program.");
    }

    glAttachShader(program_id, vert_shader_id);
    glAttachShader(program_id, frag_shader_id);

    glLinkProgram(program_id);
    GLint linked_status = 0;
    glGetProgramiv(program_id, GL_LINK_STATUS, &linked_status);
    if (linked_status == 0) {
        GLint info_len = 0;
        glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_len);
        std::vector<char> info_log(static_cast<size_t>(info_len));
        glGetProgramInfoLog(program_id, info_len, nullptr, info_log.data());
        std::cerr << "Error linking program:" << std::endl << info_log.data();
        glDeleteProgram(program_id);
    }
}

uint8_t shader::get_id() const { return static_cast<uint8_t>(program_id); }

void shader::use() { glUseProgram(program_id); }

void shader::set_uniform(std::string_view name, float value) const {
    GLint uniform_location = glGetUniformLocation(program_id, name.data());
    glUniform1f(uniform_location, value);
}

void shader::set_uniform(std::string_view name, const texture &t) const {
    GLint uniform_location = glGetUniformLocation(program_id, name.data());
    glActiveTexture(GL_TEXTURE0);
    t.bind();
    glUniform1i(uniform_location, 0);
}

// ENGINE_IMPL
class engine_impl final : public engine {
public:
    ~engine_impl() override;
    std::string initialize(std::string_view) final;
    float get_time() final;
    bool read_input(event &) final;

    std::unique_ptr<texture> create_texture(std::string_view) final;
    uint8_t create_shader(std::string_view, std::string_view) final;
    void use_shader(uint8_t) final;
    void set_uniform(std::string_view, float) final;

    void render(const triangle &) final;
    void render(const triangle &, const texture &) final;

    void morph(const triangle_colored &, const triangle_colored &, float) final;

    void swap_buffers() final;
    void terminate() final;

private:
    SDL_Window *window = nullptr;
    SDL_GLContext gl_context = nullptr;

    std::map<uint8_t, std::unique_ptr<shader>> shaders;
    shader *current_shader = nullptr;
};

static bool already_exist = false;

engine *create_engine() {
    if (already_exist) {
        throw std::runtime_error("Engine already exist.");
    }
    engine *result = new engine_impl();
    already_exist = true;
    return result;
}

void destroy_engine(engine *e) {
    if (!already_exist) {
        throw std::runtime_error("Engine not created.");
    }
    if (nullptr == e) {
        throw std::runtime_error("e is nullptr.");
    }
    delete e;
}

engine::~engine() = default;
engine_impl::~engine_impl() = default;

using config_map_t = std::map<std::string_view, std::string>;
// parsing string as "key1:value1;key2:value2;"
static config_map_t parse_config(std::string_view conf) {
    config_map_t config_map;

    try {
        char del = ';';
        char sub_del = '=';
        std::string_view key_value, key, value;
        size_t start = 0;
        size_t end = 0;
        size_t sub_pos = 0;

        while ((end = conf.find(del, start)) < conf.size()) {
            key_value = conf.substr(start, end - start);
            if ((sub_pos = key_value.find(sub_del)) != std::string::npos) {
                key = key_value.substr(0, sub_pos);
                value = key_value.substr(sub_pos + 1);
                config_map.emplace(key, value);
            }
            start = end + 1;
        }
    } catch (...) {
        std::cerr << "Incorrect config format!" << std::endl;
    }

    return config_map;
}

static int str_to_int(std::string str, int d) {
    int res = 0;
    auto check_res = std::from_chars(str.data(), str.data() + str.size(), res);
    if (check_res.ec == std::errc::invalid_argument) {
        std::cerr << "Could not convert " << str << " into integer!"
                  << std::endl;
        res = d;
    }

    return res;
}

std::string engine_impl::initialize(std::string_view config) {
    config_map_t config_map = parse_config(config);

    using namespace std;
    stringstream serr;

    SDL_version compiled = {0, 0, 0};
    SDL_version linked = {0, 0, 0};

    SDL_VERSION(&compiled)
    SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch)) {
        serr << "Warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_Init: " << err_message << endl;
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

    auto title = config_map["title"];
    if (title.empty()) title = "title";
    int window_width = str_to_int(config_map["window_width"], 640);
    int window_height = str_to_int(config_map["window_height"], 480);

    window =
        SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                         SDL_WINDOWPOS_CENTERED, window_width, window_height,
                         ::SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI);

    if (window == nullptr) {
        const char *err_message = SDL_GetError();
        serr << "Error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        return serr.str();
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr) {
        std::string msg("Cannot create opengl context: ");
        msg += SDL_GetError();
        serr << msg << endl;
        return serr.str();
    }

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    SDL_assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    SDL_assert(result == 0);

    const int gl_min_major_ver = 2;
    const int gl_min_minor_ver = 1;

    if (gl_major_ver <= gl_min_major_ver && gl_minor_ver < gl_min_minor_ver) {
        serr << "Current context opengl version: " << gl_major_ver << "."
             << gl_minor_ver << std::endl
             << "Need opengl version at least: " << gl_min_major_ver << "."
             << gl_min_minor_ver << std::endl
             << std::flush;
        return serr.str();
    }

    try {
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
        load_gl_func("glBindBuffer", glBindBuffer);
        load_gl_func("glGenBuffers", glGenBuffers);
        load_gl_func("glGenVertexArrays", glGenVertexArrays);
        load_gl_func("glBindVertexArray", glBindVertexArray);
        load_gl_func("glBufferData", glBufferData);
        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniform1f", glUniform1f);
        load_gl_func("glUniform1i", glUniform1i);
        load_gl_func("glDeleteBuffers", glDeleteBuffers);
        load_gl_func("glDisableVertexAttribArray", glDisableVertexAttribArray);
    } catch (std::exception &ex) {
        return ex.what();
    }

    GLuint vbo = 0;
    GLuint ebo = 0;
    GLuint vao = 0;

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glGenBuffers(1, &ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // todo add using ebo

    return "";
}

float engine_impl::get_time() { return SDL_GetTicks(); }

bool engine_impl::read_input(event &e) {
    SDL_Event sdl_event;
    if (SDL_PollEvent(&sdl_event)) {
        if (sdl_event.type == SDL_QUIT) {
            e = event::turn_off;
            return true;
        }
        if (sdl_event.type == SDL_KEYDOWN) {
            SDL_Keycode key_code = sdl_event.key.keysym.sym;
            if (key_code == SDLK_1) {
                e = event::button1_pressed;
                return true;
            } else if (key_code == SDLK_2) {
                e = event::button2_pressed;
                return true;
            } else if (key_code == SDLK_3) {
                e = event::button3_pressed;
                return true;
            } else if (key_code == SDLK_SPACE) {
                e = event::space_pressed;
                return true;
            } else if (key_code == SDLK_UP) {
                e = event::up_pressed;
                return true;
            } else if (key_code == SDLK_DOWN) {
                e = event::down_pressed;
                return true;
            } else if (key_code == SDLK_LCTRL || key_code == SDLK_RCTRL) {
                e = event::ctrl_pressed;
                return true;
            }
        }
    }

    return false;
}

std::unique_ptr<texture> engine_impl::create_texture(std::string_view path) {
    return std::make_unique<texture_2d>(path);
}

uint8_t engine_impl::create_shader(std::string_view vert_path,
                                   std::string_view frag_path) {
    auto s = std::make_unique<shader>(vert_path, frag_path);
    uint8_t shader_id = s->get_id();
    shaders.insert(std::make_pair(shader_id, std::move(s)));
    return shader_id;
}

void engine_impl::use_shader(uint8_t shader_id) {
    current_shader = shaders.at(shader_id).get();
    current_shader->use();
}

void engine_impl::set_uniform(std::string_view name, float value) {
    current_shader->set_uniform(name, value);
}

void engine_impl::render(const triangle &tr) {
    // todo add protect from calling just triangle::draw()
    tr.draw();
}

void engine_impl::render(const triangle &tr, const texture &t) {
    current_shader->set_uniform("texture_", t);
    tr.draw();
}

void engine_impl::morph(const triangle_colored &tr0,
                        const triangle_colored &tr1, float t) {
    current_shader->set_uniform("t", t);

    std::vector<vertex_colored> buffer;
    for (int i = 0; i < 3; ++i) {
        buffer.push_back(tr0.v[i]);
        buffer.push_back(tr1.v[i]);
    }

    glBufferData(GL_ARRAY_BUFFER, buffer.size() * sizeof(vertex_colored),
                 &buffer.front(), GL_STATIC_DRAW);

    GLuint stride = sizeof(vertex_colored) * 2;
    GLintptr position_p = 0;

    // 1th triangle: x, y
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // 1th triangle: r, g, b
    glEnableVertexAttribArray(1);
    position_p += sizeof(position);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // 2th triangle: x, y
    glEnableVertexAttribArray(2);
    position_p += sizeof(color);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));
    // triangle from 2th buffer: r, g, b
    position_p += sizeof(position);
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, stride,
                          reinterpret_cast<void *>(position_p));

    GLuint drawing_count = buffer.size() / 2;
    glDrawArrays(GL_TRIANGLES, 0, drawing_count);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
}

void engine_impl::swap_buffers() {
    SDL_GL_SwapWindow(window);
    glClearColor(1.f, 1.f, 1.f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void engine_impl::terminate() {
    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

} // namespace om